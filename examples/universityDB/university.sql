CREATE DATABASE university;
SET search_path = university;

CREATE TABLE STUDENT
(
    studentID SERIAL PRIMARY KEY NOT NULL,
    lastname VARCHAR(32) NOT NULL,
    firstname VARCHAR(32) NOT NULL,
    studies VARCHAR(32) NOT NULL
);

INSERT INTO STUDENT (studentID, lastname, firstname, studies) VALUES
    (1, 'Moore', 'Donald', 'Teaching'),
    (2, 'Morgan', 'Sarah', 'Mathematics'),
    (3, 'Wood', 'Jack', 'Engineering'),
    (4, 'Harrison', 'Elisabeth', 'Computer Science'),
    (5, 'Williams', 'John', 'Computer Science'),
    (6, 'William', 'Mary', 'Computer Science'),
    (7, 'Smith', 'Jack', 'Engineering'),
    (8, 'John', 'Jennifer', 'Theory')
;

CREATE TABLE COURSE
(
    courseID SERIAL PRIMARY KEY NOT NULL,
    title VARCHAR(64) NOT NULL
);

INSERT INTO COURSE (courseID, title) VALUES
    (1, 'Database Systems'),
    (2, 'Operating Systems'),
    (3, 'Artificial Intelligence'),
    (4, 'Probability Theory'),
    (5, 'Algorithms'),
    (6, 'Data Science'),
    (7, 'Law and Science'),
    (8, 'Data Warehouses'),
    (9, 'Cybersecurity')
;

CREATE TABLE LECTURER
(
    courseID SERIAL NOT NULL,
    lecturer VARCHAR(32) NOT NULL,
    PRIMARY KEY (courseID, lecturer),
    FOREIGN KEY (courseID) REFERENCES COURSE (courseID) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO LECTURER (courseID, lecturer) VALUES
    (1, 'Professor A'),
    (1, 'Lecturer A'),
    (2, 'Professor B'),
    (3, 'Professor C'),
    (4, 'Professor D'),
    (5, 'Lecturer B'),
    (6, 'Professor D'),
    (7, 'Professor A'),
    (8, 'Professor E'),
    (9, 'Professor A')
;

CREATE TABLE GRADE
(
    courseID SERIAL NOT NULL,
    studentID SERIAL NOT NULL,
    semester VARCHAR(8) NOT NULL,
    Grade NUMERIC(2, 1) NOT NULL,
    PRIMARY KEY (courseID, studentID, semester),
    FOREIGN KEY (courseID) REFERENCES COURSE (courseID) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (studentID) REFERENCES STUDENT (studentID) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO GRADE (courseID, studentID, semester, grade) VALUES
    (1, 1, 'SS 16', 2.0),
    (1, 2, 'SS 16', 1.7),
    (1, 4, 'SS 16', 1.7),
    (1, 5, 'SS 16', 3.0),
    (2, 1, 'WS 15/16', 3.7),
    (2, 2, 'WS 14/15', 1.3),
    (2, 3, 'WS 14/15', 2.3),
    (2, 4, 'WS 15/16', 1.0),
    (2, 5, 'WS 15/16', 1.3),
    (2, 6, 'WS 15/16', 2.0),
    (2, 7, 'WS 15/16', 3.3),
    (3, 1, 'WS 16/17', 1.0),
    (4, 3, 'WS 16/17', 1.3),
    (4, 2, 'WS 16/17', 3.0),
    (5, 4, 'SS 17', 2.7),
    (5, 7, 'SS 17', 1.7),
    (6, 4, 'SS 17', 2.7),
    (6, 5, 'SS 17', 4.0),
    (7, 1, 'SS 16', 2.3),
    (7, 3, 'SS 16', 1.7),
    (9, 1, 'SS 16', 3.3),
    (9, 5, 'SS 15', 5.0),
    (9, 5, 'SS 16', 2.7)
;

CREATE TABLE PARTICIPANT
(
    courseID SERIAL NOT NULL,
    studentID SERIAL NOT NULL,
    PRIMARY KEY (courseID, studentID),
    FOREIGN KEY (courseID) REFERENCES COURSE (courseID) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (studentID) REFERENCES STUDENT (studentID) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO PARTICIPANT (courseID, studentID) VALUES
    (1, 1),
    (1, 2),
    (1, 4),
    (1, 5),
    (2, 1),
    (2, 2),
    (2, 3),
    (2, 4),
    (2, 5),
    (2, 6),
    (2, 7),
    (3, 1),
    (4, 3),
    (4, 2),
    (4, 7),
    (5, 4),
    (5, 7),
    (6, 4),
    (6, 5),
    (7, 1),
    (7, 3),
    (7, 5),
    (8, 3),
    (9, 1)
;