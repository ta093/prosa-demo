# Company Example

```sql
SELECT name, sal
FROM employee NATURAL JOIN salary
WHERE name = 'Stefan';
```
(execution in ProSA works)

```sql
SELECT MAX(sal)
FROM salary;
```
(execution in ProSA still in progress)
