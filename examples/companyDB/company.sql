CREATE DATABASE company;
SET search_path = company;

CREATE TABLE EMPLOYEE(
    id INT PRIMARY KEY,
    name VARCHAR(16)
);

INSERT INTO EMPLOYEE(id, name) VALUES
    (1, 'Stefan'),
    (2, 'Stefan'),
    (3, 'Mia'),
    (4, 'Anna')
;

CREATE TABLE SALARY(
    id INT,
    sal INT,
    PRIMARY KEY(id, sal),
    FOREIGN KEY (id) REFERENCES EMPLOYEE (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO SALARY(id, sal) VALUES
    (2, 1500),
    (3, 1500),
    (4, 1250),
    (1, 1470),
    (2, 1470)
;