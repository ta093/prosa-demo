package sql2sttgd.serializer;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import xml.Input;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class XmlSerializer {
    /**
     * Turns the java class {@link Input} into XML code.
     *
     * @param xmlObject the xml class
     * @return XML code
     * @throws JAXBException if marshalling did not work
     */
    public static ByteArrayOutputStream marshal(Input xmlObject) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        // marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "../xsd/input.xsd");

        // set origin of XML file to "prosa", so that ChaTEAU sorts terms
        xmlObject.setOrigin("prosa");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        marshaller.marshal(xmlObject, baos);

        return baos;
    }

    /**
     * Turns xml code into java classes.
     *
     * @param xmlStream the XML code
     * @return the input object
     * @throws JAXBException if unmarshalling did not work
     */
    public static Input unmarshal(InputStream xmlStream) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        return (Input) unmarshaller.unmarshal(xmlStream);
    }

}
