package sql2sttgd.compositor;

import sql2sttgd.data.dbschema.DBSchema;
import xml.*;

public class Compositor {

    private static final ObjectFactory xmlFactory = new ObjectFactory();

    public static Input buildXml(Dependencies dependencies, DBSchema dbSchema, Instance instance) {
        Schema schema = xmlFactory.createSchema();
        schema.setRelations(dbSchema.getRelationsObject());
        schema.setDependencies(dependencies);

        Input input = xmlFactory.createInput();
        input.setSchema(schema);
        input.setInstance(instance);

        input.setOrigin("prosa");

        return input;
    }

}
