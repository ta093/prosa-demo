package sql2sttgd.dbreader;

public class NoColumnsException extends IllegalArgumentException {
    public NoColumnsException(String schema, String table) {
        super("Table " + table + " in Schema " + schema + " has no columns (Do Schema and/or Table exist?)");
    }
}
