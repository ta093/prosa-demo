package sql2sttgd.dbreader;

import lombok.NonNull;
import sql2sttgd.data.dbschema.DBSchema;
import term.Term;
import xml.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * A schema and instance reader for a database. It is capable of extracting tables and converting them into JAXB objects
 * for further processing.
 */
public class DatabaseReader {
    /**
     * Maps JDBC types to ChaTEAU-XML attribute types.
     */
    private static final Map<JDBCType, AttributeType> ATTR_TYPE_MAP = Map.of(
            JDBCType.SMALLINT, AttributeType.INT,
            JDBCType.INTEGER, AttributeType.INT,
            JDBCType.REAL, AttributeType.DOUBLE,
            JDBCType.DOUBLE, AttributeType.DOUBLE,
            JDBCType.CHAR, AttributeType.STRING,
            JDBCType.VARCHAR, AttributeType.STRING,
            JDBCType.NUMERIC, AttributeType.DOUBLE
    );
    private final Connection dbConn;
    private final ObjectFactory xmlFactory;

    /**
     * Create a new DatabaseReader that connects to a DB using the given URL and credentials.
     */
    public DatabaseReader(@NonNull Connection dbConn) {
        this.dbConn = dbConn;
        this.xmlFactory = new ObjectFactory();
    }

    /**
     * Extract some tables from a schema in the database.
     *
     * @param schema schema containing the tables
     * @param tables list of table names to extract
     * @return {@link DBSchema} object
     * @throws SQLException if a database access error occurs
     */
    public DBSchema readSchema(String schema, @NonNull List<String> tables) throws SQLException {
        DBSchema dbSchema = new DBSchema();
        DatabaseMetaData dbMeta = dbConn.getMetaData();

        for (String table : tables) {
            try (ResultSet cols = dbMeta.getColumns(null, schema, table, null)) {
                if (!cols.isBeforeFirst())
                    throw new NoColumnsException(schema, table);

                Relation rel = xmlFactory.createRelation();
                rel.setName(table);
                rel.setTag(Tag.S);

                while (cols.next()) {
                    Attribute attr = xmlFactory.createAttribute();
                    attr.setName(cols.getString("COLUMN_NAME"));
                    attr.setType(ATTR_TYPE_MAP.get(JDBCType.valueOf(cols.getInt("DATA_TYPE"))));
                    rel.getAttribute().add(attr);
                }
                dbSchema.putTable(rel);
            }
        }
        return dbSchema;
    }

    /**
     * Extract the tuples of some tables from a schema in the database.
     *
     * @param schema schema containing the tables
     * @param tables list of table names to extract tuples from
     * @return ChaTEAU-XML Instance object containing tables as InstanceAtom objects
     * @throws SQLException if a database access error occurs
     */
    public Instance readInstance(String schema, @NonNull List<String> tables) throws SQLException {
        Instance inst = xmlFactory.createInstance();
        int index = 1;

        for (String table : tables) {
            try (Statement stmt = dbConn.createStatement()) {
                ResultSet rows = stmt.executeQuery(String.format("SELECT * FROM \"%s\".\"%s\"", schema, table));
                int colCount = rows.getMetaData().getColumnCount();

                while (rows.next()) {
                    InstanceAtom atom = xmlFactory.createInstanceAtom();
                    atom.setName(table);
                    atom.setId(table + "_" + rows.getRow());

                    for (int i = 1; i <= colCount; i++) {
                        Constant constant = xmlFactory.createConstant();
                        constant.setName(rows.getMetaData().getColumnName(i));
                        constant.setValue(rows.getString(i));
                        if (rows.wasNull()) {
                            Null nullElem = xmlFactory.createNull();
                            nullElem.setName(rows.getMetaData().getColumnName(i));
                            nullElem.setIndex(index++);
                            atom.getNull().add(nullElem);
                        } else {
                            atom.getConstant().add(constant);
                        }
                    }

                    inst.getAtom().add(atom);
                }
            }
        }
        return inst;
    }
    
    /**
     * Reads tuples from given tables and return their attributes in ordered 
     * form, which {@link #readInstance(String, List)}} does not. 
     * Ordered attributes allow for more convenient readable printing.
     * 
     * @param schema the database schema to read, default: 'public'.
     * @param tables the tables to read the tuples from.
     * @return a map from tuple ID to tuple
     * @throws SQLException
     */
    public LinkedHashMap<String, atom.RelationalAtom> readInstanceWithMappedAttributes(String schema, @NonNull List<String> tables) throws SQLException {
        
        LinkedHashMap<String, atom.RelationalAtom> tuplesWithID = new LinkedHashMap<>();
        int index = 1;
        
        for (String table : tables) {
            try (Statement stmt = dbConn.createStatement()) {
                ResultSet rows = stmt.executeQuery(String.format("SELECT * FROM \"%s\".\"%s\"", schema, table));
                int colCount = rows.getMetaData().getColumnCount();

                while (rows.next()) {
                    atom.RelationalAtom tuple = new atom.RelationalAtom(table);
                    String id = table + "_" + rows.getRow();
                    ArrayList<Term> attributes = new ArrayList<>();
                    for (int i = 1; i <= colCount; i++) {
                        String attributeName = rows.getMetaData().getColumnName(i);
                        String value = rows.getString(i);
                        if (rows.wasNull()) {
                            attributes.add(new term.Null(attributeName, index++));
                        } else {
                            attributes.add(term.Constant.fromString(attributeName, value));
                        }
                    }
                    tuple.setTerms(attributes);
                    tuplesWithID.put(id, tuple);
                }
            }
        }
        return tuplesWithID;
    }
}
