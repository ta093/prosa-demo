package sql2sttgd.data.rules;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class InternalRule {

    private InternalBody body;
    private InternalHead head;
    private RuleType ruleType;

}
