package sql2sttgd.data.rules.variable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xml.AttributeType;

@Getter
@Setter
@ToString(callSuper = true)
public class InternalStringVariable extends InternalVariable<String> {

    String constantValue;

    public InternalStringVariable(String name, int index, String constantValue) {
        super(name, index);
        this.constantValue = constantValue;
    }

    @Override
    public AttributeType getType() {
        return AttributeType.STRING;
    }
}
