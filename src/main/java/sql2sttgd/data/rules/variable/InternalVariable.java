package sql2sttgd.data.rules.variable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sql2sttgd.data.rules.expression.InternalExpression;
import xml.Attribute;
import xml.AttributeType;

@Getter
@Setter
@AllArgsConstructor
@ToString
public abstract class InternalVariable<T> implements InternalExpression {

    // TODO: refactor to correctly support null values (corresponding to xml.Null)

    private String name;
    private int index;

    public static InternalVariable<?> of(Attribute attribute, int index) {
        switch (attribute.getType()) {
            case STRING:
                return new InternalStringVariable(attribute.getName(), index, null);
            case INT:
                return new InternalIntegerVariable(attribute.getName(), index, null);
            case DOUBLE:
                return new InternalDoubleVariable(attribute.getName(), index, null);
            default:
                throw new IllegalStateException("unknown column data type: " + attribute.getType());
        }
    }

    public abstract T getConstantValue();

    public abstract void setConstantValue(T constantValue);

    public abstract AttributeType getType();

    public boolean isConstant() {
        return getConstantValue() != null;
    }

    public boolean hasSameTypeAs(InternalVariable<?> other) {
        return other != null && this.getType() == other.getType();
    }

    @Override
    public boolean isAtomic() {
        return true;
    }

    public boolean isNumeric() {
        return getType() == AttributeType.DOUBLE || getType() == AttributeType.INT;
    }
}
