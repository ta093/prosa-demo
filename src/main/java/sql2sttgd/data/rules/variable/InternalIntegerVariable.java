package sql2sttgd.data.rules.variable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xml.AttributeType;

@Getter
@Setter
@ToString(callSuper = true)
public class InternalIntegerVariable extends InternalVariable<Integer> {

    private Integer constantValue;

    public InternalIntegerVariable(String name, int index, Integer constantValue) {
        super(name, index);
        this.constantValue = constantValue;
    }

    @Override
    public AttributeType getType() {
        return AttributeType.INT;
    }
}
