package sql2sttgd.data.rules.variable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xml.AttributeType;

@Getter
@Setter
@ToString(callSuper = true)
public class InternalDoubleVariable extends InternalVariable<Double> {

    Double constantValue;

    public InternalDoubleVariable(String name, int index, Double constantValue) {
        super(name, index);
        this.constantValue = constantValue;
    }

    @Override
    public AttributeType getType() {
        return AttributeType.DOUBLE;
    }
}
