package sql2sttgd.data.rules.atom;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import sql2sttgd.data.rules.variable.InternalVariable;
import xml.Attribute;
import xml.Relation;
import xml.Tag;

/**
 * Atom for the result table of a query.
 */
@Getter
@AllArgsConstructor
@ToString
public class InternalResultAtom extends InternalAtom {

    public static final String RESULT_NAME = "Result";


    @Override
    public String getName() {
        return RESULT_NAME;
    }

    /**
     * Build the result relation from this ResultAtom.
     * @return the generated result relation
     */
    public Relation buildResultRelation() {
        Relation result = new Relation();
        result.setName(RESULT_NAME);
        result.setTag(Tag.T);

        for (InternalVariable<?> variable : getVariables()) {
            Attribute attribute = new Attribute();
            attribute.setName(variable.getName());
            attribute.setType(variable.getType());

            result.getAttribute().add(attribute);
        }

        return result;
    }

    /**
     * Build the result relation from this ResultAtom but with custom column names.
     * E.g. used for the result relations from aggregates.
     * @param columnNames array of custom column names
     * @return the generated result relation
     */
    public Relation buildResultRelation(@NonNull String... columnNames) {
        Relation result = new Relation();
        result.setName(RESULT_NAME);
        result.setTag(Tag.T);

        if (columnNames.length != getVariables().size()) {
            throw new IllegalArgumentException(
                    "Number of variables in result atom doesn't match number of given column names");
        }

        for (int i = 0; i < getVariables().size(); i++) {
            Attribute attribute = new Attribute();
            attribute.setName(columnNames[i]);
            attribute.setType(getVariables().get(i).getType());

            result.getAttribute().add(attribute);
        }

        return result;
    }

}
