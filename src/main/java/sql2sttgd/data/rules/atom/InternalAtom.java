package sql2sttgd.data.rules.atom;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import sql2sttgd.data.rules.variable.InternalVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for all relational atoms.
 */
@Getter
@ToString
public abstract class InternalAtom {

    private final List<InternalVariable<?>> variables = new ArrayList<>();

    @Setter
    private boolean negated;

    public void addVariable(@NonNull InternalVariable<?> variable) {
        variables.add(variable);
    }

    public abstract String getName();

}
