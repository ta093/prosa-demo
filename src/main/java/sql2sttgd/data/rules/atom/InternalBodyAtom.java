package sql2sttgd.data.rules.atom;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import xml.Relation;

/**
 * Class for a usual relational atom in the body of an (s-t) tgd.
 */
@Getter
@RequiredArgsConstructor
@ToString
public class InternalBodyAtom extends InternalAtom {

    private final Relation relation;
    private final int relationIndex;

    @Override
    public String getName() {
        return relation.getName();
    }

}
