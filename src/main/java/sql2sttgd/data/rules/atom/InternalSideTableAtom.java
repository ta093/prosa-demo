package sql2sttgd.data.rules.atom;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import xml.Relation;

/**
 * Atom for side tables. Side tables are internally generated tables used for computation of aggregates.
 */
@Getter
@RequiredArgsConstructor
@ToString
public class InternalSideTableAtom extends InternalAtom {

    private final Relation relation;


    @Override
    public String getName() {
        return relation.getName();
    }
}
