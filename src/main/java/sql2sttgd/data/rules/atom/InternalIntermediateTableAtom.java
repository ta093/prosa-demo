package sql2sttgd.data.rules.atom;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import sql2sttgd.data.dbschema.IntermediateRelation;

/**
 * Atom corresponding to {@link IntermediateRelation}. Represents atoms for intermediate tables,
 * e.g. to add sequential IDs.
 */
@Getter
@RequiredArgsConstructor
@ToString
public class InternalIntermediateTableAtom extends InternalAtom {

    private final IntermediateRelation relation;


    @Override
    public String getName() {
        return relation.getName();
    }
}
