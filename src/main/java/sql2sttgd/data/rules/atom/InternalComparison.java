package sql2sttgd.data.rules.atom;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sql2sttgd.data.rules.expression.InternalExpression;
import sql2sttgd.data.rules.variable.InternalVariable;
import xml.ComparisonOperators;

/**
 * A (binary) comparison in a dependency between a variable and an expression,
 * e.g. a_1 < a_2 + 1.
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
public class InternalComparison {

    private ComparisonOperators operator;

    private InternalVariable<?> left;

    private InternalExpression right;


    public static InternalComparison equal(InternalVariable<?> left, InternalExpression right) {
        return new InternalComparison(ComparisonOperators.EQ, left, right);
    }

}
