package sql2sttgd.data.rules;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.atom.InternalComparison;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class InternalHead {

    private final List<InternalAtom> atoms = new ArrayList<>();

    private final List<InternalComparison> comparisons = new ArrayList<>();


    public void addAtom(@NonNull InternalAtom atom) {
        atoms.add(atom);
    }

    public void addComparison(InternalComparison comparison) {
        comparisons.add(comparison);
    }

    public static InternalHead of(InternalAtom atom) {
        InternalHead head = new InternalHead();
        head.addAtom(atom);
        return head;
    }

}
