package sql2sttgd.data.rules.expression;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import xml.AttributeType;

@Getter
@Setter
@ToString
public class InternalDoubleNumber extends InternalNumber<Double> {

    @NonNull
    private Double value;

    @Override
    public AttributeType getType() {
        return AttributeType.DOUBLE;
    }

    @Override
    public String getValueAsString() {
        return value.toString();
    }
}
