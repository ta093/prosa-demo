package sql2sttgd.data.rules.expression;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xml.FunctionOperators;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class InternalFunction implements InternalExpression {

    private FunctionOperators operator;

    private InternalExpression left;

    private InternalExpression right;


    @Override
    public boolean isAtomic() {
        return false;
    }

    public static InternalFunction plus(InternalExpression left, InternalExpression right) {
        return new InternalFunction(FunctionOperators.ADDITION, left, right);
    }

    public static InternalFunction minus(InternalExpression left, InternalExpression right) {
        return new InternalFunction(FunctionOperators.SUBTRACTION, left, right);
    }

    public static InternalFunction times(InternalExpression left, InternalExpression right) {
        return new InternalFunction(FunctionOperators.MULTIPLICATION, left, right);
    }

    public static InternalFunction divide(InternalExpression left, InternalExpression right) {
        return new InternalFunction(FunctionOperators.DIVISION, left, right);
    }

    public static InternalFunction plusOne(InternalExpression left) {
        return new InternalFunction(FunctionOperators.ADDITION, left, InternalIntegerNumber.one());
    }

    public static InternalFunction minusOne(InternalExpression left) {
        return new InternalFunction(FunctionOperators.SUBTRACTION, left, InternalIntegerNumber.one());
    }

}
