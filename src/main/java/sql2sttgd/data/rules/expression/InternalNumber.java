package sql2sttgd.data.rules.expression;

import lombok.NonNull;
import xml.AttributeType;

public abstract class InternalNumber<T> implements InternalExpression {

    public abstract AttributeType getType();

    @NonNull
    public abstract T getValue();

    public abstract void setValue(@NonNull T value);

    public abstract String getValueAsString();

    public boolean hasSameTypeAs(InternalNumber<?> other) {
        return other != null && this.getType() == other.getType();
    }

    @Override
    public boolean isAtomic() {
        return true;
    }
}
