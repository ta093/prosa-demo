package sql2sttgd.data.rules.expression;

public interface InternalExpression {

    boolean isAtomic();

}
