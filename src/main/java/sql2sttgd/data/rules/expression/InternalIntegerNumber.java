package sql2sttgd.data.rules.expression;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import xml.AttributeType;

@Getter
@Setter
@ToString
public class InternalIntegerNumber extends InternalNumber<Integer> {

    @NonNull
    private Integer value;

    @Override
    public AttributeType getType() {
        return AttributeType.INT;
    }

    @Override
    public String getValueAsString() {
        return value.toString();
    }

    public static InternalIntegerNumber one() {
        InternalIntegerNumber one = new InternalIntegerNumber();
        one.setValue(1);
        return one;
    }

}
