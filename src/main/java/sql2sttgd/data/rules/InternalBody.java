package sql2sttgd.data.rules;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.atom.InternalComparison;

import java.util.*;

@Getter
@NoArgsConstructor
@ToString
public class InternalBody {

    private final Map<Integer, InternalBodyAtom> atomsByIndex = new HashMap<>();

    /**
     * List of side table and intermediate table atoms
     */
    private final List<InternalAtom> furtherAtoms = new ArrayList<>();

    private final List<InternalComparison> comparisons = new ArrayList<>();


    public Collection<InternalBodyAtom> getRelationalAtoms() {
        return atomsByIndex.values();
    }

    public void addAtom(@NonNull InternalAtom atom) {
        if (atom instanceof InternalBodyAtom) {
            InternalBodyAtom bodyAtom = (InternalBodyAtom) atom;
            atomsByIndex.put(bodyAtom.getRelationIndex(), bodyAtom);
        } else {
            furtherAtoms.add(atom);
        }
    }

    public void addComparison(@NonNull InternalComparison comparison) {
        comparisons.add(comparison);
    }

    public static InternalBody of(List<InternalAtom> atoms, List<InternalComparison> comparisons) {
        InternalBody body = new InternalBody();
        atoms.forEach(body::addAtom);
        comparisons.forEach(body::addComparison);
        return body;
    }
}
