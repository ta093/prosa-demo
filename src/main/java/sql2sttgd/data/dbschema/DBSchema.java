package sql2sttgd.data.dbschema;

import lombok.Data;
import lombok.ToString;
import xml.Relation;
import xml.Relations;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Internal representation of a database schema.
 * Maps table names to the according table for easier access via the name.
 * The tables themselves are ChaTEAU-XML {@link Relation} objects.
 */
@Data
@ToString
public class DBSchema {

    Map<String, Relation> tables = new HashMap<>();
    Map<String, IntermediateRelation> intermediateTables = new HashMap<>();
    Map<String, Relation> sideTables = new HashMap<>();
    Relation resultRelation;

    /**
     * Add a relation to the database schema.
     * Uses the relation name as key for the underlying map.
     *
     * @param relation relation to be added
     */
    public void putTable(Relation relation) {
        tables.put(relation.getName(), relation);
    }

    /**
     * Add a relation to the database schema as an artificial intermediate table.
     * Uses the relation name as key for the underlying map.
     *
     * @param intermediateRelation intermediate relation to be added
     */
    public void putIntermediateTable(IntermediateRelation intermediateRelation) {
        intermediateTables.put(intermediateRelation.getName(), intermediateRelation);
    }

    /**
     * Add a relation to the database schema as an artificial side table.
     * Uses the relation name as key for the underlying map.
     *
     * @param relation relation to be added
     */
    public void putSideTable(Relation relation) {
        sideTables.put(relation.getName(), relation);
    }

    public boolean hasResultRelation() {
        return resultRelation != null;
    }

    public boolean contains(String relationName) {
        return tables.containsKey(relationName)
                || intermediateTables.containsKey(relationName)
                || sideTables.containsKey(relationName)
                || (hasResultRelation() && resultRelation.getName().equals(relationName));
    }

    /**
     * Build a {@link Relations} object from the database schema
     *
     * @return the ChaTEAU-XML Relations object
     */
    public Relations getRelationsObject() {
        Relations relations = new Relations();
        relations.getRelation().addAll(tables.values());
        relations.getRelation().addAll(intermediateTables.values().stream()
                .map(IntermediateRelation::buildRelationObj)
                .collect(Collectors.toList()));
        relations.getRelation().addAll(sideTables.values());
        relations.getRelation().add(resultRelation);

        return relations;
    }

}
