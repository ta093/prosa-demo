package sql2sttgd.data.dbschema;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import xml.Attribute;
import xml.Relation;

/**
 * Class to represent relations that are not present in the database, but are needed internally.
 * They extend an existing relation with additional attributes, e.g. to add new sequential IDs
 * for later computation of aggregates.
 */
@Getter
@Setter
@AllArgsConstructor
public class IntermediateRelation {

    private Relation originalRelation;

    private String name;

    private final List<Attribute> newAttributes = new ArrayList<>();


    public void addNewAttribute(Attribute attribute) {
        newAttributes.add(attribute);
    }

    public List<Attribute> getAllAttributes() {
        ArrayList<Attribute> allAttributes = new ArrayList<>(newAttributes);
        allAttributes.addAll(originalRelation.getAttribute());
        return allAttributes;
    }

    public Relation buildRelationObj() {
        Relation relation = new Relation();
        relation.setName(name);
        // relation.setTag(Tag.S);
        relation.getAttribute().addAll(newAttributes);
        relation.getAttribute().addAll(originalRelation.getAttribute());

        return relation;
    }
}
