package sql2sttgd.api;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import atom.RelationalAtom;
import instance.SchemaTag;
import jakarta.xml.bind.JAXBException;
import net.sf.jsqlparser.statement.select.Select;
import sql2sttgd.compositor.Compositor;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.dbreader.DatabaseReader;
import sql2sttgd.dbreader.NoColumnsException;
import sql2sttgd.parser.SqlQueryParser;
import sql2sttgd.parser.exception.ParsingException;
import sql2sttgd.serializer.XmlSerializer;
import sql2sttgd.transformation.Transformation;
import sql2sttgd.transformation.exception.TransformationException;
import sql2sttgd.util.QueryUtils;
import term.Term;
import xml.Attribute;
import xml.AttributeType;
import xml.Dependencies;
import xml.Input;
import xml.Instance;
import xml.InstanceAtom;
import xml.Relation;
import xml.Tag;

public class ParserDefaultService implements ParserService {
	private static final String DEFAULT_SCHEMA = "public";

	@Override
	public ByteArrayOutputStream generateXml(Connection dbConn, String query)
			throws NoColumnsException, TransformationException, SQLException, JAXBException, ParsingException {
		return generateXml(dbConn, query, DEFAULT_SCHEMA);
	}

	@Override
	public ByteArrayOutputStream generateXml(Connection dbConn, String query, String schema)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException {

		Select select = SqlQueryParser.parse(query);
		List<String> tables = QueryUtils.readRelevantRelations(select);

		DatabaseReader dbReader = new DatabaseReader(dbConn);
		DBSchema dbSchema = dbReader.readSchema(schema, tables);
		Instance instance = dbReader.readInstance(schema, tables);

		Dependencies dependencies = Transformation.transform(select, dbSchema);
		Input xmlRootElement = Compositor.buildXml(dependencies, dbSchema, instance);

		return XmlSerializer.marshal(xmlRootElement);
	}

	@Override
	public ByteArrayOutputStream generateXmlAfterEvolution(String query,
			instance.Instance evolutionOutput)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException {

		Select select = SqlQueryParser.parse(query);

		DBSchema dbSchema = new DBSchema();
		Instance instance = convertInstance(evolutionOutput);

		// add evolutionOutput schema to dbSchema
		extractTargetRelationsFromInstance(dbSchema, evolutionOutput);

		Dependencies dependencies = Transformation.transform(select, dbSchema);
		Input xmlRootElement = Compositor.buildXml(dependencies, dbSchema, instance);

		return XmlSerializer.marshal(xmlRootElement);
	}

	// can't get database as XML without dependencies -> have to fill dependency
	// because InputReader needs at least one dependency
	@Override
	public ByteArrayOutputStream generateEvolutionXml(Connection dbConn, List<String> tables)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException {

		Select select = SqlQueryParser.parse("SELECT * FROM " + tables.get(0));

		DatabaseReader dbReader = new DatabaseReader(dbConn);
		DBSchema dbSchema = dbReader.readSchema(DEFAULT_SCHEMA, tables);
		Instance instance = dbReader.readInstance(DEFAULT_SCHEMA, tables);

		Dependencies dependencies = Transformation.transform(select, dbSchema);
		Input xmlRootElement = Compositor.buildXml(dependencies, dbSchema, instance);

		return XmlSerializer.marshal(xmlRootElement);
	}

	// We need to extract all relations needed from the evolution Output Instance
	// and add them to the DBSchema, for creating the query as s-t tgd
	public DBSchema extractTargetRelationsFromInstance(DBSchema dbSchema, instance.Instance instance) {
		HashMap<String, SchemaTag> schemaTags = instance.getSchemaTags();
		for (Entry<String, LinkedHashMap<String, String>> schema : instance.getSchema().entrySet()) {

			// Only use Target tagged schema (others are empty anyways)
			if (schemaTags.get(schema.getKey()).getToken() == "T") {
				// create Relation, needed for DBSchema
				Relation rel = new Relation();
				rel.setName(schema.getKey());
				rel.setTag(Tag.fromValue("S"));

				// extract attributes and convert them for relation
				List<Attribute> attributes = rel.getAttribute();

				for (Entry<String, String> attribute : schema.getValue().entrySet()) {
					Attribute newAttribute = new Attribute();
					newAttribute.setName(attribute.getKey());
					newAttribute.setType(AttributeType.fromValue(attribute.getValue()));
					attributes.add(newAttribute);
				}

				dbSchema.putTable(rel);
			}
		}

		return dbSchema;
	}

	// extract instance.Instance of evolutionOutput and convert to xml.Instance
	public xml.Instance convertInstance(instance.Instance evolutionOutput) {

		xml.Instance convertedInstance = new xml.Instance();
		int i = 1;
		for (RelationalAtom relAtom : evolutionOutput.getRelationalAtoms()) {

			InstanceAtom convertedAtom = new InstanceAtom();
			convertedAtom.setName(relAtom.getRelationName());
			convertedAtom.setId(relAtom.getRelationName() + "_" + i);

			for (Term term : relAtom.getTerms()) {

				xml.Constant xmlConstant = new xml.Constant();

				xmlConstant.setName(term.getName());
				xmlConstant.setValue(term.toString().replace("\"", ""));

				convertedAtom.getConstant().add(xmlConstant);
			}

			convertedInstance.getAtom().add(convertedAtom);
			i++;
		}

		return convertedInstance;

	}

}
