package sql2sttgd.api;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import jakarta.xml.bind.JAXBException;
import sql2sttgd.dbreader.NoColumnsException;
import sql2sttgd.parser.exception.ParsingException;
import sql2sttgd.transformation.exception.TransformationException;

public interface ParserService {
	/**
	 * Generate a ChaTEAU-XML input file from the given DB connection and SQL query
	 * in a specific DB schema.
	 * <p>
	 * The SQL query is used for finding the relevant schema and tables as well as
	 * producing s-t tgds for the CHASE algorithm.
	 * </p>
	 *
	 * @param dbConn Connection to DB containing the schema and tables used in the
	 *               SQL query
	 * @param schema Database schema name
	 * @param query  SQL query to be converted into ChaTEAU-XML
	 * @return ChaTEAU-XML input file
	 * @throws NoColumnsException if the schema or tables in the SQL query are
	 *                            missing or do not contain any columns
	 * @throws SQLException       if a database access error occurs
	 */
	ByteArrayOutputStream generateXml(Connection dbConn, String query, String schema)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException;

	/**
	 * Generate a ChaTEAU-XML input file from the given DB connection and SQL query
	 * using the default schema.
	 * <p>
	 * The SQL query is used for finding the relevant schema and tables as well as
	 * producing s-t tgds for the CHASE algorithm.
	 * </p>
	 *
	 * @param dbConn Connection to DB containing the schema and tables used in the
	 *               SQL query
	 * @param query  SQL query to be converted into ChaTEAU-XML
	 * @return ChaTEAU-XML input file
	 * @throws NoColumnsException if the schema or tables in the SQL query are
	 *                            missing or do not contain any columns
	 * @throws SQLException       if a database access error occurs
	 */
	ByteArrayOutputStream generateXml(Connection dbConn, String query)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException;

	ByteArrayOutputStream generateEvolutionXml(Connection dbConn, List<String> tables)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException;

	ByteArrayOutputStream generateXmlAfterEvolution(String query, instance.Instance evolutionOutput)
			throws NoColumnsException, SQLException, TransformationException, JAXBException, ParsingException;
}
