package sql2sttgd.examples;

import xml.*;
import jakarta.xml.bind.*;

import java.io.InputStream;

public class JaxbExample {

    public static void main(String[] args) throws JAXBException {
        System.out.println("Jaxb Example!");

        JAXBContext jaxbContext = JAXBContext.newInstance(Input.class);
        ObjectFactory inputFactory = new ObjectFactory();
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Marshaller marshaller = jaxbContext.createMarshaller();

        // Unmarshal and remarshal a provided example
        InputStream xmlIs = JaxbExample.class.getResourceAsStream("/xml/example_1c_I_STTGD.xml");
        Input xml = (Input) unmarshaller.unmarshal(xmlIs);
        marshaller.marshal(xml, System.out);
        System.out.println();

        // Build some input and marshal it
        Input someInput = inputFactory.createInput();

        Attribute att1 = inputFactory.createAttribute();
        att1.setName("1");
        att1.setType(AttributeType.INT);

        Attribute att2 = inputFactory.createAttribute();
        att2.setName("2.0");
        att2.setType(AttributeType.DOUBLE);

        Attribute att3 = inputFactory.createAttribute();
        att3.setName("DREI");
        att3.setType(AttributeType.STRING);

        Relation relationA = inputFactory.createRelation();
        relationA.setName("A");
        relationA.setTag(Tag.S);

        relationA.getAttribute().add(att1);
        relationA.getAttribute().add(att2);
        relationA.getAttribute().add(att3);

        Relations relations = inputFactory.createRelations();
        relations.getRelation().add(relationA);

        Schema schema = inputFactory.createSchema();
        schema.setRelations(relations);
        someInput.setSchema(schema);

        marshaller.marshal(someInput, System.out);
        System.out.println();
    }
}
