package sql2sttgd.examples;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JDBCExample {
    private static final String DB_URL = "jdbc:postgresql:";
    private static final String DB_SCHEMA = "example_1c";
    private static final String DB_TABLE = "Students";

    public static void main(String[] args) throws SQLException {
        Connection dbConn = DriverManager.getConnection(DB_URL);
        System.out.println("Opened Connection to " + DB_URL);
        DatabaseMetaData dbMeta = dbConn.getMetaData();

        ResultSet dbSchemas = dbMeta.getSchemas();
        ArrayList<String> schemas = new ArrayList<>();
        while (dbSchemas.next())
            schemas.add(dbSchemas.getString("TABLE_SCHEM"));
        if (schemas.contains(DB_SCHEMA))
            System.out.println("Found Schema " + DB_SCHEMA + " in DB");
        else
            throw new IllegalArgumentException("Schema " + DB_SCHEMA + " not found in DB");

        String[] tableTypes = {"TABLE"}; // only show actual tables
        ResultSet dbSchemaTables = dbMeta.getTables(null, DB_SCHEMA, "%", tableTypes);
        ArrayList<String> schemaTables = new ArrayList<>();
        while (dbSchemaTables.next())
            schemaTables.add(dbSchemaTables.getString("TABLE_NAME"));
        System.out.println("Tables in " + DB_SCHEMA + ": " + schemaTables);

        ResultSet dbSchemaColumns = dbMeta.getColumns(null, DB_SCHEMA, "%", "%");
        Map<String, ArrayList<String>> tableColumns = new HashMap<>();
        for (String table : schemaTables)
            tableColumns.put(table, new ArrayList<>());
        while (dbSchemaColumns.next())
            tableColumns.get(dbSchemaColumns.getString("TABLE_NAME")).add(dbSchemaColumns.getString("COLUMN_NAME")
                    + " (" + dbSchemaColumns.getString("TYPE_NAME") + ")");
        for (String k : tableColumns.keySet())
            System.out.println("Columns in " + k + ": " + tableColumns.get(k));

        // better make sure DB_SCHEMA and DB_TABLE are trusted because PreparedStatements do not work with table names
        Statement stmt = dbConn.createStatement();
        ResultSet rSet = stmt.executeQuery("SELECT * FROM " + DB_SCHEMA + '.' + '"' + DB_TABLE + '"');

        ArrayList<String> row = new ArrayList<>();
        while (rSet.next()) {
            row.clear();
            for (int i = 1; i <= rSet.getMetaData().getColumnCount(); i++)
                row.add(rSet.getString(i));
            System.out.println("Row in " + DB_TABLE + ": " + row);
        }

        dbConn.close();
    }
}
