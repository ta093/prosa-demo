package sql2sttgd.examples;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.Select;
import sql2sttgd.util.QueryUtils;

public class JSqlParserExample {
    public static void main(String[] args) throws JSQLParserException {
        QueryUtils.readRelevantRelations(selectEverything());
        QueryUtils.readRelevantRelations(selectSome());
        QueryUtils.readRelevantRelations(selectWhere());
        QueryUtils.readRelevantRelations(selectJoin());
        QueryUtils.readRelevantRelations(selectOldJoin());
    }

    public static Select selectEverything() throws JSQLParserException {
        String sql = "SELECT * FROM table";
        return (Select) CCJSqlParserUtil.parse(sql);
    }

    public static Select selectSome() throws JSQLParserException {
        String sql = "SELECT a,b,c FROM table";
        return (Select) CCJSqlParserUtil.parse(sql);
    }

    public static Select selectWhere() throws JSQLParserException {
        String sql = "SELECT * FROM table WHERE a = 1 AND b = 'hello'";
        return (Select) CCJSqlParserUtil.parse(sql);
    }

    public static Select selectJoin() throws JSQLParserException {
        String sql = "SELECT * FROM table1 NATURAL JOIN table2 WHERE x = 1";
        return (Select) CCJSqlParserUtil.parse(sql);
    }

    public static Select selectOldJoin() throws JSQLParserException {
        String sql = "SELECT * FROM table1, table2 WHERE table1.x = table2.x";
        return (Select) CCJSqlParserUtil.parse(sql);
    }
}