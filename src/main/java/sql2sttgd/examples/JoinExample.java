package sql2sttgd.examples;

import jakarta.xml.bind.JAXBException;
import sql2sttgd.parser.exception.ParsingException;
import sql2sttgd.api.ParserDefaultService;
import sql2sttgd.transformation.exception.TransformationException;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JoinExample {
    public static void main(String[] args)
            throws SQLException, TransformationException, JAXBException, ParsingException {
        Connection dbConn = DriverManager.getConnection("jdbc:postgresql:");
        String schema = "exam_results2";
        String query = "SELECT *" +
                " FROM studierende NATURAL JOIN noten" +
                " JOIN faecher ON (noten.fachnr = faecher.fachnr)" +
                " CROSS JOIN raeume";

        ParserDefaultService sql2sttgd = new ParserDefaultService();
        ByteArrayOutputStream xmlStream = sql2sttgd.generateXml(dbConn, query, schema);
        System.out.println(xmlStream.toString());
    }
}