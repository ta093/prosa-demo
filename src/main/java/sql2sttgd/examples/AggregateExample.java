package sql2sttgd.examples;

import lombok.SneakyThrows;
import sql2sttgd.api.ParserDefaultService;
import sql2sttgd.parser.exception.ParsingException;

import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;

public class AggregateExample {

    // private static final String MIN_QUERY = "SELECT MIN(age) FROM students";
    // private static final String COUNT_QUERY = "SELECT COUNT(*) FROM students";
    // private static final String SUM_QUERY = "SELECT SUM(age) FROM students";
    // private static final String AVG_QUERY = "SELECT AVG(age) FROM students";
    private static final String UNION_QUERY = "SELECT SUM(b) FROM r1 UNION SELECT SUM(b) FROM r1";

    public static void main(String[] args) throws ParsingException {
        full();
    }

    @SneakyThrows
    private static void full() {
        Connection dbConn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ba_example", "ivo",
                "Pe*At@RXc9");

        ParserDefaultService sql2sttgd = new ParserDefaultService();
        ByteArrayOutputStream xmlStream = sql2sttgd.generateXml(dbConn, UNION_QUERY);
        // System.out.println(xmlStream.toString());
        FileWriter fileWriter = new FileWriter("target/aggregate_example.xml");
        fileWriter.write(xmlStream.toString());
        fileWriter.close();
    }

}
