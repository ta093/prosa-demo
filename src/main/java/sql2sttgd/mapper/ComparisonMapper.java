package sql2sttgd.mapper;

import sql2sttgd.data.rules.atom.InternalComparison;
import sql2sttgd.data.rules.expression.InternalExpression;
import sql2sttgd.data.rules.expression.InternalFunction;
import sql2sttgd.data.rules.expression.InternalNumber;
import sql2sttgd.data.rules.variable.InternalVariable;
import xml.Comparison;
import xml.Expression;
import xml.Function;
import xml.Number;

public class ComparisonMapper {

    public static Comparison mapComparison(InternalComparison internalComparison) {
        Comparison comparison = new Comparison();
        comparison.setOperator(internalComparison.getOperator());
        comparison.setLeft(mapComparisonLeft(internalComparison.getLeft()));
        comparison.setRight(mapExpression(internalComparison.getRight()));

        return comparison;
    }

    private static Comparison.Left mapComparisonLeft(InternalVariable<?> internalVariable) {
        Comparison.Left left = new Comparison.Left();
        left.setVariable(VariableMapper.mapVariable(internalVariable));

        return left;
    }

    public static Expression mapExpression(InternalExpression internalExpr) {
        Expression expression = new Expression();

        if (internalExpr instanceof InternalNumber) {
            expression.setNumber(mapNumber((InternalNumber<?>) internalExpr));
        } else if (internalExpr instanceof InternalVariable) {
            expression.setVariable(VariableMapper.mapVariable((InternalVariable<?>) internalExpr));
        } else if (internalExpr instanceof InternalFunction) {
            expression.setFunction(mapFunction((InternalFunction) internalExpr));
        } else {
            throw new IllegalStateException("Unknown expression type: " + internalExpr.toString());
        }

        return expression;
    }

    public static Number mapNumber(InternalNumber<?> internalConst) {
        Number number = new Number();
        number.setValue(internalConst.getValueAsString());

        return number;
    }

    public static Function mapFunction(InternalFunction internalFunction) {
        Function function = new Function();
        function.setOperator(internalFunction.getOperator());
        function.setLeft(mapExpression(internalFunction.getLeft()));
        function.setRight(mapExpression(internalFunction.getRight()));

        return function;
    }
}
