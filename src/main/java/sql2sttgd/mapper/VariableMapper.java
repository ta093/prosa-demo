package sql2sttgd.mapper;

import sql2sttgd.data.rules.variable.InternalVariable;
import xml.*;

public class VariableMapper {

    public static void mapVariableByType(Atom atom, InternalVariable<?> variable) {
        if (variable.isConstant()) {
            if (variable.getConstantValue() == null) {
                // TODO: currently this part is never reached because variable.isConstant() only checks whether
                //  constantValue is not Null
                //  -> need to change InternalVariable to allow nulls (maybe a isNull flag?)
                atom.getNull().add(mapNull(variable));
            } else {
                atom.getConstant().add(mapConstant(variable));
            }
        } else {
            atom.getVariable().add(mapVariable(variable));
        }
    }

    /**
     * Maps a variable to its xml representation.
     *
     * @param variable The {@link InternalVariable} to map.
     * @return A new {@link Variable}.
     */
    public static Variable mapVariable(InternalVariable<?> variable) {
        Variable result = new Variable();
        result.setIndex(variable.getIndex());
        result.setType(Quantifier.V);
        result.setName(variable.getName());
        return result;
    }

    /**
     * Maps a constant to its xml representation.
     *
     * @param variable The {@link InternalVariable} to map.
     * @return A new {@link Constant}.
     */
    public static Constant mapConstant(InternalVariable<?> variable) {
        Constant result = new Constant();
        // assumes that name of the variable is equal to column name
        result.setName(variable.getName());
        result.setValue(variable.getConstantValue().toString());
        return result;
    }

    /**
     * Maps a null value to its xml representation.
     *
     * @param variable The {@link InternalVariable} to map.
     * @return A new {@link Null Null value}.
     */
    public static Null mapNull(InternalVariable<?> variable) {
        Null nullValue = new Null();
        nullValue.setIndex(variable.getIndex());
        nullValue.setName(variable.getName());
        return nullValue;
    }

}
