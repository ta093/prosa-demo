package sql2sttgd.mapper;

import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import xml.Atom;

public class AtomMapper {

    public static Atom mapAtom(InternalAtom atom) {
        Atom result = new Atom();
        result.setName(atom.getName());
        result.setNegation(atom.isNegated());

        for (InternalVariable<?> variable : atom.getVariables()) {
            VariableMapper.mapVariableByType(result, variable);
        }

        return result;
    }

}
