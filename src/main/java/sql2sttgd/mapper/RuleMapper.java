package sql2sttgd.mapper;

import java.util.List;

import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.InternalHead;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.atom.InternalComparison;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import xml.AtomsAndComparisons;
import xml.Comparisons;
import xml.Dependencies;
import xml.Rule;

public class RuleMapper {

    public static Dependencies mapRules(List<InternalRule> rules) {
        Dependencies dependencies = new Dependencies();

        for (InternalRule rule : rules) {
            switch (rule.getRuleType()) {
                case TGD:
                    dependencies.getTgd().add(mapRule(rule));
                    break;
                case ST_TGD:
                    dependencies.getSttgd().add(mapRule(rule));
                    break;
            }
        }

        return dependencies;
    }

    public static Rule mapRule(InternalRule internalRule) {
        Rule rule = new Rule();

        rule.setBody(mapBody(internalRule.getBody()));
        rule.setHead(mapHead(internalRule.getHead()));

        return rule;
    }

    public static Dependencies mapSimpleSttgd(InternalResultAtom resultAtom, InternalBody body) {
        Rule sttgd = new Rule();

        sttgd.setHead(mapResultAtomToHead(resultAtom));
        sttgd.setBody(mapBody(body));

        Dependencies dependencies = new Dependencies();
        dependencies.getSttgd().add(sttgd);

        return dependencies;
    }

    public static AtomsAndComparisons mapResultAtomToHead(InternalResultAtom resultAtom) {
        AtomsAndComparisons result = new AtomsAndComparisons();

        result.getAtom().add(AtomMapper.mapAtom(resultAtom));

        return result;
    }

    public static AtomsAndComparisons mapHead(InternalHead head) {
        AtomsAndComparisons result = new AtomsAndComparisons();
        Comparisons resultComparisons = new Comparisons();

        for (InternalAtom atom : head.getAtoms()) {
            result.getAtom().add(AtomMapper.mapAtom(atom));
        }

        for (InternalComparison comparison : head.getComparisons()) {
            resultComparisons.getComparison().add(ComparisonMapper.mapComparison(comparison));
        }

        result.setComparisons(resultComparisons);

        return result;
    }

    public static AtomsAndComparisons mapBody(InternalBody body) {
        AtomsAndComparisons result = new AtomsAndComparisons();
        Comparisons resultComparisons = new Comparisons();

        for (InternalBodyAtom atom : body.getRelationalAtoms()) {
            result.getAtom().add(AtomMapper.mapAtom(atom));
        }

        for (InternalAtom atom : body.getFurtherAtoms()) {
            result.getAtom().add(AtomMapper.mapAtom(atom));
        }

        for (InternalComparison comparison : body.getComparisons()) {
            resultComparisons.getComparison().add(ComparisonMapper.mapComparison(comparison));
        }

        result.setComparisons(resultComparisons);

        return result;
    }

}
