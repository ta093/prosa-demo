package sql2sttgd.parser;

import lombok.NoArgsConstructor;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SetOperationList;
import sql2sttgd.parser.exception.NotAQueryException;
import sql2sttgd.parser.exception.UnsupportedQueryException;

@NoArgsConstructor
public class QueryValidator {

    static Select validate(Statement statement) throws UnsupportedQueryException {
        Select select = validateThatQuery(statement);
        validateThatAllowedSelectBody(select);

        return select;
    }

    static Select validateThatQuery(Statement statement) throws NotAQueryException {
        if (statement instanceof Select) {
            return (Select) statement;
        } else {
            throw new NotAQueryException(statement);
        }
    }

    static void validateThatAllowedSelectBody(Select select) throws UnsupportedQueryException {
        SelectBody selectBody = select.getSelectBody();

        if (!(selectBody instanceof PlainSelect || selectBody instanceof SetOperationList)) {
            throw new UnsupportedQueryException("Unsupported select body type", select);
        }
    }
}
