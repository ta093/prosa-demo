package sql2sttgd.parser;

import lombok.NonNull;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;
import sql2sttgd.parser.exception.JSqlParserWrapperException;
import sql2sttgd.parser.exception.ParsingException;

public class SqlQueryParser {

    public static Select parse(@NonNull String sql) throws ParsingException {
        // TODO: further null handling?
        Statement statement = parseUsingWrapperException(sql);

        return QueryValidator.validate(statement);
    }

    private static Statement parseUsingWrapperException(String sql) throws ParsingException {
        try {
            return CCJSqlParserUtil.parse(sql);
        } catch (JSQLParserException e) {
            throw new JSqlParserWrapperException(e);
        }
    }
}
