package sql2sttgd.parser.exception;

import net.sf.jsqlparser.statement.Statement;

/**
 * Exception thrown when it is tried to enter SQL-Statements other then SELECT
 * (e.g. ALTER or DELETE are not allowed).
 */
public class NotAQueryException extends UnsupportedQueryException {

    public NotAQueryException(Statement statement) {
        super("Only SQL-Queries are allowed", statement);
    }
}
