package sql2sttgd.parser.exception;

import net.sf.jsqlparser.statement.Statement;

/**
 * Exception thrown when the SQL statement could be parsed (i.e. it is syntactically correct),
 * but it is not an allowed query (because it can't be transformed into an s-t tgd).
 */
public class UnsupportedQueryException extends ParsingException {

    public UnsupportedQueryException(String message, Statement statement) {
        super(message + ": " + statement.toString());
    }

}
