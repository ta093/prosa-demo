package sql2sttgd.parser.exception;

/**
 * Class for wrapping {@link net.sf.jsqlparser.JSQLParserException} to our own exceptions.
 */
public class JSqlParserWrapperException extends ParsingException {

    public JSqlParserWrapperException(Throwable cause) {
        super(cause);
    }

}
