package sql2sttgd.parser.exception;

/**
 * Top-level exception thrown when any problem occurs while trying to parse the SQL query string.
 */
public abstract class ParsingException extends Exception {

    protected ParsingException(String message) {
        super(message);
    }

    protected ParsingException(Throwable cause) {
        super(cause);
    }

}
