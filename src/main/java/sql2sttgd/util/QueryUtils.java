package sql2sttgd.util;

import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SetOperationList;

import java.util.ArrayList;
import java.util.List;

public class QueryUtils {

    /**
     * TODO: consider a table occurring multiple times because a table may be joined with itself
     *  else tables will also occur multiple times in schema and instance
     * Reads the DB relations that are relevant for a query,
     * i.e. all the relations that occur in it.
     *
     * @param select the parsed SQL query
     * @return a list of relevant table names
     */
    public static List<String> readRelevantRelations(Select select) {
        List<String> tableNames = new ArrayList<>();
        if (select.getSelectBody() instanceof PlainSelect) {
            PlainSelect plainSelect = select.getSelectBody(PlainSelect.class);
            tableNames.add(plainSelect.getFromItem().toString());

            if (plainSelect.getJoins() != null) {
                plainSelect.getJoins().forEach(join -> tableNames.add(join.getRightItem().toString()));
            }

        } else if (select.getSelectBody() instanceof SetOperationList) {
            SetOperationList union = select.getSelectBody(SetOperationList.class);
            for (SelectBody selectBody : union.getSelects()) {
                Select subSelect = new Select();
                subSelect.setSelectBody(selectBody);

                List<String> subTableNames = readRelevantRelations(subSelect);
                tableNames.addAll(subTableNames);
            }
        }
        return tableNames;
    }

}
