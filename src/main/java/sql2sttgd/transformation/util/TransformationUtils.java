package sql2sttgd.transformation.util;

import net.sf.jsqlparser.schema.Column;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import xml.Attribute;
import xml.Dependencies;
import xml.Relation;

import java.util.ArrayList;
import java.util.List;

public class TransformationUtils {

    /**
     * Traverse an {@link InternalBody} object to find the Variable that corresponds to the given names.
     *
     * @param body       the search space
     * @param tableName  name of the table in the source schema.
     *                   If the tableName is null the Variable will be searched in all Atoms.
     * @param columnName name of the column in the table in the source schema
     * @return variable that represents the column or null if not found
     */
    public static InternalVariable<?> getVariableByName(InternalBody body, String tableName, String columnName) {
        for (InternalBodyAtom atom : body.getRelationalAtoms())
            if (tableName == null || atom.getName().equals(tableName))
                for (InternalVariable<?> variable : atom.getVariables())
                    if (variable.getName().equals(columnName))
                        return variable;

        return null;
    }

    public static InternalVariable<?> getVariableByName(InternalBody body, Column column) {
        String tableName = column.getTable() != null ? column.getTable().getName() : null;
        String columnName = column.getColumnName();

        return getVariableByName(body, tableName, columnName);
    }

    public static InternalVariable<?> getVariableByNameInAtom(InternalAtom atom, String columnName) {
        for (InternalVariable<?> variable : atom.getVariables())
            if (variable.getName().equals(columnName))
                return variable;

        return null;
    }

    public static List<InternalVariable<?>> getAllVariablesByName(InternalBody body, Column column) {
        String tableName = column.getTable() != null ? column.getTable().getName() : null;
        String columnName = column.getColumnName();

        ArrayList<InternalVariable<?>> variables = new ArrayList<>();

        for (InternalBodyAtom atom : body.getRelationalAtoms())
            if (tableName == null || atom.getName().equals(tableName))
                for (InternalVariable<?> variable : atom.getVariables())
                    if (variable.getName().equals(columnName))
                        variables.add(variable);

        return variables;
    }

    public static void replaceVariable(InternalBody body, InternalVariable<?> oldVariable,
                                       InternalVariable<?> newVariable) {
        InternalBodyAtom atom = body.getAtomsByIndex().get(oldVariable.getIndex());
        replaceVariable(atom, oldVariable, newVariable);
    }

    public static void replaceVariable(InternalAtom atom, InternalVariable<?> oldVariable,
                                       InternalVariable<?> newVariable) {
        List<InternalVariable<?>> atomVariables = atom.getVariables();
        for (int i = 0; i < atomVariables.size(); i++) {
            InternalVariable<?> variable = atomVariables.get(i);
            if (variable.equals(oldVariable)) {
                atomVariables.set(i, newVariable);
            }
        }
    }

    public static void mergeDependencies(Dependencies deps1, Dependencies deps2) {
        deps1.getSttgd().addAll(deps2.getSttgd());
        deps1.getTgd().addAll(deps2.getTgd());
        deps1.getEgd().addAll(deps2.getEgd());
    }

    public static Attribute getAttribute(Relation relation, String attributeName) {
        return relation.getAttribute().stream()
                .filter(attribute -> attribute.getName().equals(attributeName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Relation '" + relation.getName() +
                        "' does not contain attribute '" + attributeName + "'"));
    }

}
