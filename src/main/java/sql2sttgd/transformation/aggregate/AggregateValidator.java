package sql2sttgd.transformation.aggregate;

import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.commons.lang3.NotImplementedException;

import java.util.List;

public class AggregateValidator {

    public static void noOtherOperations(PlainSelect query) {
        if (query.getJoins() != null
                || query.getWhere() != null
                || query.getGroupBy() != null) {
            throw new NotImplementedException("Currently no other operation is supported next to aggregates");
        }
    }

    public static void onlyOneAggregate(List<Function> functions) {
        if (functions.size() != 1) {
            throw new NotImplementedException("Currently only one aggregate is allowed");
        }
    }

}
