package sql2sttgd.transformation.aggregate.rulegen;

import java.util.Collections;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.dbschema.IntermediateRelation;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.InternalHead;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.data.rules.RuleType;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.atom.InternalComparison;
import sql2sttgd.data.rules.atom.InternalIntermediateTableAtom;
import sql2sttgd.data.rules.atom.InternalSideTableAtom;
import sql2sttgd.data.rules.expression.InternalFunction;
import sql2sttgd.data.rules.variable.InternalIntegerVariable;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.AtomBuilder;
import sql2sttgd.transformation.aggregate.AggregateHelper;
import sql2sttgd.transformation.util.TransformationUtils;
import xml.Attribute;
import xml.AttributeType;
import xml.Relation;

public class IdRuleGenerator {

    public static final String ID_ATTRIBUTE_NAME = "sequential_id";
    public static final String SORTED_TABLE_SUFFIX = "_sorted";

    static IdRuleGenerator.Result generateIdRules(DBSchema dbSchema, PlainSelect query) {
        String relationName = RuleGeneratorUtils.readTableName(query);
        IntermediateRelation sortedRelation = buildSortedRelation(dbSchema, relationName);

        if (!dbSchema.contains(sortedRelation.getName())) {
            dbSchema.putIntermediateTable(sortedRelation);
        } else {
            // ID Rules were already generated previously for another atomic query
            // with an aggregate on the same table
            return Result.builder()
                    .rules(Collections.emptyList())
                    .sortedRelation(sortedRelation)
                    .build();
        }

        InternalRule rule1 = generateFirst(dbSchema, relationName, sortedRelation);
        InternalRule rule2 = generateSecond(dbSchema, relationName, sortedRelation);

        return IdRuleGenerator.Result.builder()
                .rules(List.of(rule1, rule2))
                .sortedRelation(sortedRelation)
                .build();
    }

    @NonNull
    private static InternalRule generateFirst(DBSchema dbSchema, String relationName,
            IntermediateRelation sortedRelation) {
        // R(...terms)
        InternalBodyAtom atom1 = AtomBuilder.buildBodyAtom(dbSchema, relationName, 1);

        // !-R_S(x, ...terms)
        InternalIntermediateTableAtom atom2 = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 2);
        atom2.setNegated(true);
        RuleGeneratorUtils.getIdVariable(atom2).setIndex(1);

        // body
        InternalBody body = InternalBody.of(
                List.of(atom1, atom2),
                Collections.emptyList());

        // R_S(1, ...terms)
        InternalIntermediateTableAtom headAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);
        InternalIntegerVariable headIdVariable = RuleGeneratorUtils.getIdVariable(headAtom);
        headIdVariable.setConstantValue(1);

        // R''(...terms)
        Relation relation = new Relation();
        relation.getAttribute().addAll(atom1.getRelation().getAttribute());
        relation.setName("R''");

        dbSchema.putTable(relation);

        InternalBodyAtom atom3 = AtomBuilder.buildBodyAtom(dbSchema, "R''", 1);

        // head
        InternalHead head = new InternalHead();
        head.addAtom(headAtom);
        head.addAtom(atom3);

        return new InternalRule(body, head, RuleType.TGD);
    }

    @NonNull
    private static InternalRule generateSecond(DBSchema dbSchema, String relationName,
            IntermediateRelation sortedRelation) {
        // R(...terms)
        InternalBodyAtom bodyAtom = AtomBuilder.buildBodyAtom(dbSchema, relationName, 1);

        // R_S(x, ...terms)
        InternalIntermediateTableAtom sortedBodyAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 2);

        // !-R''(...terms)
        InternalBodyAtom sideTableAtom1 = AtomBuilder.buildBodyAtom(dbSchema, "R''", 2);
        sideTableAtom1.getVariables().forEach(variable -> variable.setIndex(1));
        sideTableAtom1.setNegated(true);

        // !-R'(x)
        InternalIntegerVariable idVariable = RuleGeneratorUtils.getIdVariable(sortedBodyAtom);
        idVariable.setIndex(1);

        Relation sideTable = AggregateHelper.createSideTable("R'", idVariable);
        dbSchema.putSideTable(sideTable);

        InternalSideTableAtom sideTableAtom2 = new InternalSideTableAtom(sideTable);
        sideTableAtom2.addVariable(idVariable);
        sideTableAtom2.setNegated(true);

        // body
        InternalBody body = new InternalBody();
        body.addAtom(bodyAtom);
        body.addAtom(sortedBodyAtom);
        body.addAtom(sideTableAtom1);
        body.addAtom(sideTableAtom2);

        // RS(x+1, a1, b1, t1)
        InternalIntermediateTableAtom headAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);
        InternalIntegerVariable idVariableHead = RuleGeneratorUtils.getIdVariable(headAtom);

        InternalVariable<?> accVariable = idVariableHead;
        accVariable.setIndex(accVariable.getIndex() + 1);

        TransformationUtils.replaceVariable(headAtom, idVariableHead, accVariable);

        InternalFunction idVarPlusOne = InternalFunction.plusOne(idVariable);
        InternalComparison comparison = InternalComparison.equal(accVariable, idVarPlusOne);

        // R'(x)
        InternalSideTableAtom sideTableAtom3 = new InternalSideTableAtom(sideTable);
        sideTableAtom3.addVariable(idVariable);

        // R''(...terms)
        InternalBodyAtom sideTableAtom4 = AtomBuilder.buildBodyAtom(dbSchema, "R''", 1);

        // head
        InternalHead head = new InternalHead();
        head.addAtom(headAtom);
        head.addAtom(sideTableAtom3);
        head.addAtom(sideTableAtom4);

        head.addComparison(comparison);

        return new InternalRule(body, head, RuleType.TGD);
    }

    private static IntermediateRelation buildSortedRelation(DBSchema dbSchema, String originalRelationName) {
        Relation relation = dbSchema.getTables().get(originalRelationName);
        String sortedName = relation.getName() + SORTED_TABLE_SUFFIX;
        IntermediateRelation sortedRelation = new IntermediateRelation(relation, sortedName);

        Attribute idAttribute = new Attribute();
        idAttribute.setType(AttributeType.INT);
        idAttribute.setName(ID_ATTRIBUTE_NAME);
        sortedRelation.addNewAttribute(idAttribute);

        return sortedRelation;
    }

    /**
     * Simple data class that is returned in {@link IdRuleGenerator}.
     * Contains the generated rules and the new intermediate relation (extended by
     * sequential IDs).
     */
    @Data
    @Builder
    static class Result {
        List<InternalRule> rules;
        IntermediateRelation sortedRelation;
    }
}
