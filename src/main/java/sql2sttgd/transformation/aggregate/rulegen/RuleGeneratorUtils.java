package sql2sttgd.transformation.aggregate.rulegen;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.rules.atom.InternalAtom;
import sql2sttgd.data.rules.variable.InternalIntegerVariable;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.exception.TransformationException;
import sql2sttgd.transformation.util.TransformationUtils;

import java.util.List;

public class RuleGeneratorUtils {

    static String readTableName(PlainSelect query) {
        return query.getFromItem().toString();
    }

    static InternalVariable<?> getAggregateVariable(InternalAtom atom, Aggregate aggregate)
            throws TransformationException {
        String columnName = readColumnName(aggregate);
        InternalVariable<?> aggregateVariable = TransformationUtils.getVariableByNameInAtom(atom, columnName);

        if (aggregateVariable == null) {
            throw new TransformationException("Unable to find column '" + columnName + "' in table '"
                    + atom.getName() + "'");
        } else if (!aggregateVariable.isNumeric()) {
            throw new TransformationException("Aggregate can't be applied to column '" + columnName
                    + "' because it is not numeric");
        } else {
            return aggregateVariable;
        }
    }

    static InternalIntegerVariable getIdVariable(InternalAtom atom) {
        InternalVariable<?> idVariable = TransformationUtils.getVariableByNameInAtom(atom, IdRuleGenerator.ID_ATTRIBUTE_NAME);

        if (idVariable instanceof InternalIntegerVariable) {
            return (InternalIntegerVariable) idVariable;
        } else {
            throw new IllegalStateException("Variable for sequential ID must always have type Integer: " + idVariable);
        }
    }

    static String readColumnName(Aggregate aggregate) throws TransformationException {
        List<Expression> parameters = aggregate.getFunction().getParameters().getExpressions();

        String functionName = aggregate.getFunction().toString();
        if (parameters.size() != 1) {
            throw new TransformationException("No or too many parameters in function: " + functionName);
        } else if (!(parameters.get(0) instanceof Column)) {
            throw new TransformationException("Only columns allowed as parameter of an aggregate: " + functionName);
        } else {
            return ((Column) parameters.get(0)).getColumnName();
        }
    }
}
