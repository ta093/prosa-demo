package sql2sttgd.transformation.aggregate.rulegen;

import lombok.Builder;
import lombok.Data;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.commons.lang3.NotImplementedException;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.exception.TransformationException;
import xml.Relation;

import java.util.List;

public class AggregateRuleGenerator {

    public static AggregateRuleGenerator.Result generateRules(DBSchema dbSchema, PlainSelect query, int queryIndex,
            Aggregate aggregate) throws TransformationException {

        switch (aggregate.getType()) {
            case MIN:
                return MinMaxRuleGenerator.generateMinRules(dbSchema, query, aggregate);
            case MAX:
                return MinMaxRuleGenerator.generateMaxRules(dbSchema, query, aggregate);
            case SUM:
                return SumAvgRuleGenerator.generateSumRules(dbSchema, query, queryIndex, aggregate);
            case AVG:
                return SumAvgRuleGenerator.generateAvgRules(dbSchema, query, queryIndex, aggregate);
            case COUNT:
                return CountRuleGenerator.generateCountRules(dbSchema, query);
            default:
                throw new NotImplementedException("Aggregate type not implemented: " + aggregate.getType());
        }
    }

    /**
     * Simple data class that is returned in by the different Rule Generator
     * classes.
     * Contains the generated rules and the new result relation (later added to the
     * DB schema).
     */
    @Data
    @Builder
    public static class Result {
        List<InternalRule> rules;
        Relation resultRelation;
    }

}
