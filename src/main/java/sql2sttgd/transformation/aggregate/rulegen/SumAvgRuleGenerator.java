package sql2sttgd.transformation.aggregate.rulegen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import lombok.NonNull;
import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.dbschema.IntermediateRelation;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.InternalHead;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.data.rules.RuleType;
import sql2sttgd.data.rules.atom.InternalComparison;
import sql2sttgd.data.rules.atom.InternalIntermediateTableAtom;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import sql2sttgd.data.rules.atom.InternalSideTableAtom;
import sql2sttgd.data.rules.expression.InternalFunction;
import sql2sttgd.data.rules.variable.InternalDoubleVariable;
import sql2sttgd.data.rules.variable.InternalIntegerVariable;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.AtomBuilder;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.exception.TransformationException;
import sql2sttgd.transformation.util.TransformationUtils;
import xml.Attribute;
import xml.AttributeType;
import xml.Relation;
import xml.Tag;

public class SumAvgRuleGenerator {

    public static final String SIDE_TABLE_PREFIX = "H_";

    public static String accumulatorAttributeName = "accumulator";

    static AggregateRuleGenerator.Result generateSumRules(DBSchema dbSchema, PlainSelect query, int queryIndex,
            Aggregate aggregate) throws TransformationException {
        return generateSumAvgRules(dbSchema, query, queryIndex, aggregate, true);
    }

    static AggregateRuleGenerator.Result generateAvgRules(DBSchema dbSchema, PlainSelect query, int queryIndex,
            Aggregate aggregate) throws TransformationException {
        return generateSumAvgRules(dbSchema, query, queryIndex, aggregate, false);
    }

    static AggregateRuleGenerator.Result generateSumAvgRules(DBSchema dbSchema, PlainSelect query, int queryIndex,
            Aggregate aggregate, boolean isSum)
            throws TransformationException {
        IdRuleGenerator.Result idRuleGenResult = IdRuleGenerator.generateIdRules(dbSchema, query);
        IntermediateRelation sortedRelation = idRuleGenResult.getSortedRelation();

        InternalIntermediateTableAtom atom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);
        accumulatorAttributeName = RuleGeneratorUtils.getAggregateVariable(atom, aggregate).getName();
        // H(c, x)
        Relation sideTable = addSideTableToSchema(dbSchema, query, queryIndex, aggregate);

        InternalRule rule1 = generateFirst(sortedRelation, aggregate, sideTable, dbSchema);
        InternalRule rule2 = generateSecond(sortedRelation, aggregate, sideTable, dbSchema);
        Pair<InternalRule, Relation> resultPair = generateFinal(sideTable, dbSchema, isSum);

        List<InternalRule> rules = new ArrayList<>(idRuleGenResult.getRules());
        rules.addAll(List.of(rule1, rule2, resultPair.getLeft()));

        return AggregateRuleGenerator.Result.builder()
                .rules(rules)
                .resultRelation(resultPair.getRight())
                .build();
    }

    @NonNull
    private static InternalRule generateFirst(IntermediateRelation sortedRelation, Aggregate aggregate,
            Relation sideTable, DBSchema dbSchema) throws TransformationException {
        // R_S(1, ...terms)
        InternalIntermediateTableAtom bodyAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);
        InternalIntegerVariable idVariable = RuleGeneratorUtils.getIdVariable(bodyAtom);
        idVariable.setConstantValue(1);

        InternalVariable<?> aggregateVariable = RuleGeneratorUtils.getAggregateVariable(bodyAtom, aggregate);

        // body
        InternalBody body = InternalBody.of(
                List.of(bodyAtom),
                Collections.emptyList());

        // H(b, 1)
        InternalSideTableAtom sideTableAtom = new InternalSideTableAtom(sideTable);
        sideTableAtom.addVariable(aggregateVariable);
        sideTableAtom.addVariable(idVariable);

        // head
        InternalHead head = InternalHead.of(sideTableAtom);

        return new InternalRule(body, head, RuleType.TGD);
    }

    @NonNull
    private static InternalRule generateSecond(IntermediateRelation sortedRelation, Aggregate aggregate,
            Relation sideTable, DBSchema dbSchema)
            throws TransformationException {
        Attribute accAttribute = TransformationUtils.getAttribute(sideTable, accumulatorAttributeName);

        // R_S(x, ...terms)
        InternalIntermediateTableAtom sortedBodyAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);
        InternalIntegerVariable idVariable = RuleGeneratorUtils.getIdVariable(sortedBodyAtom);
        InternalVariable<?> aggregateVariable = RuleGeneratorUtils.getAggregateVariable(sortedBodyAtom, aggregate);

        // !-H(d, x)
        InternalSideTableAtom sideTableAtom2 = AtomBuilder.buildSideTableAtom(sideTable, 1);
        sideTableAtom2.setNegated(true);

        // H(c, x-1)
        InternalSideTableAtom sideTableAtom1 = AtomBuilder.buildSideTableAtom(sideTable, 2);
        InternalVariable<?> accVariable2 = getAccumulatorVariable(sideTableAtom1);
        InternalIntegerVariable idVariable2 = RuleGeneratorUtils.getIdVariable(sideTableAtom1);

        InternalFunction idVarMinusOne = InternalFunction.minusOne(idVariable);
        InternalComparison comparison1 = InternalComparison.equal(idVariable2, idVarMinusOne);

        // body
        InternalBody body = InternalBody.of(
                List.of(sortedBodyAtom, sideTableAtom1, sideTableAtom2),
                List.of(comparison1));

        // H(b+c, x)
        InternalVariable<?> accVariable3 = InternalVariable.of(accAttribute, 3);
        InternalSideTableAtom sideTableAtomHead = new InternalSideTableAtom(sideTable);
        sideTableAtomHead.addVariable(accVariable3);
        sideTableAtomHead.addVariable(idVariable);

        InternalFunction newAccSum = InternalFunction.plus(aggregateVariable, accVariable2);
        InternalComparison comparison2 = InternalComparison.equal(accVariable3, newAccSum);

        // head
        InternalHead head = new InternalHead();
        head.addAtom(sideTableAtomHead);
        head.addComparison(comparison2);

        return new InternalRule(body, head, RuleType.TGD);
    }

    private static Pair<InternalRule, Relation> generateFinal(Relation sideTable, DBSchema dbSchema, boolean isSum) {
        // H(c, x)
        InternalSideTableAtom atom1 = AtomBuilder.buildSideTableAtom(sideTable, 1);
        InternalVariable<?> sumVariable = getAccumulatorVariable(atom1);
        InternalIntegerVariable idVariable = RuleGeneratorUtils.getIdVariable(atom1);

        // !-R'(x)
        Relation sideTable2 = dbSchema.getSideTables().get("R'");

        InternalSideTableAtom atom2 = new InternalSideTableAtom(sideTable2);
        atom2.addVariable(idVariable);
        atom2.setNegated(true);

        // body
        InternalBody body = InternalBody.of(
                List.of(atom1, atom2),
                Collections.emptyList());

        // res(...)
        InternalResultAtom resultAtom = new InternalResultAtom();
        InternalHead head = new InternalHead();
        if (isSum) {
            resultAtom.addVariable(sumVariable);
        } else {
            InternalDoubleVariable resultVariable = new InternalDoubleVariable(accumulatorAttributeName, 0, null);
            resultAtom.addVariable(resultVariable);
            InternalFunction divide = InternalFunction.divide(sumVariable, idVariable);
            head.addComparison(InternalComparison.equal(resultVariable, divide));
        }

        head.addAtom(resultAtom);

        return Pair.of(
                new InternalRule(body, head, RuleType.ST_TGD),
                resultAtom.buildResultRelation(sumVariable.getName()));
    }

    private static Relation addSideTableToSchema(DBSchema dbSchema, PlainSelect query, int queryIndex,
            Aggregate aggregate) throws TransformationException {
        Relation sideTable = new Relation();
        // make side table unique with the queryIndex
        // to prevent conflicts when there is a UNION in the query
        sideTable.setName(SIDE_TABLE_PREFIX + queryIndex);
        sideTable.setTag(Tag.S);

        AttributeType attributeType = getAggregateAttributeType(dbSchema, query, aggregate);
        Attribute aggregateAttribute2 = new Attribute();
        aggregateAttribute2.setName(accumulatorAttributeName);
        aggregateAttribute2.setType(attributeType);
        sideTable.getAttribute().add(aggregateAttribute2);

        Attribute idAttribute = new Attribute();
        idAttribute.setName(IdRuleGenerator.ID_ATTRIBUTE_NAME);
        idAttribute.setType(AttributeType.INT);
        sideTable.getAttribute().add(idAttribute);

        dbSchema.putSideTable(sideTable);
        return sideTable;
    }

    private static AttributeType getAggregateAttributeType(DBSchema dbSchema, PlainSelect query, Aggregate aggregate)
            throws TransformationException {
        String tableName = RuleGeneratorUtils.readTableName(query);
        String aggregateColumnName = RuleGeneratorUtils.readColumnName(aggregate);
        Relation relation = dbSchema.getTables().get(tableName);
        Attribute aggregateAttribute = TransformationUtils.getAttribute(relation, aggregateColumnName);
        return aggregateAttribute.getType();
    }

    private static InternalVariable<?> getAccumulatorVariable(InternalSideTableAtom atom) {
        return atom.getVariables().stream()
                .filter(variable -> variable.getName().equals(accumulatorAttributeName))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Side table atom '" + atom +
                        "' does not have a variable for accumulating values"));
    }

}
