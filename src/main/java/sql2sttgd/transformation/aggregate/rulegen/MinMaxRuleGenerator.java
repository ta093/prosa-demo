package sql2sttgd.transformation.aggregate.rulegen;

import java.util.List;

import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.InternalHead;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.data.rules.RuleType;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.atom.InternalComparison;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import sql2sttgd.data.rules.atom.InternalSideTableAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.AtomBuilder;
import sql2sttgd.transformation.aggregate.AggregateHelper;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.exception.TransformationException;
import xml.ComparisonOperators;
import xml.Relation;

public class MinMaxRuleGenerator {

    static AggregateRuleGenerator.Result generateMinRules(DBSchema dbSchema, PlainSelect query, Aggregate aggregate)
            throws TransformationException {
        return generateMinOrMaxRules(dbSchema, query, aggregate, true);
    }

    static AggregateRuleGenerator.Result generateMaxRules(DBSchema dbSchema, PlainSelect query, Aggregate aggregate)
            throws TransformationException {
        return generateMinOrMaxRules(dbSchema, query, aggregate, false);
    }

    // TODO refactor
    private static AggregateRuleGenerator.Result generateMinOrMaxRules(DBSchema dbSchema, PlainSelect query,
            Aggregate aggregate, boolean isMin)
            throws TransformationException {
        String relationName = RuleGeneratorUtils.readTableName(query);

        // tgd
        InternalBodyAtom atom1 = AtomBuilder.buildBodyAtom(dbSchema, relationName, 1);
        InternalBodyAtom atom2 = AtomBuilder.buildBodyAtom(dbSchema, relationName, 2);

        InternalVariable<?> leftCompVariable = RuleGeneratorUtils.getAggregateVariable(atom1, aggregate);
        InternalVariable<?> rightCompVariable = RuleGeneratorUtils.getAggregateVariable(atom2, aggregate);

        ComparisonOperators operator = isMin ? ComparisonOperators.GT : ComparisonOperators.LT;
        InternalComparison comparison = new InternalComparison(operator, leftCompVariable, rightCompVariable);

        InternalBody tgdBody = InternalBody.of(List.of(atom1, atom2), List.of(comparison));

        Relation sideTable = AggregateHelper.createSideTable("H", leftCompVariable);
        dbSchema.putSideTable(sideTable);

        InternalSideTableAtom sideTableAtom1 = new InternalSideTableAtom(sideTable);
        sideTableAtom1.addVariable(leftCompVariable);

        InternalRule rule1 = new InternalRule(tgdBody, InternalHead.of(sideTableAtom1), RuleType.TGD);

        InternalResultAtom tgdResultAtom = new InternalResultAtom();
        tgdResultAtom.addVariable(leftCompVariable);

        // s-t tgd
        InternalSideTableAtom sideTableAtom2 = new InternalSideTableAtom(sideTable);
        sideTableAtom2.addVariable(leftCompVariable);
        sideTableAtom2.setNegated(true);

        InternalBody sttgdBody = InternalBody.of(List.of(atom1, sideTableAtom2), List.of());

        InternalResultAtom sttgdResultAtom = new InternalResultAtom();
        sttgdResultAtom.addVariable(leftCompVariable);

        InternalRule rule2 = new InternalRule(sttgdBody, InternalHead.of(sttgdResultAtom), RuleType.ST_TGD);

        return AggregateRuleGenerator.Result.builder()
                .rules(List.of(rule1, rule2))
                .resultRelation(sttgdResultAtom.buildResultRelation(leftCompVariable.getName()))
                .build();
    }

}
