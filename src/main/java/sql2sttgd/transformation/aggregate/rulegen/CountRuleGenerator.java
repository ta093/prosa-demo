package sql2sttgd.transformation.aggregate.rulegen;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.dbschema.IntermediateRelation;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.InternalHead;
import sql2sttgd.data.rules.InternalRule;
import sql2sttgd.data.rules.RuleType;
import sql2sttgd.data.rules.atom.InternalIntermediateTableAtom;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import sql2sttgd.data.rules.atom.InternalSideTableAtom;
import sql2sttgd.data.rules.variable.InternalIntegerVariable;
import sql2sttgd.transformation.AtomBuilder;
import xml.Relation;

public class CountRuleGenerator {

    static AggregateRuleGenerator.Result generateCountRules(DBSchema dbSchema, PlainSelect query) {
        IdRuleGenerator.Result idRuleGenResult = IdRuleGenerator.generateIdRules(dbSchema, query);
        IntermediateRelation sortedRelation = idRuleGenResult.getSortedRelation();

        // R_S(x, ...terms)
        InternalIntermediateTableAtom bodyAtom = AtomBuilder.buildIntermediateTableAtom(sortedRelation, 1);

        InternalIntegerVariable idVariable = RuleGeneratorUtils.getIdVariable(bodyAtom);

        // !-R'(x)
        Relation sideTable = dbSchema.getSideTables().get("R'");

        InternalSideTableAtom sideTableAtom = new InternalSideTableAtom(sideTable);
        sideTableAtom.addVariable(idVariable);
        sideTableAtom.setNegated(true);

        // body
        InternalBody body = new InternalBody();
        body.addAtom(bodyAtom);
        body.addAtom(sideTableAtom);

        // Result(x)
        InternalResultAtom resultAtom = new InternalResultAtom();
        resultAtom.addVariable(idVariable); // hard coded index!

        // head and final s-t tgd
        List<InternalRule> rules = new ArrayList<>(idRuleGenResult.getRules());
        InternalRule sttgd = new InternalRule(body, InternalHead.of(resultAtom), RuleType.ST_TGD);
        rules.add(sttgd);

        return AggregateRuleGenerator.Result.builder()
                .rules(rules)
                .resultRelation(resultAtom.buildResultRelation(idVariable.getName()))
                .build();
    }

}
