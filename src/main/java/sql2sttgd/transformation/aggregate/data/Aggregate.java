package sql2sttgd.transformation.aggregate.data;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import net.sf.jsqlparser.expression.Function;

/**
 * Wrapper for the JSqlParser {@link Function} class.
 * Type of the function can later be easier accessed.
 */
@Data
@Builder
@ToString
public class Aggregate {

    Function function;
    AggregateType type;

}
