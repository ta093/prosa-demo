package sql2sttgd.transformation.aggregate.data;

import java.util.Arrays;
import java.util.Locale;

public enum AggregateType {

    MIN, MAX, SUM, COUNT, AVG;

    public static boolean isSupported(String functionName) {
        return Arrays.stream(values())
                .anyMatch(type -> type.name().equalsIgnoreCase(functionName));
    }

    public static AggregateType valueOfIgnoreCase(String name) {
        return AggregateType.valueOf(name.toUpperCase(Locale.ROOT));
    }

}
