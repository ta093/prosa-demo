package sql2sttgd.transformation.aggregate;

import java.util.List;
import java.util.stream.Collectors;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.aggregate.data.AggregateType;
import sql2sttgd.transformation.exception.TransformationException;
import xml.Attribute;
import xml.Relation;

public class AggregateHelper {

    /**
     * Checks whether an aggregate occurs in the query.
     *
     * @param query the query to check
     * @return true if at least one of the items in the SELECT-clause is an
     *         aggregate, false otherwise
     */
    public static boolean containsAggregate(PlainSelect query) {
        return query.getSelectItems().stream()
                .anyMatch(AggregateHelper::selectItemIsAggregate);
    }

    public static boolean containsOnlyAggregates(PlainSelect query) {
        return query.getSelectItems().stream()
                .allMatch(AggregateHelper::selectItemIsAggregate);
    }

    private static boolean selectItemIsAggregate(SelectItem selectItem) {
        if (!(selectItem instanceof SelectExpressionItem)) {
            return false;
        }

        Expression expression = ((SelectExpressionItem) selectItem).getExpression();
        if (!(expression instanceof Function)) {
            return false;
        }

        String functionName = ((Function) expression).getName();
        return AggregateType.isSupported(functionName);
    }

    public static Aggregate readAggregate(PlainSelect query) throws TransformationException {
        if (containsOnlyAggregates(query)) {
            List<Function> functions = mapToFunctions(query.getSelectItems());

            // those checks may be removed later if further functionality is added
            AggregateValidator.noOtherOperations(query);
            AggregateValidator.onlyOneAggregate(functions);

            Function function = functions.get(0);
            return Aggregate.builder()
                    .function(function)
                    .type(AggregateType.valueOfIgnoreCase(function.getName()))
                    .build();
        } else {
            throw new TransformationException("No other columns are allowed next to aggregates in the SELECT-clause");
        }
    }

    public static Relation createSideTable(String name, InternalVariable<?> variable) {
        Relation sideTable = new Relation();
        sideTable.setName(name);

        Attribute sideTableAttribute = new Attribute();

        sideTableAttribute.setName(variable.getName());
        sideTableAttribute.setType(variable.getType());

        sideTable.getAttribute().add(sideTableAttribute);
        return sideTable;
    }

    private static List<Function> mapToFunctions(List<SelectItem> selectItems) {
        return selectItems.stream()
                .map(SelectExpressionItem.class::cast)
                .map(SelectExpressionItem::getExpression)
                .map(Function.class::cast)
                .collect(Collectors.toList());
    }

}
