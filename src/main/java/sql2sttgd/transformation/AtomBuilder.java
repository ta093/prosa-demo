package sql2sttgd.transformation;

import lombok.NonNull;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.*;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.dbschema.IntermediateRelation;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.*;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.exception.TransformationException;
import sql2sttgd.util.QueryUtils;
import xml.Attribute;
import xml.Relation;

import java.util.List;

import static sql2sttgd.transformation.util.TransformationUtils.getAllVariablesByName;

public class AtomBuilder {

    public static InternalBody buildBody(DBSchema dbSchema, PlainSelect query) {
        InternalBody body = new InternalBody();

        Select select = new Select();
        select.setSelectBody(query);
        List<String> usedRelations = QueryUtils.readRelevantRelations(select);

        // column names/attributes have to be unique per table and the index makes them
        // unique across all tables
        // this way all variables are unique
        int relationIndex = 1;
        for (String relationName : usedRelations) {
            body.addAtom(buildBodyAtom(dbSchema, relationName, relationIndex));
            relationIndex++;
        }
        return body;
    }

    public static InternalBodyAtom buildBodyAtom(DBSchema dbSchema, String relationName, int relationIndex) {
        Relation relation = dbSchema.getTables().get(relationName);
        InternalBodyAtom bodyAtom = new InternalBodyAtom(relation, relationIndex);
        addVariablesFromAttributes(bodyAtom, relation.getAttribute(), relationIndex);
        return bodyAtom;
    }

    public static InternalIntermediateTableAtom buildIntermediateTableAtom(IntermediateRelation intermediateRelation,
            int defaultIndex) {
        InternalIntermediateTableAtom intermediateTableAtom = new InternalIntermediateTableAtom(intermediateRelation);
        addVariablesFromAttributes(intermediateTableAtom, intermediateRelation.getAllAttributes(), defaultIndex);
        return intermediateTableAtom;
    }

    public static InternalSideTableAtom buildSideTableAtom(Relation sideTable, int defaultIndex) {
        InternalSideTableAtom sideTableAtom = new InternalSideTableAtom(sideTable);
        addVariablesFromAttributes(sideTableAtom, sideTable.getAttribute(), defaultIndex);
        return sideTableAtom;
    }

    private static void addVariablesFromAttributes(InternalAtom atom, List<Attribute> attributes, int index) {
        for (Attribute attribute : attributes) {
            InternalVariable<?> variable = InternalVariable.of(attribute, index);
            atom.addVariable(variable);
        }
    }

    public static InternalResultAtom buildResultAtom(InternalBody body, @NonNull List<SelectItem> selectItems,
            List<InternalVariable<?>> resultSchema)
            throws TransformationException {
        InternalResultAtom resultAtom = new InternalResultAtom();

        for (var selectItem : selectItems) {
            if (selectItem instanceof SelectExpressionItem) {
                Expression selectLine = ((SelectExpressionItem) selectItem).getExpression();
                if (selectLine instanceof Column) {
                    Column column = (Column) selectLine;
                    List<InternalVariable<?>> variables = getAllVariablesByName(body, column);

                    if (column.getTable() == null && variables.size() > 1) {
                        // TODO: This is not always the case. It is allowed to have no table given if
                        // the variable
                        // is a join variable. See TransformationIT.simpleJoin() for an example.
                        // We need to fix this after having dealt with JOIN ON and SELECT *.
                        throw new TransformationException("Incorrect SQL Query. The select body had an attribute"
                                + " which existed in multiple tables, but no specific table was specified.");
                    } else if (variables.size() < 1) {
                        throw new TransformationException("The attribute " + column.getColumnName() + " inside the "
                                + "select expression does not exist in any specified table.");
                    }

                    resultAtom.addVariable(variables.get(0));
                } else {
                    throw new UnsupportedOperationException(
                            "Aggregations and other Expressions are not allowed inside the select body.");
                }
            } else if (selectItem instanceof AllColumns) {
                resultAtom.getVariables().addAll(resultSchema);
                break; // ensures that variables are not added twice, even though it should be
                // unnecessary
            } else if (selectItem instanceof AllTableColumns) {
                throw new UnsupportedOperationException("Select of type '<table>.*' not yet implemented");
            } else {
                throw new IllegalArgumentException();
            }
        }

        return resultAtom;
    }

}
