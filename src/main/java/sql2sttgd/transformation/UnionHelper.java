package sql2sttgd.transformation;

import net.sf.jsqlparser.statement.select.*;
import org.apache.commons.lang3.NotImplementedException;
import sql2sttgd.transformation.exception.TransformationException;
import xml.Attribute;
import xml.Relation;

import java.util.ArrayList;
import java.util.List;

public class UnionHelper {

    protected static List<PlainSelect> getSelectsFromUnion(Select sqlQuery) {
        List<PlainSelect> plainSelects = new ArrayList<>();
        SetOperationList setOperations = sqlQuery.getSelectBody(SetOperationList.class);

        if (setOperations.getOperations().stream()
                .anyMatch(setOperation -> !(setOperation instanceof UnionOp))) {
            throw new NotImplementedException("Other set operations next to UNION are not supported");
        }

        List<SelectBody> selectBodies = setOperations.getSelects();
        for (var selectBody : selectBodies) {
            if (selectBody instanceof PlainSelect) {
                plainSelects.add((PlainSelect) selectBody);
            } else if (selectBody instanceof SetOperationList) {
                Select nestedUnion = new Select();
                nestedUnion.setSelectBody(selectBody);
                plainSelects.addAll(getSelectsFromUnion(nestedUnion));
            }
        }
        return plainSelects;
    }

    public static void assertRelationEquality(Relation resultRelation, Relation resultRelation2)
            throws TransformationException {
        List<Attribute> attributes = resultRelation.getAttribute();
        List<Attribute> attributes2 = resultRelation2.getAttribute();

        if (attributes.size() != attributes2.size()) {
            throw new TransformationException("Unions on tables with different result schemas not allowed.");
        }
        for (int i = 0; i < attributes.size(); i++) {
            Attribute a1 = attributes.get(i);
            Attribute a2 = attributes2.get(i);

            if (a1.getType() != a2.getType()) {
                throw new TransformationException("Unions on tables with different result schemas not allowed.");
            }
        }
    }
}
