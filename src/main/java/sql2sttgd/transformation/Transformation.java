package sql2sttgd.transformation;

import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SetOperationList;
import org.apache.commons.lang3.tuple.Pair;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.mapper.RuleMapper;
import sql2sttgd.transformation.aggregate.AggregateHelper;
import sql2sttgd.transformation.aggregate.data.Aggregate;
import sql2sttgd.transformation.aggregate.rulegen.AggregateRuleGenerator;
import sql2sttgd.transformation.exception.TransformationException;
import sql2sttgd.transformation.util.TransformationUtils;
import xml.Dependencies;
import xml.Relation;

import java.util.ArrayList;
import java.util.List;

public class Transformation {

    public static Dependencies transform(Select sqlQuery, DBSchema dbSchema) throws TransformationException {
        Dependencies rules = new Dependencies();
        List<PlainSelect> plainSelects = new ArrayList<>();

        boolean isUnion = sqlQuery.getSelectBody() instanceof SetOperationList;
        if (isUnion) {
            plainSelects = UnionHelper.getSelectsFromUnion(sqlQuery);
        } else {
            plainSelects.add(sqlQuery.getSelectBody(PlainSelect.class));
        }

        for (int i = 0; i < plainSelects.size(); i++) {
            PlainSelect plainSelect = plainSelects.get(i);
            Pair<Dependencies, Relation> trafoResult = transformAtomicQuery(plainSelect, i, dbSchema);

            if (!dbSchema.hasResultRelation()) {
                dbSchema.setResultRelation(trafoResult.getRight());
            } else {
                // assert that all s-t tgds have the same result relation as their head
                Relation curResultRelation = trafoResult.getRight();
                UnionHelper.assertRelationEquality(dbSchema.getResultRelation(), curResultRelation);
            }

            TransformationUtils.mergeDependencies(rules, trafoResult.getLeft());
        }

        return rules;
    }

    public static Pair<Dependencies, Relation> transformAtomicQuery(PlainSelect atomicQuery, int queryIndex,
                                                                    DBSchema dbSchema) throws TransformationException {
        if (AggregateHelper.containsAggregate(atomicQuery)) {
            Aggregate aggregate = AggregateHelper.readAggregate(atomicQuery);
            AggregateRuleGenerator.Result result =
                    AggregateRuleGenerator.generateRules(dbSchema, atomicQuery, queryIndex, aggregate);

            Dependencies dependencies = RuleMapper.mapRules(result.getRules());
            return Pair.of(dependencies, result.getResultRelation());
        } else {
            InternalBody body = AtomBuilder.buildBody(dbSchema, atomicQuery);
            List<InternalVariable<?>> resultSchema = EqualizeVariables.equalizeVariables(body, atomicQuery);
            InternalResultAtom resultAtom = AtomBuilder.buildResultAtom(body, atomicQuery.getSelectItems(), resultSchema);
            InsertConstants.insertConstants(resultAtom, body, atomicQuery);

            Dependencies dependencies = RuleMapper.mapSimpleSttgd(resultAtom, body);
            return Pair.of(dependencies, resultAtom.buildResultRelation());
        }
    }

}
