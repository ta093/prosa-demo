package sql2sttgd.transformation.exception;


/**
 * Exception thrown when there was a problem while transforming the sql query into a st-tgd
 */
public class TransformationException extends Exception {

    public TransformationException(String message) {
        super(message);
    }

    protected TransformationException(Throwable cause) {
        super(cause);
    }
}
