package sql2sttgd.transformation;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.commons.lang3.NotImplementedException;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.data.JoinRelation;
import sql2sttgd.transformation.exception.TransformationException;

import java.util.List;

import static sql2sttgd.transformation.util.TransformationUtils.replaceVariable;

public class EqualizeVariables {

    public static List<InternalVariable<?>> equalizeVariables(InternalBody body, PlainSelect plainSelect)
            throws TransformationException {
        JoinRelation joinRelation = buildJoinRelation(plainSelect, body);

        // if there are no joins, there is nothing to do here
        if (plainSelect.getJoins() != null) {
            updateBody(body, joinRelation);
        }

        return joinRelation.getEqualizedResultSchema();
    }

    private static JoinRelation buildJoinRelation(PlainSelect plainSelect, InternalBody body)
            throws TransformationException {
        JoinRelation joinRelation = new JoinRelation(body);
        joinRelation.addInitial(resolveAtom(plainSelect.getFromItem(), body));

        if (plainSelect.getJoins() != null) {
            for (Join join : plainSelect.getJoins()) {
                InternalBodyAtom atom = resolveAtom(join.getRightItem(), body);
                if (join.isNatural()) {
                    joinRelation.addNatural(atom);
                } else if (isInnerJoin(join)) {
                    joinRelation.addInner(atom, join.getOnExpression());
                } else if (isCrossProduct(join)) {
                    joinRelation.addCrossProduct(atom);
                } else {
                    throw new NotImplementedException("Joins other than natural and inner joins are not supported.");
                }
            }
        }
        return joinRelation;
    }

    private static InternalBodyAtom resolveAtom(FromItem fromItem, InternalBody body) throws TransformationException {
        if (!(fromItem instanceof Table)) {
            throw new NotImplementedException(
                    "The right item in a join must be a table, but is something else.");
        }

        String tableName = ((Table) fromItem).getName();

        for (InternalBodyAtom atom : body.getRelationalAtoms()) {
            if (atom.getName().equals(tableName)) {
                return atom;
            }
        }

        throw new TransformationException("Cannot find a relation with the name '" + tableName + "' in schema.");
    }

    static boolean isInnerJoin(Join join) {
        if (join.getOnExpression() == null) {
            return false;
        } else if (join.isInner()) {
            return true;
        } else {
            // if all flags are false the join can still be an inner join (but without the optional 'INNER' keyword)
            return !join.isNatural() && !join.isCross() && !join.isSimple() && !join.isFull() && !join.isLeft() &&
                    !join.isRight() && !join.isOuter() && !join.isSemi();
        }
    }

    static boolean isCrossProduct(Join join) {
        return (join.isSimple() || join.isCross()) &&
                !(join.isNatural() || join.isInner() || join.isFull() || join.isLeft() ||
                        join.isRight() || join.isOuter() || join.isSemi() || join.isStraight() || join.isApply());
    }

    public static void updateBody(InternalBody body, JoinRelation joinRelation) {
        for (List<InternalVariable<?>> equalVariables : joinRelation.buildEquivalences()) {
            InternalVariable<?> representative = equalVariables.get(0);

            equalVariables.listIterator(1).forEachRemaining(variableToReplace ->
                    replaceVariable(body, variableToReplace, representative));
        }
    }

}
