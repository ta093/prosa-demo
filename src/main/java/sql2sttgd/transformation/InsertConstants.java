package sql2sttgd.transformation;

import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.PlainSelect;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.InternalResultAtom;
import sql2sttgd.data.rules.variable.InternalDoubleVariable;
import sql2sttgd.data.rules.variable.InternalIntegerVariable;
import sql2sttgd.data.rules.variable.InternalStringVariable;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.exception.TransformationException;

import java.util.ArrayList;

import static sql2sttgd.transformation.util.TransformationUtils.getVariableByName;
import static sql2sttgd.transformation.util.TransformationUtils.getVariableByNameInAtom;

public class InsertConstants {

    public static void insertConstants(InternalResultAtom resultAtom, InternalBody body, PlainSelect plainSelect)
            throws TransformationException {

        Expression where = plainSelect.getWhere();
        if (where == null) {
            return;
        }

        ArrayList<EqualsTo> assignsInQuery = getAssignsInQuery(where);

        for (var assignment : assignsInQuery) {
            Column column = (Column) assignment.getLeftExpression();
            Expression value = assignment.getRightExpression();

            InternalVariable<?> variable = getVariableByName(body, column);

            if (variable == null) {
                variable = getVariableByNameInAtom(resultAtom, column.getColumnName());
                if (variable == null) {
                    throw new TransformationException("Unknown column in query: " + column);
                }
            }

            setValueInVariable(variable, value, assignment);
        }
    }

    private static ArrayList<EqualsTo> getAssignsInQuery(Expression where) throws TransformationException {
        ArrayList<EqualsTo> assertions = new ArrayList<>();

        if (where instanceof AndExpression) {
            assertions.addAll(getAssignsInQuery(((AndExpression) where).getLeftExpression()));
            assertions.addAll(getAssignsInQuery(((AndExpression) where).getRightExpression()));
        } else if (where instanceof EqualsTo) {
            EqualsTo equalsTo = (EqualsTo) where;
            Expression left = equalsTo.getLeftExpression();
            Expression right = equalsTo.getRightExpression();

            if (left instanceof Column ^ right instanceof Column) {
                if (right instanceof Column) {
                    EqualsTo swapped = new EqualsTo();
                    swapped.setRightExpression(left);
                    swapped.setLeftExpression(right);
                    assertions.add(swapped);
                } else {
                    assertions.add(equalsTo);
                }
            }
        } else {
            throw new TransformationException("Unsupported value type in where expression. " +
                    "Only Integer-, Double- and String-like types are supported.");
        }

        return assertions;
    }

    private static boolean isSupportedType(Expression value) {
        return value instanceof LongValue
                || value instanceof DoubleValue
                || value instanceof StringValue;
    }

    private static void setValueInVariable(InternalVariable<?> variable, Expression value, Expression assignment)
            throws TransformationException {
        if (!isSupportedType(value)) {
            throw new TransformationException("Unsupported value type in assignment: " + assignment);
        }

        if (value instanceof LongValue && variable instanceof InternalIntegerVariable) {
            int intValue = (int) ((LongValue) value).getValue();
            ((InternalIntegerVariable) variable).setConstantValue(intValue);
        } else if (value instanceof DoubleValue && variable instanceof InternalDoubleVariable) {
            double doubleValue = ((DoubleValue) value).getValue();
            ((InternalDoubleVariable) variable).setConstantValue(doubleValue);
        } else if (value instanceof StringValue && variable instanceof InternalStringVariable) {
            String stringValue = ((StringValue) value).getValue();
            ((InternalStringVariable) variable).setConstantValue(stringValue);
        } else {
            throw new TransformationException("Value type does not match column of type " + variable.getType()
                    + " in assignment '" + assignment + "'");
        }
    }

}
