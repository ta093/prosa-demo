package sql2sttgd.transformation.data;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import org.apache.commons.lang3.NotImplementedException;
import sql2sttgd.data.rules.InternalBody;
import sql2sttgd.data.rules.atom.InternalBodyAtom;
import sql2sttgd.data.rules.variable.InternalVariable;
import sql2sttgd.transformation.exception.TransformationException;
import xml.AttributeType;

import java.util.*;

/**
 * Data structure to represent the relation schema of a natural join
 */
@RequiredArgsConstructor
public class JoinRelation {

    @Getter
    private final List<InternalVariable<?>> resultSchema = new ArrayList<>();

    private final ImmutableGraph.Builder<InternalVariable<?>> equivalences = GraphBuilder
            .undirected()
            .immutable();

    private final InternalBody body;


    public void addInitial(InternalBodyAtom atom) {
        atom.getVariables().forEach(equivalences::addNode);
        resultSchema.addAll(atom.getVariables());
    }

    public void addNatural(InternalBodyAtom atom) {
        for (InternalVariable<?> variableToAdd : atom.getVariables()) {
            InternalVariable<?> schemaVariable = findVariableInSchema(variableToAdd);

            if (schemaVariable != null) {
                equivalences.putEdge(schemaVariable, variableToAdd);
            } else {
                equivalences.addNode(variableToAdd);
                resultSchema.add(variableToAdd);
            }
        }
    }

    private InternalVariable<?> findVariableInSchema(InternalVariable<?> variable) {
        return resultSchema.stream()
                .filter(schemaVariable -> schemaVariable.getName().equals(variable.getName())
                        && schemaVariable.hasSameTypeAs(variable))
                .findFirst().orElse(null);
    }

    public void addInner(InternalBodyAtom atom, Expression onExpression) throws TransformationException {
        atom.getVariables().forEach(equivalences::addNode);
        extractEquivalences(atom, onExpression);
        resultSchema.addAll(atom.getVariables());
    }

    public void addCrossProduct(InternalBodyAtom atom) {
        atom.getVariables().forEach(equivalences::addNode);
        resultSchema.addAll(atom.getVariables());
    }

    private void extractEquivalences(InternalBodyAtom atom, Expression expression) throws TransformationException {
        if (expression instanceof EqualsTo) {
            EqualsTo equalsTo = (EqualsTo) expression;
            Expression left = equalsTo.getLeftExpression();
            Expression right = equalsTo.getRightExpression();

            if (left instanceof Column && right instanceof Column) {
                // check that one column is already present in resultSchema and the other in the atom to add
                casesForVariableFromAtomAndSchema(atom, (Column) left, (Column) right);
            } else {
                throw new TransformationException("Both items in an equalsTo expression have to be columns, " +
                        "but are not: " + expression);
            }
        } else if (expression instanceof AndExpression) {
            // recurse for left and right
            AndExpression andExpression = (AndExpression) expression;
            extractEquivalences(atom, andExpression.getLeftExpression());
            extractEquivalences(atom, andExpression.getRightExpression());
        } else if (expression instanceof Parenthesis) {
            // simply recurse for inner expression
            Parenthesis parenthesisExpression = (Parenthesis) expression;
            extractEquivalences(atom, parenthesisExpression.getExpression());
        } else {
            throw new NotImplementedException("This type of join on expression is not (yet) supported: " +
                    expression.toString());
        }
    }

    private void casesForVariableFromAtomAndSchema(InternalBodyAtom atom, Column leftColumn, Column rightColumn)
            throws TransformationException {
        InternalVariable<?> leftVariableFromAtom = findVariableInAtom(atom, leftColumn);
        InternalVariable<?> rightVariableFromAtom = findVariableInAtom(atom, rightColumn);

        if (leftVariableFromAtom == null && rightVariableFromAtom != null) {
            InternalVariable<?> leftVariableFromSchema = findVariableInSchema(leftColumn, rightVariableFromAtom.getType());
            equivalences.putEdge(leftVariableFromSchema, rightVariableFromAtom);
        } else if (leftVariableFromAtom != null && rightVariableFromAtom == null) {
            InternalVariable<?> rightVariableFromSchema = findVariableInSchema(rightColumn, leftVariableFromAtom.getType());
            equivalences.putEdge(leftVariableFromAtom, rightVariableFromSchema);
        } else if (leftVariableFromAtom != null) {
            throw new TransformationException("There is a join on expression defining equality between two " +
                    "columns, but both of them are part of the relation on the right side of the join.");
        } else {
            throw new TransformationException("There is a join on expression defining equality between two " +
                    "columns, but none of them are part of the relation on the right side of the join.");
        }
    }

    private InternalVariable<?> findVariableInAtom(InternalBodyAtom atom, Column column) {
        Table table = column.getTable();
        String columnName = column.getColumnName();

        if (table != null && !atom.getName().equals(table.getName())) {
            return null;
        }

        return atom.getVariables().stream()
                .filter(variable -> variable.getName().equals(columnName))
                .findFirst().orElse(null);
    }

    private InternalVariable<?> findVariableInSchema(Column columnFromSchema, AttributeType type)
            throws TransformationException {
        return resultSchema.stream()
                .filter(variable -> variableMatchesColumnAndType(variable, columnFromSchema, type))
                .findFirst()
                .orElseThrow(() -> new TransformationException("There is a join on expression defining equality " +
                        "between two columns, but one of them does not occur in the schemes of the joined relations."));
    }

    private boolean variableMatchesColumnAndType(InternalVariable<?> variable, Column column, AttributeType type) {
        Table table = column.getTable();
        String columnName = column.getColumnName();
        InternalBodyAtom atom = body.getAtomsByIndex().get(variable.getIndex());

        if (table != null && !atom.getName().equals(table.getName())) {
            return false;
        }

        return variable.getName().equals(columnName) && variable.getType() == type;
    }

    public List<List<InternalVariable<?>>> buildEquivalences() {
        ImmutableGraph<InternalVariable<?>> equivalenceGraph = equivalences.build();
        List<List<InternalVariable<?>>> allConnectedComponents = new ArrayList<>();
        Set<InternalVariable<?>> visited = new HashSet<>();

        for (InternalVariable<?> start : resultSchema) {
            if (visited.contains(start)) continue;
            allConnectedComponents.add(findConnectedComponentBFS(equivalenceGraph, visited, start));
        }

        return allConnectedComponents;
    }

    private List<InternalVariable<?>> findConnectedComponentBFS(ImmutableGraph<InternalVariable<?>> equivalenceGraph,
                                                                Set<InternalVariable<?>> visited,
                                                                InternalVariable<?> start) {
        List<InternalVariable<?>> connectedComponent = new ArrayList<>();
        Queue<InternalVariable<?>> nextNodes = new ArrayDeque<>();
        nextNodes.add(start);
        while (!nextNodes.isEmpty()) {
            InternalVariable<?> currentNode = nextNodes.poll();

            if (visited.contains(currentNode)) continue;

            for (InternalVariable<?> adjacentNode : equivalenceGraph.adjacentNodes(currentNode)) {
                if (!visited.contains(adjacentNode)) {
                    nextNodes.add(adjacentNode);
                }
            }

            visited.add(currentNode);
            connectedComponent.add(currentNode);
        }

        return connectedComponent;
    }

    public List<InternalVariable<?>> getEqualizedResultSchema() {
        List<List<InternalVariable<?>>> equivalences = buildEquivalences();

        for (List<InternalVariable<?>> equalVariables : equivalences) {
            InternalVariable<?> representative = equalVariables.get(0);

            for (int i = 0; i < resultSchema.size(); i++) {
                InternalVariable<?> schemaVariable = resultSchema.get(i);
                if (schemaVariable == representative) {
                    continue;
                }

                for (int j = 1; j < equalVariables.size(); j++) {
                    InternalVariable<?> variableEqualToRepresentative = equalVariables.get(j);
                    if (schemaVariable == variableEqualToRepresentative) {
                        resultSchema.set(i, representative);
                    }
                }
            }
        }

        return resultSchema;
    }
}
