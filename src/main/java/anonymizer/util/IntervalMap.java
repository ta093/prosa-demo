package anonymizer.util;

import java.util.HashMap;
import java.util.stream.IntStream;

public class IntervalMap {
    
    public static HashMap<String, String> fromRange(int start, int end) {
        HashMap<String, String> map = new HashMap<>();
        
        int[] range = IntStream.range(start, end + 1).toArray();
        String interval = String.format("[%d,%d]", start, end);

        for (int i : range) {
            map.put(Integer.toString(i), interval);
        }

        return map;
    }

}
