package anonymizer.util;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class PowerSet {

    /**
     * Takes a set of integers and returns its power set.
     * 
     * DISCLAIMER: THIS METHOD HAS BEEN COPIED FROM:
     * https://stackoverflow.com/a/14818944
     * (last visited: 2022-02-22 20:43+0100) 
     * 
     * @param set Input set of integers
     * @return A set of sets of integers
     */
    public static LinkedHashSet<LinkedHashSet<Integer>> fromSet(Set<Integer> set) {
        ArrayList<Integer> list = new ArrayList<>(set);
        int size = list.size();

        LinkedHashSet<LinkedHashSet<Integer>> powerSet = new LinkedHashSet<>();

        for (long i = 0; i < (1 << size); i++) {
            LinkedHashSet<Integer> element = new LinkedHashSet<>();

            for (int j = 0; j < size; j++) {
                if ((i >> j) % 2 == 1) {
                    element.add(list.get(j));
                }
            }

            // optimization: ignore empty set
            if (element.size() != 0) {
                powerSet.add(element);
            }
        }

        return powerSet;
    }

}
