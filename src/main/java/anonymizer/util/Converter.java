package anonymizer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import anonymizer.structures.Attribute;
import anonymizer.structures.Row;
import anonymizer.structures.Table;
import atom.RelationalAtom;
import instance.Instance;
import instance.OriginTag;
import term.Constant;
import term.Term;

public class Converter {

    /**
     * Converts an instance into a list of tables.
     * 
     * @param instance The ChaTEAU instance
     * @return A list of ProSAnon tables
     */
    public static ArrayList<Table> fromInstanceToTables(Instance instance) {
        ArrayList<Table> tables = new ArrayList<>();

        var schema = instance.getSchema();

        for (final String tableName : schema.keySet()) {
            // skip "Result" relation
            if (tableName.toLowerCase().equals("result")) {
                continue;
            }

            // init a new table
            Table table = new Table(tableName);

            // add attributes
            var attributes = schema.get(tableName);
            for (String name : attributes.keySet()) {
                System.out.println(name);
                // skip provenance id attributes (usually the last attribute in a table)
                if (name.equals(tableName.toLowerCase() + "_id")) {
                    continue;
                }

                Attribute attribute = new Attribute(name, attributes.get(name));
                table.addAttribute(attribute);
            }

            // add rows
            for (RelationalAtom atom : instance.getRelationalAtomsBySchema(tableName)) {
                Row row = new Row(table);

                // extract all terms except provenance id
                List<Term> terms = atom.getTerms().subList(0, atom.getTerms().size());
                terms.forEach(term -> {
                    if (!term.getName().equals(tableName.toLowerCase() + "_id")) {
                        row.addValue(term.toString().replaceAll("\"", ""));
                    }
                });

                table.addRow(row);
            }

            // add table to table list
            tables.add(table);
        }

        return tables;
    }

    /**
     * Converts a list of tables to an instance.
     * 
     * @param tables A list of ProSAnon tables
     * @return A ChaTEAU instance containing the tables
     */
    public static Instance fromTablesToInstance(ArrayList<Table> tables) {
        HashMap<String, LinkedHashMap<String, String>> schema = new HashMap<>();

        // convert schema
        for (Table table : tables) {
            String name = table.getName();
            ArrayList<Attribute> attributes = table.getAttributes();

            LinkedHashMap<String, String> attributeMap = new LinkedHashMap<>();

            for (Attribute attribute : attributes) {
                String attributeName = attribute.getName();
                String attributeType = attribute.getType();

                attributeMap.putIfAbsent(attributeName, attributeType);
            }

            schema.put(name, attributeMap);
        }

        // create instance with schema
        Instance instance = new Instance(schema, OriginTag.INSTANCE);

        // convert rows to relational atoms and add them to instance
        for (Table table : tables) {
            for (Row row : table.getRows()) {
                RelationalAtom atom = fromRowToAtom(table.getName(), row);
                instance.addRelationalAtom(atom);
            }
        }

        return instance;
    }

    /**
     * Converts a {@link Row} to a {@link RelationalAtom}.
     * 
     * @param tableName The name of the table containing the row/atom
     * @param row       The row to convert
     * @return A relational atom representing the row
     */
    public static RelationalAtom fromRowToAtom(String tableName, Row row) {
        RelationalAtom atom = new RelationalAtom(tableName);
        ArrayList<Attribute> attributes = row.getAttributes();
        ArrayList<Term> terms = new ArrayList<>();

        for (int i = 0; i < row.getValues().size(); i++) {
            Attribute attribute = attributes.get(i);
            Object value = row.getValue(i);

            Term term = null;

            switch (attribute.getType()) {
                // case "int":
                // int resInt = Integer.parseInt(value.toString());
                // term = Constant.fromInteger(attribute.getName(), resInt);
                // break;

                // case "double":
                // double resDouble = Double.parseDouble(value.toString());
                // term = Constant.fromDouble(attribute.getName(), resDouble);
                // break;

                case "double":
                case "int":
                case "string":
                    String resStr = value.toString();
                    term = Constant.fromString(attribute.getName(), resStr);
                    break;

                default:
                    break;
            }

            if (term != null) {
                terms.add(term);
            }
        }

        atom.setTerms(terms);

        return atom;
    }

}
