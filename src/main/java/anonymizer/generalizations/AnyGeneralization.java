package anonymizer.generalizations;

/**
 * Generalization that substitutes each value with an "any" value. If "any" is
 * not specified, it defaults to "*".
 */
public class AnyGeneralization extends Generalization {

    private String any;

    public AnyGeneralization(String any) {
        super();

        this.any = any;
    }

    public AnyGeneralization() {
        this("*");
    }

    @Override
    public String generalize(String value) {
        return any;
    }

}
