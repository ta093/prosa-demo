package anonymizer.generalizations;

import java.util.Map;

/**
 * Generalization that maps values to more specific values. It can be used to
 * implement concept hierarchies that cannot be expressed mathematically.
 */
public class MapGeneralization extends Generalization {

    private Map<String, String> map;

    public MapGeneralization(Map<String, String> map) {
        super();

        this.map = map;
    }

    @Override
    public String generalize(String value) {
        return map.containsKey(value) ? map.get(value) : value;
    }

}
