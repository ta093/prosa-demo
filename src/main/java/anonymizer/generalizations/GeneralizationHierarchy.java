package anonymizer.generalizations;

import java.util.ArrayList;

/**
 * Class that represents a generalization hierarchy for a certain attribute.
 */
public class GeneralizationHierarchy {

    private ArrayList<Generalization> generalizations;
    private int step;

    /**
     * Constructor for a new, empty hierarchy.
     */
    public GeneralizationHierarchy() {
        this.generalizations = new ArrayList<>();
        this.step = 0;
    }

    /**
     * Constructor for an already existing list of {@link Generalization}s.
     * 
     * @param generalizations A list of generalizations
     */
    public GeneralizationHierarchy(ArrayList<Generalization> generalizations) {
        this();
        this.generalizations = generalizations;
    }

    /**
     * Adds a generalization to the hierarchy.
     * 
     * @param generalization the generalization to add
     */
    public void addGeneralization(Generalization generalization) {
        generalizations.add(generalization);
    }

    /**
     * Gets the current generalization.
     * 
     * @return the current generalization
     */
    public Generalization current() {
        return generalizations.get(step);
    }

    /**
     * Gets the next generalization and increases the internal step counter.
     * 
     * @return the next generalization
     * @throws EndOfHierarchyException Signals that the end of the hierarchy has
     *                                 been reached
     */
    public Generalization next() throws EndOfHierarchyException {
        if (step < generalizations.size() - 1) {
            return generalizations.get(++step);
        } else {
            step++;
            throw new EndOfHierarchyException();
        }
    }

    /**
     * Gets the amount of remaining generalization steps.
     * 
     * @return Remaining stepts (integer)
     */
    public int getRemainingSteps() {
        return this.generalizations.size() - step;
    }

    /**
     * Resets the generalization step counter.
     */
    public void reset() {
        this.step = 0;
    }

}
