package anonymizer.generalizations;

/**
 * This class represents an abstract generalization. Classes inheriting from
 * this class need to implement the {@link Generalization#generalize(String)}
 * method.
 */
public abstract class Generalization {

    protected Generalization() {

    }

    public abstract String generalize(String value);

}
