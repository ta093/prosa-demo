package anonymizer.generalizations;

/**
 * Generalization that substitutes the last n characters of a string with
 * asterisks.
 */
public class MaskingGeneralization extends Generalization {

    private int numberOfChars;

    public MaskingGeneralization(int numberOfChars) {
        super();

        this.numberOfChars = numberOfChars;
    }

    @Override
    public String generalize(String value) {
        String maskedValue = new String(value);
        maskedValue = maskedValue.substring(0, value.length() - numberOfChars);
        maskedValue += "*".repeat(numberOfChars);

        return maskedValue;
    }
}
