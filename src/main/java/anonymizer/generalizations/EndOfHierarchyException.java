package anonymizer.generalizations;

/**
 * Class that represents an exception that is thrown whenever the end of a
 * generalization hierarchy was reached.
 */
public class EndOfHierarchyException extends Exception {

    public EndOfHierarchyException() {
        super();
    }

    public EndOfHierarchyException(String message) {
        super(message);
    }

}
