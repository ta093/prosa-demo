package anonymizer.generalizations;

/**
 * Generalization that only returns a substring of a given string.
 */
public class SubstringGeneralization extends Generalization {

    private int start;
    private int end;

    public SubstringGeneralization(int start, int end) {
        super();

        this.start = start;
        this.end = end;
    }

    @Override
    public String generalize(String value) {
        return value.substring(start, end);
    }

}
