package anonymizer.generalizations;

/**
 * Generalization that floors or ceils double values to their corresponding
 * integer values.
 */
public class RoundGeneralization extends Generalization {

    public RoundGeneralization() {
        super();
    }

    @Override
    public String generalize(String value) {
        int roundedValue = (int) Math.round(Double.parseDouble(value));
        return Integer.toString(roundedValue);
    }

}
