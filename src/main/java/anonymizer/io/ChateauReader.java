package anonymizer.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import anonymizer.structures.Attribute;
import anonymizer.structures.Row;
import anonymizer.structures.Table;
import anonymizer.structures.TableType;

/**
 * This class provides a reader that reads ChaTEAU-formatted XML files and
 * parses the tables, including their contents.
 */
public class ChateauReader {

    private SAXBuilder saxBuilder;
    private ArrayList<Table> tables;

    /**
     * Initialize
     */
    public ChateauReader() {
        saxBuilder = new SAXBuilder();
        tables = new ArrayList<>();
    }

    /**
     * Reads a ChaTEAU file.
     * 
     * @param file The {@link File} in ChaTEAU XML format
     * @throws JDOMException
     * @throws IOException
     */
    public void readFile(File file) throws JDOMException, IOException, IllegalArgumentException {
        Document document = saxBuilder.build(file);

        Element inputElement = document.getRootElement();
        Element relationsElement = inputElement.getChild("schema").getChild("relations");

        for (Element relationElement : relationsElement.getChildren("relation")) {
            String tag = relationElement.getAttributeValue("tag");
            Table table = extractTable(relationElement);
            tables.add(table);

            if (tag != null) {
                if (tag.equals("S")) {
                    table.setTableType(TableType.SOURCE);
                } else {
                    table.setTableType(TableType.TARGET);
                }
            }
        }

        Element instanceElement = inputElement.getChild("instance");
        List<Element> atomElements = extractAtomElements(instanceElement);

        tables.forEach(table -> fillTable(table, atomElements));
    }

    /**
     * Returns the tables, if a file has been read yet, otherwise an empty
     * {@link ArrayList} of {@link Table tables}.
     * 
     * @return List of already built tables
     */
    public ArrayList<Table> getTables() {
        return tables;
    }

    /**
     * Extracts a table from a SAX {@link Element}.
     * 
     * @param relationElement The {@link Element} representing the table
     * @return A new table
     */
    private Table extractTable(Element relationElement) {
        String tableName = relationElement.getAttributeValue("name");
        List<Element> attributeElements = relationElement.getChildren("attribute");

        Table table = new Table(tableName);

        for (Element attributeElement : attributeElements) {
            String attributeName = attributeElement.getAttributeValue("name");
            String attributeType = attributeElement.getAttributeValue("type");

            Attribute attribute = new Attribute(attributeName, attributeType);

            table.addAttribute(attribute);
        }

        return table;
    }

    /**
     * Extracts atom {@link Element elements} from a SAX instance element.
     * 
     * @param instanceElement The {@link Element} representing the instance
     * @return A list of atom elements (SAX)
     */
    private List<Element> extractAtomElements(Element instanceElement) {
        List<Element> atomElements = instanceElement.getChildren("atom");
        return atomElements;
    }

    /**
     * Fills a table with its corresponding rows.
     * 
     * @param table        The table to fill
     * @param atomElements A list of atom {@link Element elements}
     */
    private void fillTable(Table table, List<Element> atomElements) {
        for (Element atomElement : atomElements) {
            String tableName = atomElement.getAttributeValue("name");

            if (tableName.equals(table.getName())) {
                List<Element> children = atomElement.getChildren();
                ArrayList<Object> values = extractValues(children);

                Row row = new Row(table, values.toArray());
                table.addRow(row);
            }
        }
    }

    /**
     * Extracts values from a list of SAX atom {@link Element elements}.
     * 
     * @param children A list of SAX elements representing constants, variables, and
     *                 nulls
     * @return An {@link ArrayList} of objects representing a row
     */
    private ArrayList<Object> extractValues(List<Element> children) {
        ArrayList<Object> values = new ArrayList<>();

        for (Element child : children) {
            String name, value;
            int index;

            switch (child.getName().toLowerCase()) {
                case "constant":
                    value = child.getAttributeValue("value");
                    values.add(value);
                    break;

                case "null":
                    name = child.getAttributeValue("name");
                    index = Integer.parseInt(child.getAttributeValue("index"));
                    value = "#N_" + name + "_" + index;
                    values.add(value);
                    break;

                case "variable":
                    name = child.getAttributeValue("name");
                    index = Integer.parseInt(child.getAttributeValue("index"));
                    value = "#V_" + name + "_" + index;
                    values.add(value);
                    break;

                default:
                    break;
            }
        }

        return values;
    }
}
