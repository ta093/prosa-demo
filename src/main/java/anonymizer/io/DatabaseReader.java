package anonymizer.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import anonymizer.structures.Row;
import anonymizer.structures.Table;

public class DatabaseReader {

    private Connection connection;
    private String databaseName;
    private String host;
    private String username;
    private String password;

    /**
     * Constructor for a database client with given database name.
     * 
     * @param databaseName The database name
     * @param host         The database host
     * @param username     The database user
     * @param password     The user's password
     */
    public DatabaseReader(String databaseName, String host, String username, String password) {
        this.databaseName = databaseName;
        this.host = host;
        this.username = username;
        this.password = password;
    }

    /**
     * Establishes a connection to a (PostgreSQL) database. If a previous connection
     * already exists, this method does nothing.
     * 
     * @throws SQLException
     */
    public void connect() throws SQLException {
        // singleton approach
        if (connection == null) {
            connection = DriverManager.getConnection(
                    String.format("jdbc:postgresql://%s/%s", this.host, this.databaseName), this.username,
                    this.password);
        }
    }

    /**
     * Closes the connection to a database, if one exists.
     * 
     * @throws SQLException
     */
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * Executes a SQL query and returns the result set.
     * 
     * @param query The query to execute
     * @return A {@link ResultSet} containing the query result aka target database
     * @throws SQLException
     */
    private ResultSet executeQuery(String query) throws SQLException {
        connect();

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        return resultSet;
    }

    /**
     * Executes a query and returns the result as a {@link Table}.
     * 
     * @param query      The query to execute
     * @param resultName The name the resulting table should get
     * @return new table containing the query result
     * @throws SQLException
     */
    public Table getResultTable(String query, String resultName) throws SQLException {
        connect();

        // create empty table
        ResultSet resultSet = executeQuery(query);
        Table table = new Table(resultName);

        // get total column count
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columns = metaData.getColumnCount();

        // create "schema"
        for (int i = 1; i <= columns; i++) {
            table.addAttribute(metaData.getColumnLabel(i), metaData.getColumnTypeName(i));
        }

        // add rows to table
        while (resultSet.next()) {
            Row row = new Row(table);

            for (int i = 1; i <= columns; i++) {
                row.addValue(resultSet.getObject(i));
            }

            table.addRow(row);
        }

        return table;
    }

    /**
     * Gets the names of all tables within a database schema.
     * 
     * @return An {@link ArrayList} of table names (strings)
     * @throws SQLException
     */
    public ArrayList<String> getTableNames() throws SQLException {
        connect();

        final String query = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'";

        ArrayList<String> tableNames = new ArrayList<>();
        ResultSet result = executeQuery(query);

        while (result.next()) {
            tableNames.add(result.getString(1));
        }

        return tableNames;
    }

    /**
     * Gets a single table from a database, including all data within that table.
     * 
     * @param tableName The table name
     * @return A {@link Table} object that represents the whole table
     * @throws SQLException
     */
    public Table getTable(String tableName) throws SQLException {
        connect();

        // receive columns
        Table table = new Table(tableName);
        ResultSet result = connection.getMetaData().getColumns(null, "public", tableName, null);

        while (result.next()) {
            table.addAttribute(result.getString(4), result.getString(6));
        }

        // receive rows
        result = executeQuery("SELECT * FROM " + tableName);
        while (result.next()) {
            Row row = new Row(table);

            for (int i = 1; i <= table.getAttributeNames().size(); i++) {
                row.addValue(result.getObject(i));
            }

            table.addRow(row);
        }

        return table;
    }

}