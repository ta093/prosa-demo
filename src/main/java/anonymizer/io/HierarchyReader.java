package anonymizer.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import anonymizer.generalizations.AnyGeneralization;
import anonymizer.generalizations.Generalization;
import anonymizer.generalizations.GeneralizationHierarchy;
import anonymizer.generalizations.MapGeneralization;
import anonymizer.generalizations.MaskingGeneralization;
import anonymizer.generalizations.RoundGeneralization;
import anonymizer.generalizations.SubstringGeneralization;

/**
 * This class provides a reader that reads hierarchy files and parses it to
 * actual generalization hierarchies per attribute.
 */
public class HierarchyReader {

    private Scanner reader;
    private HashMap<String, GeneralizationHierarchy> hierarchyMap;

    /**
     * Constructs a new HierarchyReader.
     * 
     * @param file The {@link File} that contains the hierarchies
     * @throws FileNotFoundException If file does not exist
     */
    public HierarchyReader(File file) throws FileNotFoundException {
        reader = new Scanner(file);
        hierarchyMap = new HashMap<>();
    }

    /**
     * Closes the reader. Should be called every time the reader is not needed any
     * longer.
     */
    public void close() {
        reader.close();
    }

    /**
     * Reads the determined file.
     * 
     * @return All hierarchies contained in the file
     */
    public HashMap<String, GeneralizationHierarchy> getHierarchies() {
        while (reader.hasNextLine()) {
            parseLine(reader.nextLine());
        }

        return hierarchyMap;
    }

    /**
     * Parses a line of the input file.
     * 
     * @param line A single line of the hierarchy input file
     */
    private void parseLine(String line) {
        int colonIndex = line.indexOf(":");

        // catch empty lines
        if (colonIndex == -1) {
            return;
        }

        String attributeName = line.substring(0, colonIndex).trim();
        String hierarchyString = line.substring(colonIndex + 1).trim();

        String[] generalizationStrings = hierarchyString.split(";");
        GeneralizationHierarchy hierarchy = new GeneralizationHierarchy();

        for (int i = 0; i < generalizationStrings.length; i++) {
            // catch empty definitions
            if (generalizationStrings[i].length() == 0) {
                continue;
            }

            Generalization generalization = parseGeneralization(generalizationStrings[i].trim());

            if (generalization != null) {
                hierarchy.addGeneralization(generalization);
            }
        }

        hierarchyMap.put(attributeName, hierarchy);
    }

    /**
     * Parses a generalization string.
     * 
     * @param generalizationString
     * @return
     */
    private Generalization parseGeneralization(String generalizationString) {
        if (generalizationString.startsWith("any")) {
            return parseAny();
        } else if (generalizationString.startsWith("masking")) {
            return parseMasking(generalizationString);
        } else if (generalizationString.startsWith("map")) {
            return parseMap(generalizationString);
        } else if (generalizationString.startsWith("interval")) {
            return parseInterval(generalizationString);
        } else if (generalizationString.startsWith("round")) {
            return parseRound();
        } else if (generalizationString.startsWith("substring")) {
            return parseSubstring(generalizationString);
        }

        return null;
    }

    /**
     * Gets the "inner string" of a generalization definition.
     * 
     * @param input Line of hierarchy file
     * @return The inner string, which is the part surrounded by curly brackets {
     *         and }.
     */
    private String getInnerString(String input) {
        int start = input.indexOf("{");
        int end = input.indexOf("}");

        if (start == 0 || end == 0)
            return null;

        String inner = input.substring(++start, end);
        return inner;
    }

    /**
     * Gets a new any-generalization.
     * 
     * @return A new {@link AnyGeneralization}
     */
    private AnyGeneralization parseAny() {
        return new AnyGeneralization();
    }

    /**
     * Parses a masking generalization.
     * 
     * @param input String representation of masking
     * @return A new {@link MaskingGeneralization}
     */
    private MaskingGeneralization parseMasking(String input) {
        String inner = getInnerString(input);
        return new MaskingGeneralization(Integer.parseInt(inner));
    }

    /**
     * Parses a map generalization.
     * 
     * @param input String representation of mapping
     * @return A new {@link MapGeneralization}
     */
    private MapGeneralization parseMap(String input) {
        HashMap<String, String> map = new HashMap<>();

        String inner = getInnerString(input);
        String[] mappingStrings = inner.split(" and ");

        for (int i = 0; i < mappingStrings.length; i++) {
            String mappingString = mappingStrings[i].trim();

            String left = mappingString.split(" to ")[0].trim();
            String right = mappingString.split(" to ")[1].trim();

            map.put(left, right);
        }

        return new MapGeneralization(map);
    }

    /**
     * Parses an interval generalization, which is a special kind of map
     * generalization.
     * 
     * @param input String representation of mapping
     * @return A new {@link MapGeneralization}
     */
    private MapGeneralization parseInterval(String input) {
        HashMap<String, String> map = new HashMap<>();

        String inner = getInnerString(input);
        String[] intervalStrings = inner.split(" and ");

        for (int i = 0; i < intervalStrings.length; i++) {
            String intervalString = intervalStrings[i].trim();

            String leftString = intervalString.split("-")[0].trim();
            String rightString = intervalString.split("-")[1].trim();

            int start = Integer.parseInt(leftString);
            int end = Integer.parseInt(rightString);

            for (int j = start; j <= end; j++) {
                map.put(Integer.toString(j), String.format("%d-%d", start, end));
            }
        }

        return new MapGeneralization(map);
    }

    /**
     * Gets a new round-generalization.
     * 
     * @return A new {@link RoundGeneralization}
     */
    private RoundGeneralization parseRound() {
        return new RoundGeneralization();
    }

    /**
     * Parses a new substring generalization.
     * 
     * @param input String representation of mapping
     * @return A new {@link SubstringGeneralization}
     */
    private SubstringGeneralization parseSubstring(String input) {
        String inner = getInnerString(input);

        String leftString = inner.split(" to ")[0].trim();
        String rightString = inner.split(" to ")[1].trim();

        int start = Integer.parseInt(leftString);
        int end = Integer.parseInt(rightString);

        return new SubstringGeneralization(start, end);
    }

}
