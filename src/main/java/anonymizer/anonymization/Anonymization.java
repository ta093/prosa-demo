package anonymizer.anonymization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import anonymizer.generalizations.EndOfHierarchyException;
import anonymizer.structures.Attribute;
import anonymizer.structures.QuasiIdentifier;
import anonymizer.structures.Table;

/**
 * This class provides the main anonymization process.
 */
public class Anonymization {

    /**
     * Anonymizes a table.
     * 
     * @param table         the table to anonymize
     * @param k             desired k-anonymity
     * @param distinctRatio minimum distinct ratio for quasi-identifiers
     * @param ids           indices of (directly) identifying attributes
     * @return
     */
    public static Table anonymizeTable(Table table, int k, double distinctRatio, int[] ids, int sensitiveIndex) {
        // compare attributes by number of remaining generalization steps
        Comparator<Attribute> compareByRemainingSteps = (Attribute a1, Attribute a2) -> Integer
                .compare(a2.getRemainingGeneralizationSteps(), a1.getRemainingGeneralizationSteps());

        System.out.println(String.format("Table %s:", table.getName()));
        System.out.println(table);

        int columnCount = table.getAttributes().size();

        // identifying part
        Table identifyingPart = table.subtable(ids);

        // get "remaining" indices
        int[] quasiIdentifyingIndices = new int[columnCount - ids.length];
        int index = ids.length;
        for (int i = 0; i < quasiIdentifyingIndices.length; i++) {
            quasiIdentifyingIndices[i] = index++;
        }

        // quasi-identifying and sensitive part
        Table qiTable = table.subtable(quasiIdentifyingIndices);

        // "main algorithm"
        for (QuasiIdentifier qi : qiTable.findQuasiIdentifiers(sensitiveIndex, distinctRatio)) {
            System.out.println(String.format("*** Found QI: %s ***", qi.getAttributes().toString()));

            // generalize until desired k has been reached
            WhileLoop: while (qiTable.measureKAnonymity(qi) < k) {
                // order attributes by remaining steps
                ArrayList<Attribute> orderedAttributes = qi.getAttributes();
                Collections.sort(orderedAttributes, compareByRemainingSteps);

                try {
                    Attribute selectedAttribute = orderedAttributes.get(0);

                    if (selectedAttribute.getRemainingGeneralizationSteps() == 0) {
                        // quasi-identifying attributes can't be further generalized
                        break WhileLoop;
                    }

                    // column generalization
                    qiTable.generalizeColumn(selectedAttribute);
                } catch (EndOfHierarchyException | NullPointerException ex) {
                    // no generalization available or highest generalization reached
                }
            }

            // output result
            int achievedK = qiTable.measureKAnonymity(qi);
            System.out.println(String.format("*** Achieved %d-anonymity for this QI ***", achievedK));
            System.out.println();
        }

        // put table back together and shuffle it
        Table finalTable = Table.combine(identifyingPart, qiTable);
        finalTable.shuffleRows();

        // return final table
        return finalTable;
    }

}
