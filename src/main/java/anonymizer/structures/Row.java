package anonymizer.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.IntStream;

/**
 * Class that represents a single row (aka tuple).
 */
public class Row {

    private ArrayList<Attribute> rowAttributes;
    private ArrayList<Object> rowValues;

    /**
     * Constructor for an empty row.
     * 
     * @param table The table that contains the new row
     */
    public Row(Table table) {
        this.rowAttributes = table.getAttributes();
        this.rowValues = new ArrayList<>();
    }

    /**
     * Constructor for a row with values.
     * 
     * @param table  The table that contains the new row
     * @param values The values, with any kind of length
     */
    public Row(Table table, Object... values) {
        this(table);

        for (int i = 0; i < values.length; i++) {
            rowValues.add(values[i]);
        }
    }

    /**
     * Sets the attributes.
     * 
     * @param attributes A String --> String {@link HashMap} that maps attribute
     *                   names to data types
     */
    public void setAttributes(ArrayList<Attribute> attributes) {
        this.rowAttributes = attributes;
    }

    /**
     * Gets the attributes and their types.
     * 
     * @return Attributes and types, as String --> String {@link HashMap}
     */
    public ArrayList<Attribute> getAttributes() {
        return rowAttributes;
    }

    /**
     * Appends a value to the row.
     * 
     * @param value The value to add
     */
    public void addValue(Object value) {
        rowValues.add(value);
    }

    /**
     * Updates a value at a given index.
     * 
     * @param index The zero-based index of the value to update
     * @param value The new value
     */
    public void updateValue(int index, Object value) {
        rowValues.set(index, value);
    }

    /**
     * Gets the value at a specific position/index.
     * 
     * @param index The zero-based index of the value to get
     * @return The value at position <index>
     */
    public Object getValue(int index) {
        return rowValues.get(index);
    }

    /**
     * Gets all values.
     * 
     * @return The row values as {@link ArrayList} of objects
     */
    public ArrayList<Object> getValues() {
        return rowValues;
    }

    /**
     * Gets all distinct values (row in set semantics). Especially useful for
     * transposed tables.
     * 
     * @return The distinct row values as {@link ArrayList} of objects
     */
    public ArrayList<Object> getDistinctValues() {
        // init new set and list
        HashSet<Object> valueSet = new HashSet<>();
        ArrayList<Object> valueList = new ArrayList<>();

        // convert list to set
        for (Object value : rowValues) {
            valueSet.add(value);
        }

        // re-convert set to list
        for (Object value : valueSet) {
            valueList.add(value);
        }

        return valueList;
    }

    /**
     * Gets the number of values of the row.
     * 
     * @return Number of values in row
     */
    public int size() {
        return rowValues.size();
    }

    /**
     * Gets the number of distinct values (set semantics) of the row.
     * 
     * @return Number of distinct values in row
     */
    public int sizeDistinct() {
        return getDistinctValues().size();
    }

    /**
     * Gets the ratio of distinct values and all values of the row. Useful for QI
     * calculations.
     * 
     * @return Ratio, as double-precision value
     */
    public double distinctRatio() {
        return (double) sizeDistinct() / (double) size();
    }

    /**
     * Returns the so-called "fingerprint" of a row.
     * This is a special case of a partial fingerprint, starting at zero, ending at
     * the maximum value.
     * 
     * @return The fingerprint
     */
    public int fingerprint() {
        return partialFingerprint(0, rowValues.size() - 1);
    }

    /**
     * Returns the so-called "partial fingerprint" of a row segment.
     * This is a special case of a selective fingerprint, starting at the start
     * index, ending at the end index, including all values in-between.
     * 
     * @param start The zero-based start index
     * @param end   The zero-based end index
     * @return The "partial fingerprint"
     */
    public int partialFingerprint(int start, int end) {
        IntStream indicesStream = IntStream.range(start, end + 1);
        int[] indices = indicesStream.toArray();
        indicesStream.close();

        return selectiveFingerprint(indices);
    }

    /**
     * Returns the so-called "selective fingerprint" of a row segment.
     * 
     * @param indices An array of attribute indices
     * @return The "selective fingerprint"
     */
    public int selectiveFingerprint(int... indices) {
        StringBuilder compressedString = new StringBuilder();

        for (int i : indices) {
            if (!rowValues.get(i).toString().startsWith("#N_")) {
                compressedString.append(rowValues.get(i)).append(";");
            } else {
                // treat null values as 'any'
                compressedString.append("*;");
            }
        }

        // remove last semicolon
        compressedString.deleteCharAt(compressedString.length() - 1);

        // return hash code of compressed string ("fingerprint")
        return compressedString.toString().hashCode();
    }

    @Override
    public String toString() {
        ArrayList<String> valueStrings = new ArrayList<>();

        for (Object value : rowValues) {
            valueStrings.add(value.toString());
        }

        return String.join(" | ", valueStrings);
    }

}
