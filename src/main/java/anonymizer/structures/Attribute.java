package anonymizer.structures;

import anonymizer.generalizations.GeneralizationHierarchy;

/**
 * Class that represents an attribute (without an actual value).
 */
public class Attribute {

    private String name;
    private String type;
    private GeneralizationHierarchy hierarchy;

    /**
     * Constructor for a new attribute without hierarchy.
     * 
     * @param name The attribute's name
     * @param type The attribute's data type
     */
    public Attribute(String name, String type) {
        this.name = name;
        this.type = type;
        this.hierarchy = null;
    }

    /**
     * Constructor for a new attribute with a hierarchy.
     * 
     * @param name      The attribute's name
     * @param type      The attribute's data type
     * @param hierarchy The attribute's {@link GeneralizationHierarchy}
     */
    public Attribute(String name, String type, GeneralizationHierarchy hierarchy) {
        this(name, type);

        this.hierarchy = hierarchy;
    }

    /**
     * Gets the name of the attribute.
     * 
     * @return Attribute name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the type of the attribute.
     * 
     * @return Attribute type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the hierarchy of the attribute.
     * 
     * @return Attribute hierarchy
     */
    public GeneralizationHierarchy getHierarchy() {
        return hierarchy;
    }

    /**
     * Sets the hierarchy.
     * 
     * @param hierarchy A generalization hierarchy
     */
    public void setHierarchy(GeneralizationHierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

    /**
     * Returns whether a hierarchy is present.
     * 
     * @return true if a hierarchy exists, otherwise false
     */
    public boolean hasHierarchy() {
        return this.hierarchy != null;
    }

    /**
     * Gets the amount of remaining generalization steps for the attribute's
     * hierarchy.
     * 
     * @return Amount of remaining generalization steps
     */
    public int getRemainingGeneralizationSteps() {
        if (this.hierarchy == null) {
            return 0;
        }

        return this.hierarchy.getRemainingSteps();
    }

    /**
     * Resets the generalization hierarchy.
     */
    public void resetHierarchy() {
        this.hierarchy.reset();
    }

    @Override
    public String toString() {
        return name;
    }

}