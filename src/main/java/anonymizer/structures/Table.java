package anonymizer.structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.stream.IntStream;

import anonymizer.generalizations.EndOfHierarchyException;
import anonymizer.generalizations.GeneralizationHierarchy;
import anonymizer.util.PowerSet;

/**
 * Class that represents a table/relation.
 */
public class Table {

    private String tableName;
    private ArrayList<Attribute> attributes;
    private ArrayList<Row> rows;
    private boolean isTransposed;
    private TableType tableType;

    /**
     * Constructor for empty tables (tables that have a name but no attributes yet).
     * 
     * @param name The table name
     */
    public Table(String name) {
        this.tableName = name;
        this.attributes = new ArrayList<>();
        this.rows = new ArrayList<>();
        this.isTransposed = false;
        this.tableType = TableType.TARGET;
    }

    /**
     * Constructor for tables with a name and list of attributes.
     * 
     * @param name       The table name
     * @param attributes The table's attributes, as {@link ArrayList}
     */
    public Table(String name, ArrayList<Attribute> attributes) {
        this(name);
        this.attributes = attributes;
    }

    /**
     * Gets the name of the table.
     * 
     * @return The table name
     */
    public String getName() {
        return tableName;
    }

    /**
     * Gets the names of the attributes.
     * 
     * @return Attribute names as {@link ArrayList}
     */
    public ArrayList<String> getAttributeNames() {
        ArrayList<String> attributeNames = new ArrayList<>();
        attributes.forEach(attribute -> attributeNames.add(attribute.getName()));

        return attributeNames;
    }

    /**
     * Gets a single {@link Attribute} at a given index.
     * 
     * @param index The zero-based index of the attribute
     * @return The {@link Attribute}
     */
    public Attribute getAttribute(int index) {
        return attributes.get(index);
    }

    /**
     * Gets a single {@link Attribute} by its name.
     * 
     * @param name The name of the attribute
     * @return The {@link Attribute} or null
     */
    public Attribute getAttribute(String name) {
        for (Attribute attribute : attributes) {
            if (attribute.getName().equals(name)) {
                return attribute;
            }
        }

        return null;
    }

    /**
     * Gets the attributes and their values.
     * 
     * @return Attributes and data types as String --> String {@link HashMap}
     */
    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * Adds an attribute to the table.
     * 
     * @param name The name of the attribute
     * @param type The type of the attribute
     */
    public void addAttribute(String name, String type) {
        attributes.add(new Attribute(name, type));
    }

    /**
     * Adds an attribute to the table.
     * 
     * @param attribute The {@link Attribute} to add
     */
    public void addAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

    /**
     * Adds a row to the table.
     * 
     * @param row The new {@link Row}
     */
    public void addRow(Row row) {
        rows.add(row);
    }

    /**
     * Gets a single row of the table.
     * 
     * @param index The zero-based index of the row to get
     * @return The row at position <index>
     */
    public Row getRow(int index) {
        return rows.get(index);
    }

    /**
     * Gets the rows of the table.
     * 
     * @return An {@link ArrayList} of {@link Row}s
     */
    public ArrayList<Row> getRows() {
        return rows;
    }

    /**
     * Gets the first row of the table.
     * 
     * @return The first {@link Row}
     */
    public Row getFirstRow() {
        return getRow(0);
    }

    /**
     * Gets the last row of the table.
     * 
     * @return The last {@link Row}
     */
    public Row getLastRow() {
        return getRow(rows.size() - 1);
    }

    /**
     * Removes a row at a given index.
     * 
     * @param index The zero-based index of the row to remove
     */
    public void deleteRow(int index) {
        rows.remove(index);
    }

    /**
     * Gets the table type (source or target).
     * 
     * @return The {@link TableType} for this table
     */
    public TableType getTableType() {
        return tableType;
    }

    /**
     * Sets the table type (source or target).
     * 
     * @param tableType The new {@link TableType}
     */
    public void setTableType(TableType tableType) {
        this.tableType = tableType;
    }

    /**
     * Gets whether the table is transposed or not.
     * 
     * @return true, if table is in a transposed state, else false
     */
    public boolean isTransposed() {
        return isTransposed;
    }

    /**
     * Switches the "transposed" status. Use with caution, it might break the
     * semantics of transpositions if applied wrong.
     */
    private void toggleTransposedState() {
        isTransposed = !isTransposed;
    }

    /**
     * Transposes the table, which means that rows become columns and vice versa. In
     * other words, the table is handled as a matrix M and transformed into M^T.
     */
    public void transpose() {
        // switch status
        toggleTransposedState();

        ArrayList<Row> newRows = new ArrayList<>();

        // transpose table by treating it like a matrix
        for (int i = 0; i < getRow(0).size(); i++) {
            Row newRow = new Row(this);
            for (Row row : rows) {
                newRow.addValue(row.getValue(i));
            }
            newRows.add(newRow);
        }

        // replace rows with transposed rows
        rows = newRows;
    }

    /**
     * Returns a new table that contains only selected columns.
     * 
     * @param indices The columns the new table shall contain, as zero-based indices
     * @return A subtable of the current table, including only selected columns
     */
    public Table subtable(int... indices) {
        Table subTable = new Table(tableName);

        // transpose table, then select rows (actually columns) to keep
        transpose();

        for (int index : indices) {
            subTable.addAttribute(attributes.get(index));
            subTable.addRow(rows.get(index));
        }

        // transpose original table back
        transpose();

        // transpose new subtable; set transposed to false
        subTable.transpose();
        subTable.toggleTransposedState();

        return subTable;
    }

    /**
     * Measures k-anonymity for given columns (as zero-based indices).
     * 
     * @param indices The zero-based indices of rows in interest
     * @return The k of k-anonymity
     */
    public int measureKAnonymity(int... indices) {
        // store occurrences of unique rows
        LinkedHashMap<Integer, Integer> occurrences = new LinkedHashMap<>();

        // measure occurrences. Unique rows are rows that have every selected attribute
        // in common, which also makes their selective fingerprint identical.
        for (Row row : rows) {
            int fingerprint = row.selectiveFingerprint(indices);

            // init
            occurrences.putIfAbsent(fingerprint, 0);

            occurrences.put(fingerprint, occurrences.get(fingerprint) + 1);
        }

        // get minimum of unique occurrences
        int min = Collections.min(occurrences.values());
        return min;
    }

    /**
     * Measures k-anonymity for a given {@link QuasiIdentifier quasi-identifier}.
     * 
     * @param quasiIdentifier The quasi-identifier of interest
     * @return The k of k-anonymity
     */
    public int measureKAnonymity(QuasiIdentifier quasiIdentifier) {
        int[] indices = quasiIdentifier.getAttributeIndices().stream().mapToInt(Integer::intValue).toArray();
        return measureKAnonymity(indices);
    }

    /**
     * Finds all quasi-identifiers that satisfy a given threshold value for their
     * distinct ratio.
     * 
     * @param sensitiveIndex The zero-based index of the sensitive attribute. If set
     *                       to -1, no sensitive index will be used.
     * @param threshold      The minimum distinct ratio an attribute combination has
     *                       to reach in order to count as quasi-identifier
     * @return A descending list of quasi-identifiers, from largest to smallest
     */
    public ArrayList<QuasiIdentifier> findQuasiIdentifiers(int sensitiveIndex, double threshold) {
        ArrayList<QuasiIdentifier> quasiIdentifiers = new ArrayList<>();

        // build array of attribute indices to consider
        int maxRange = attributes.size() - 1;
        if (sensitiveIndex == -1) {
            maxRange++;
        }

        int[] range = IntStream.range(0, maxRange).toArray();
        LinkedHashSet<Integer> indices = new LinkedHashSet<>();

        for (int i : range) {
            if (i != sensitiveIndex) {
                indices.add(i);
            }
        }

        // try every combination ("powerset") of attributes, with regard to pruning for
        // supersets, if enabled
        for (LinkedHashSet<Integer> combination : PowerSet.fromSet(indices)) {
            LinkedHashMap<Integer, Integer> occurrences = new LinkedHashMap<>();

            for (Row row : rows) {
                int[] qiIndices = combination.stream().mapToInt(Integer::intValue).toArray();
                int fingerprint = row.selectiveFingerprint(qiIndices);

                // init
                occurrences.putIfAbsent(fingerprint, 0);

                occurrences.put(fingerprint, occurrences.get(fingerprint) + 1);
            }

            // get distinct ratio
            int distinctValues = occurrences.keySet().size();
            int allValues = rows.size();
            double distinctRatio = (double) distinctValues / (double) allValues;

            // catch all idenfifying combinations that satisfy the threshold
            if (distinctRatio >= threshold) {
                ArrayList<Attribute> qiAttributes = new ArrayList<>();

                for (int i : combination) {
                    qiAttributes.add(attributes.get(i));
                }

                ArrayList<Integer> qiIndices = new ArrayList<>(combination);
                QuasiIdentifier quasiIdentifier = new QuasiIdentifier(this, qiAttributes, qiIndices, distinctRatio);
                quasiIdentifiers.add(quasiIdentifier);
            }
        }

        // order descending by size, then return list of QIs
        Collections.sort(quasiIdentifiers);
        return quasiIdentifiers;
    }

    /**
     * Given a quasi-identifier and a minimum k value, all rows that do not satisfy
     * k-anonymity will be removed.
     * 
     * @param qi             The quasi-identifier defining the QI* classes
     * @param minOccurrences The k of k-anonymity
     */
    public void suppressRows(QuasiIdentifier qi, int minOccurrences) {
        // get qi indices as array
        LinkedHashMap<Integer, Integer> occurrences = new LinkedHashMap<>();
        int[] qiIndices = qi.getAttributeIndices().stream().mapToInt(Integer::intValue).toArray();

        // count occurrences
        for (Row row : rows) {
            int fingerprint = row.selectiveFingerprint(qiIndices);

            // init
            occurrences.putIfAbsent(fingerprint, 0);

            occurrences.put(fingerprint, occurrences.get(fingerprint) + 1);
        }

        // get list of row fingerprints
        ArrayList<Integer> fingerprints = new ArrayList<>(occurrences.keySet());

        // filter by rows that do not satisfy k-anonymity
        for (int fingerprint : fingerprints) {
            if (occurrences.get(fingerprint) >= minOccurrences) {
                occurrences.remove(fingerprint);
            }
        }

        // remove rows that do not satisfy k-anonymity
        for (int i = 0; i < rows.size(); i++) {
            if (occurrences.containsKey(rows.get(i).selectiveFingerprint(qiIndices))) {
                deleteRow(i--);
            }
        }
    }

    /**
     * Generalizes a specific column by one step.
     * 
     * @param index The zero-based index of the column
     * @throws EndOfHierarchyException Signals that the attribute cannot further be
     *                                 generalized
     */
    public void generalizeColumn(int index) throws EndOfHierarchyException {
        // column mode
        transpose();

        // get hierarchy of attribute of interest
        Row row = rows.get(index);
        GeneralizationHierarchy hierarchy = attributes.get(index).getHierarchy();

        if (hierarchy == null) {
            transpose();
            throw new NullPointerException(String.format("Attribute at index %d has no hierarchy.", index));
        }

        // generalize every attribute
        int i = 0;
        for (Object value : row.getValues()) {
            // ignore null values
            if (!value.toString().startsWith("#N_")) {
                row.updateValue(i++, hierarchy.current().generalize(value.toString()));
            }
        }

        // set hierarchy to next step; if end has been reached, a custom exception will
        // be thrown
        try {
            hierarchy.next();
        } catch (EndOfHierarchyException ex) {
            throw ex;
        } finally {
            // restore row mode
            transpose();
        }
    }

    /**
     * Generalizes a specific column by one step.
     * 
     * @param attribute The attribute to generalize
     * @throws EndOfHierarchyException Signals that the attribute cannot be further
     *                                 generalized
     */
    public void generalizeColumn(Attribute attribute) throws EndOfHierarchyException {
        int index = this.getAttributes().indexOf(attribute);
        generalizeColumn(index);
    }

    /**
     * Gets the size (number or rows) of the table.
     * 
     * @return Number of rows
     */
    public int size() {
        return this.rows.size();
    }

    /**
     * Permutates the rows.
     */
    public void shuffleRows() {
        Collections.shuffle(this.rows);
    }

    /**
     * Creates a new table out of two subtables.
     * 
     * @param t1 left subtable
     * @param t2 right subtable
     * @return A new, combined table
     */
    public static Table combine(Table t1, Table t2) {
        // set name of new table
        Table combinedTable = new Table(t1.getName());

        // combine attributes
        t1.getAttributes().forEach(attribute -> combinedTable.addAttribute(attribute));
        t2.getAttributes().forEach(attribute -> combinedTable.addAttribute(attribute));

        // if tables have different amounts of rows, return null since
        // tables cannot be combined
        if (t1.size() != t2.size()) {
            return null;
        }

        // combine rows
        for (int i = 0; i < t1.size(); i++) {
            ArrayList<Object> values = t1.getRow(i).getValues();
            values.addAll(t2.getRow(i).getValues());

            Row row = new Row(combinedTable);
            values.forEach(value -> row.addValue(value));

            combinedTable.addRow(row);
        }

        return combinedTable;
    }

    @Override
    public String toString() {
        StringBuilder tableString = new StringBuilder();

        // schema
        tableString.append(tableName).append("(");
        tableString.append(String.join(", ", getAttributeNames()));
        tableString.append("):").append(System.lineSeparator());

        // rows
        for (Row row : rows) {
            tableString.append(row.toString());
            tableString.append(System.lineSeparator());
        }

        return tableString.toString();
    }

}
