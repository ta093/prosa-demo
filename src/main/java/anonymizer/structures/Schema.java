package anonymizer.structures;

import java.util.ArrayList;

/**
 * Class that represents a schema, that is, a collection of {@link Table}s.
 * These tables might hold actual data (instances).
 * 
 * Unused for now, might be removed.
 */
@Deprecated
public class Schema {

    private ArrayList<Table> tables;

    /**
     * Constructor for a new, empty schema.
     */
    public Schema() {
        this.tables = new ArrayList<>();
    }

    /**
     * Gets all tables within the schema.
     * 
     * @return An {@link ArrayList} of {@link Table}s
     */
    public ArrayList<Table> getTables() {
        return tables;
    }

    /**
     * Adds a table to the schema.
     * 
     * @param table The {@link Table} to add
     */
    public void addTable(Table table) {
        tables.add(table);
    }

    @Override
    public String toString() {
        StringBuilder schemaString = new StringBuilder();

        for (Table table : tables) {
            schemaString.append(table.toString());
            schemaString.append(System.lineSeparator());
        }

        return schemaString.toString();
    }

}
