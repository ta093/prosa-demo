package anonymizer.structures;

/**
 * A enumeration of available table types.
 * A table can either be a source relation or a target relation.
 */
public enum TableType {
    SOURCE, TARGET
}
