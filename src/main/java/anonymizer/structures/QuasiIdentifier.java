package anonymizer.structures;

import java.util.ArrayList;

/**
 * Class that represents a quasi-identifier.
 */
public class QuasiIdentifier implements Comparable<QuasiIdentifier> {

    private final Table table;
    private final ArrayList<Attribute> attributes;
    private final ArrayList<Integer> attributeIndices;
    private double distinctRatio;

    /**
     * Constructor for a new quasi-identifier (QI).
     * 
     * @param table            The table the QI is part of
     * @param attributes       The attribute names of the QI
     * @param attributeIndices The attribute indices of the QI
     */
    public QuasiIdentifier(Table table, ArrayList<Attribute> attributes, ArrayList<Integer> attributeIndices) {
        this.table = table;
        this.attributes = attributes;
        this.attributeIndices = attributeIndices;
    }

    /**
     * Constructor for a new quasi-identifier (QI) including a distinct ratio.
     * 
     * @param table            The table the QI is part of
     * @param attributes       The attribute names of the QI
     * @param attributeIndices The attribute indices of the QI
     * @param distinctRatio    The distinct ratio of the QI
     */
    public QuasiIdentifier(Table table, ArrayList<Attribute> attributes, ArrayList<Integer> attributeIndices,
            double distinctRatio) {
        this(table, attributes, attributeIndices);
        this.distinctRatio = distinctRatio;
    }

    /**
     * Gets the distinct ratio. Returns 0 if the distinct ratio has not been
     * determined yet.
     * 
     * @return Distinct ratio
     */
    public double getDistinctRatio() {
        return (Double) distinctRatio != null ? distinctRatio : 0;
    }

    /**
     * Gets the attribute names.
     * 
     * @return List of attribute names
     */
    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * Gets the attribute indices.
     * 
     * @return List of attribute indices
     */
    public ArrayList<Integer> getAttributeIndices() {
        return attributeIndices;
    }

    /**
     * Gets the name of the table.
     * 
     * @return Table name
     */
    public String getTableName() {
        return table.getName();
    }

    /**
     * Gets a subtable that only consists of QI attributes.
     * 
     * @return A new subtable
     */
    public Table getQISubTable() {
        return table.subtable(attributeIndices.stream().mapToInt(Integer::intValue).toArray());
    }

    public int size() {
        return this.getAttributes().size();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        ArrayList<String> attributeNames = new ArrayList<>();
        attributes.forEach(attribute -> attributeNames.add(attribute.getName()));

        stringBuilder.append("{");
        stringBuilder.append(String.join(", ", attributeNames));
        stringBuilder.append("}").append(" - ");

        stringBuilder.append("distinct ratio: ");
        stringBuilder.append(Math.round(distinctRatio * 100) / 100D);

        return stringBuilder.toString();
    }

    @Override
    public int compareTo(QuasiIdentifier other) {
        return Integer.compare(other.getAttributes().size(), this.getAttributes().size());
    }
}
