package provenance;

import static java.util.logging.Level.INFO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Constraint;
import constraints.Egd;
import constraints.STTgd;
import instance.Instance;
import instance.SchemaTag;
import io.SingleInput;
import term.Constant;
import term.Term;
import term.Variable;
import term.VariableType;

public class Provenancer {

    /**
     * Rewrites the given ChaTEAU input to collect ProSA provenance information.
     * Does NOT create a copy of given {@link #SingleInput}, but modifies the actual
     * given object.
     * 
     * @param inputCopy
     * @return
     */
    public static SingleInput provenanceChaseInput(SingleInput inputCopy) {

        addTupleIDAttributeToSchema(inputCopy.getInstance().getSchema(), inputCopy.getInstance().getSchemaTags());
        addTupleIDsToInstanceTuples(inputCopy.getInstance());
        provenanceConstraints(inputCopy.getConstraints());

        return inputCopy;
    }

    public static void addTupleIDAttributeToSchema(Map<String, LinkedHashMap<String, String>> schema,
            HashMap<String, SchemaTag> schemaTags) {
        LinkedHashSet<String> sourceRelations = new LinkedHashSet<>();
        schemaTags.forEach((relation, tag) -> {
            if (tag == SchemaTag.SOURCE) {
                sourceRelations.add(new String(relation));
            }
        });

        schema.forEach((relationName, attributeToTypeMap) -> {
            if (!relationName.equals("Result")) {
                attributeToTypeMap.put(relationName + "_id", "string");
            } else {
                // Result relation also needs the source relation ids from sttgd body:
                attributeToTypeMap.put(relationName + "_id", "string");
                sourceRelations.forEach(sourceRelation -> {
                    attributeToTypeMap.put(sourceRelation + "_id", "string");

                });
            }
        });

    }

    public static void addTupleIDsToInstanceTuples(Instance instance) {
        instance.getRelationalAtoms().forEach(atom -> {
            atom.getTerms().add(Constant.fromString(atom.getRelationName() + "_id", atom.getProvInfo().getId()));
        });
    }

    /**
     * Rewrites the sttgd query so that for each result tuple, all source tuple ids
     * will be stored, that have contributed in creating that result tuple. Unlike
     * how-provenance, this provenance is also order-dependent, e.g. for the sttgd
     * "Rel('Mary'), Rel('Freddy') -> Result('Found Mary and Freddy')" which source
     * tuple id matched on the left atom (i.e. whose attribute value was 'Mary') and
     * which source tuple id matched on the right atom (i.e. its attribute value was
     * 'Freddy'). In order to do so, we add the tuple id of the source tuples as
     * normal attributes and save them for the result tuple: "Rel('Mary',
     * #V_Rel_id_1), Rel('Freddy', #V_Rel_id_2) -> Result('Found Mary and Freddy',
     * #V_Rel_id_1, #V_Rel_id_2)"
     * 
     * @param constraints
     * @return
     */
    public static void provenanceConstraints(LinkedHashSet<Constraint> constraints) {

        if (constraints.size() != 1) {
            throw new RuntimeException("More than one constraint.");
        }
        Constraint dep = constraints.iterator().next();
        if (!(dep instanceof STTgd)) {
            throw new RuntimeException("Constraint must be an s-t tgd.");
        }
        STTgd stTgd = (STTgd) dep;
        final Map<String, Integer> idCounter = new HashMap<>();

        if (stTgd.getHead().size() != 1) {
            throw new RuntimeException("STTGD head must have exactly one atom!");

        }
        RelationalAtom resultAtom = stTgd.getHead().iterator().next();
        stTgd.getBody().forEach(atom -> {

            int currentID = idCounter.getOrDefault(atom.getName(), 0);
            Variable id = new Variable(VariableType.FOR_ALL, atom.getName() + "_id", ++currentID);
            atom.getTerms().add(id);
            resultAtom.getTerms().add(id.copy());
            idCounter.put(atom.getName(), currentID);
        });
    }

    public static void addEgdsForDuplicateRemoval(SingleInput backChaseInput, Logger logger) {
        logger.log(INFO, "Creating EGDs for duplicate removal...");
        HashMap<String, SchemaTag> schemaTags = backChaseInput.getInstance().getSchemaTags();

        LinkedHashSet<String> targetRelationNames = new LinkedHashSet<>();
        schemaTags.forEach((relation, tag) -> {
            if (tag == SchemaTag.TARGET) {
                targetRelationNames.add(new String(relation));
            }
        });

        targetRelationNames.forEach(relationName -> {
            ArrayList<Term> variables1 = new ArrayList<>();
            ArrayList<Term> variables2 = new ArrayList<>();

            LinkedHashMap<String, String> attributes = backChaseInput.getInstance().getSchema().get(relationName);
            LinkedHashSet<RelationalAtom> egdBody = new LinkedHashSet<>();
            LinkedHashSet<EqualityAtom> egdHead = new LinkedHashSet<>();

            attributes.forEach((attribute, type) -> {
                Variable var1 = new Variable(VariableType.FOR_ALL, attribute, 1);
                Variable var2 = new Variable(VariableType.FOR_ALL, attribute, 2);
                if (!attribute.endsWith("_id")) {
                    variables1.add(var1);
                    variables2.add(var2);
                    egdHead.add(new EqualityAtom(var1, var2));
                } else {
                    // add the tuple id, which is identical
                    variables1.add(var1);
                    variables2.add(var1);
                }
            });
            RelationalAtom bodyAtom1 = new RelationalAtom(relationName, variables1);
            RelationalAtom bodyAtom2 = new RelationalAtom(relationName, variables2);
            egdBody.add(bodyAtom1);
            egdBody.add(bodyAtom2);

            Egd egd = new Egd(egdBody, egdHead);
            logger.log(Level.INFO, "Adding EGD: " + egd);
            backChaseInput.getConstraints().add(egd);
        });

    }

}
