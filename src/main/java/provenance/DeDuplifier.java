package provenance;

import atom.RelationalAtom;
import term.Constant;
import term.Term;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class DeDuplifier {

    public LinkedHashSet<RelationalAtom> deDuplicateAtoms(LinkedHashSet<RelationalAtom> relationalAtomsBySchema) {
        var container = new ArrayList<LinkedHashSet<RelationalAtom>>();
        for (var atom : relationalAtomsBySchema){
            var temp = new LinkedHashSet<RelationalAtom>();
            for (var otherAtom : relationalAtomsBySchema) {
                if (compareTerms(atom, otherAtom)) {
                    temp.add(otherAtom);
                }
            }
            if (!container.contains(temp)){
                container.add(temp);
            }
        }
        return compress(container);
    }

    public int findIndex(ArrayList<Term> terms){
        for (Term term : terms){
            if(containsUnderscore(term)){
                return terms.indexOf(term);
            }
        }
        return 0;
    }

    private boolean containsUnderscore(Term term) {
        var constant = (Constant) term;
        var value = constant.getValue();
        if (value instanceof String){
            var name = (String) value;
            return name.contains("_");
        }else {
            return false;
        }
    }

    private RelationalAtom rewriteAtom(LinkedHashSet<RelationalAtom> set) {
        RelationalAtom relationalAtom = set.stream().findFirst().get();
        set.forEach(atom -> {
            if (atom.hashCode()!=(relationalAtom.hashCode())){
                var currentProvInfo = relationalAtom.getProvInfo();
                var currentWhere = currentProvInfo.getWhereTupleProvenance();
                var currentWhy = currentProvInfo.getWhyProvenance();
                currentWhere.addAll(atom.getProvInfo().getWhereTupleProvenance());
                currentWhy.addAll(atom.getProvInfo().getWhyProvenance());
                currentProvInfo.setWhereTupleProvenance(currentWhere);
                currentProvInfo.setWhyProvenance(currentWhy);
                relationalAtom.setProvInfo(currentProvInfo);
            }
        });
        return relationalAtom;
    }

    private boolean compareTerms(RelationalAtom currentAtom, RelationalAtom relationalAtom) {
        var otherTerms = relationalAtom.getTerms();
        var terms = currentAtom.getTerms();
        var index = findIndex(terms);
        for (int i = 0; i<index; i++ ){
            if (!terms.get(i).equals(otherTerms.get(i))){
                return false;
            }
        }
        return true;
    }

    private LinkedHashSet<RelationalAtom> compress(List<LinkedHashSet<RelationalAtom>> container) {
        return container.stream().map(this::rewriteAtom).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
