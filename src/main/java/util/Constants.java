package util;

public final class Constants {
    public static final String SLS = System.lineSeparator();
    public static final String jsonDBConfigPath = "config.json";
    public static final String SQL_QUERY = "SELECT firstname, modulename \n" + "FROM students NATURAL JOIN participants \n"
            + "WHERE firstname = 'Stefan'";

    // XML file paths:
    private static final String PIPELINE_DIR = "xml/pipelineSteps/";
    public static final String ACTUAL_DIR = PIPELINE_DIR + "";
    private static final String EXPECTED_DIR = "xml/pipelineSteps_expected/";

    public static enum ACTUALS {
        PARSER_OUTPUT(ACTUAL_DIR + "parser_output.xml"),
        CHASE_RESULT_WITHOUT_PROV(ACTUAL_DIR + "chase_result_without_provenance.xml"),
        CHASE_RESULT_WITH_PROV(ACTUAL_DIR + "chase_result_with_provenance.xml"),
        BACKCHASE_INPUT_WITHOUT_PROV(ACTUAL_DIR + "backchase_input_without_provenance.xml"),
        BACKCHASE_RESULT_WITHOUT_PROV(ACTUAL_DIR + "backchase_result_without_provenance.xml"),
        CHASE_INPUT_WITH_PROV(ACTUAL_DIR + "chase_input_with_provenance.xml"),
        BACKCHASE_INPUT_WITH_PROV(ACTUAL_DIR + "backchase_input_with_provenance.xml"),
        BACKCHASE_RESULT_WITH_PROV(ACTUAL_DIR + "backchase_result_with_provenance.xml"),
        EVOLUTION_INPUT(ACTUAL_DIR + "evolution_input.xml"),
        EVOLUTION_RESULT(ACTUAL_DIR + "evolution_result.xml"),
        BACK_EVOLUTION_INPUT(ACTUAL_DIR + "back_evolution_input.xml"),
        BACK_EVOLUTION_RESULT(ACTUAL_DIR + "back_evolution_result.xml");

        private final String path;

        private ACTUALS(String path) {
            this.path = path;
        }

        @Override
        public String toString() {
            return path;
        }
    }

    public static enum EXPECTED {
        PARSER_OUTPUT(EXPECTED_DIR + "parser_output.xml"),
        CHASE_RESULT_WITHOUT_PROV(EXPECTED_DIR + "chase_result_without_provenance.xml"),
        CHASE_RESULT_WITH_PROV(EXPECTED_DIR + "chase_result_with_provenance.xml"),
        BACKCHASE_INPUT_WITHOUT_PROV(EXPECTED_DIR + "backchase_input_without_provenance.xml"),
        BACKCHASE_RESULT_WITHOUT_PROV(EXPECTED_DIR + "backchase_result_without_provenance.xml"),
        CHASE_INPUT_WITH_PROV(EXPECTED_DIR + "chase_input_with_provenance.xml"),
        BACKCHASE_RESULT_WITH_PROV(EXPECTED_DIR + "backchase_result_with_provenance.xml"),
        BACKCHASE_INPUT_WITH_PROV(EXPECTED_DIR + "backchase_input_with_provenance.xml"),
        EVOLUTION_INPUT(EXPECTED_DIR + "evolution_input.xml"),
        EVOLUTION_RESULT(EXPECTED_DIR + "evolution_result.xml"),
        BACK_EVOLUTION_INPUT(EXPECTED_DIR + "back_evolution_input.xml"),
        BACK_EVOLUTION_RESULT(EXPECTED_DIR + "back_evolution_result.xml");

        private final String path;

        private EXPECTED(String path) {
            this.path = path;
        }

        @Override
        public String toString() {
            return path;
        }
    }

    public static enum ProvenanceType {

        WHY("why"), WHERE("where"), HOW("how");

        private final String typeName;

        private ProvenanceType(String type) {
            this.typeName = type;
        }

        public String getTypeName() {
            return typeName;
        }
    }

    public static final String LINE = SLS + "----------------------------------------------------------------" + SLS;

}
