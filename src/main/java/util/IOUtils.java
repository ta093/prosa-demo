package util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import constraints.Constraint;
import instance.Instance;
import io.InputReader;
import io.OutputWriter;
import io.SingleInput;

public class IOUtils {

    public static Properties loadFromJsonConfig(String pathToJsonConfig) throws IOException, ParseException {
        FileReader configReader = new FileReader(pathToJsonConfig);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfig = (JSONObject) jsonParser.parse(configReader);

        String dbName = (String) jsonConfig.get("dbName");
        String user = (String) jsonConfig.get("user");
        String pwd = (String) jsonConfig.get("password");
        String host = (String) jsonConfig.get("host");
        String port_string = (String) jsonConfig.get("port");
        int port = Integer.parseInt(port_string);

        String dbUrl = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;

        Properties props = new Properties();
        props.put("dbName", dbName);
        props.put("user", user);
        props.put("password", pwd);
        props.put("host", host);
        props.put("dbUrl", dbUrl);

        return props;
    }

    public static void saveSingleInput(SingleInput input, String path, String errorMessage, Logger logger) {
        saveToXML(input.getInstance(), input.getConstraints(), path, false, errorMessage, logger);
    }

    public static void saveInstanceToXML(Instance instance, String path, String errorMessage, Logger logger) {
        saveToXML(instance, null, path, true, errorMessage, logger);
    }

    public static void saveInstanceToXMLWithoutProv(Instance instance, String path, String errorMessage,
            Logger logger) {
        saveToXML(instance, null, path, false, errorMessage, logger);
    }

    protected static void saveToXML(Instance instance, LinkedHashSet<Constraint> constraints, String path,
            boolean includeProvenance, String errorMessage, Logger logger) {
        saveToXML(instance, constraints, new File(path), includeProvenance, errorMessage, logger);

    }

    private static void saveToXML(Instance instance, LinkedHashSet<Constraint> constraints, File file,
            boolean includeProvenance, String errorMessage, Logger logger) {
        logger.log(Level.INFO, "Saving XML to " + file.getPath());
        OutputWriter ow = new OutputWriter();

        ow.outputProvenance = includeProvenance;
        ow.preserveSourceSchema = true;
        ow.setOrigin("prosa");

        try {
            ow.writeXML(instance, null, constraints, file);
        } catch (IOException e) {
            if (logger != null) {
                logger.log(Level.SEVERE, errorMessage, e);
            }
        }
    }

    public static void saveBackChaseResultToXML(Instance backChaseResult, String path, String errorMessage,
            Logger logger) {
        saveToXML(backChaseResult, null, path, false, errorMessage, logger);

    }

    /**
     * Serves as copy method.
     * 
     * @param input  the SingleInput to copy
     * @param logger
     * @return the copy
     */
    public static SingleInput reReadInput(SingleInput input, Logger logger) {

        try {
            // Thread safety: use different names for different threads:
            File file = File.createTempFile(Thread.currentThread().toString(), ".xml");
            file.deleteOnExit();
            saveToXML(input.getInstance(), input.getConstraints(), file, false, "Could not re-read SingleInput",
                    logger);
            InputReader reader = new InputReader();
            return reader.readFile(file);
        } catch (Exception e) {
            String msg = "Could not read temporary file";
            if (logger != null) {
                logger.log(Level.SEVERE, msg, e);
            } else {
                System.out.println(msg);
                e.printStackTrace();
            }
            return null;
        }
    }

}
