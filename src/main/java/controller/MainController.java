package controller;

import controller.guiController.DBController;
import controller.guiController.FileController;
import controller.guiController.MainGuiController;
import controller.guiController.TabController;

public class MainController {
    MainGuiController mainGuiController;
    FileController fileController;
    DBController dbController;
    LogController logController;
    TabController tabController;

    public MainController(MainGuiController mainGuiController) {
        this.mainGuiController = mainGuiController;
        this.logController = new LogController(this);
        this.fileController = new FileController(this);
        this.dbController = new DBController(this);
        this.tabController = new TabController(this);
        this.mainGuiController.setMainController(this);
        this.getMainGuiController().setLogger(this.logController.getProsaLogger());
    }

    public MainGuiController getMainGuiController() {
        return mainGuiController;
    }

    public FileController getFileController() {
        return fileController;
    }

    public DBController getDBController() {
        return dbController;
    }


    public LogController getLogController() {
        return logController;
    }

    public TabController getTabController() {
        return tabController;
    }
}
