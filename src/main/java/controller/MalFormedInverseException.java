package controller;

public class MalFormedInverseException extends Exception {
    public MalFormedInverseException(String string) {
        super(string);
    }

    private static final long serialVersionUID = 1L;
}
