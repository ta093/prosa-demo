package controller;

import java.io.IOException;
import java.net.URL;

import controller.guiController.ErrorSceneController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ErrorController {

    /**
     * Raises an exception.
     * 
     * @param exception The exception to raise
     * @throws IOException
     */
    public static void raise(Throwable exception) throws IOException {
        URL fxmlLocation = ErrorSceneController.class.getResource("/gui/ErrorScene.fxml");
        FXMLLoader loader = new FXMLLoader(fxmlLocation);
        Parent root = loader.load();

        Stage stage = new Stage();
        Scene scene = new Scene(root, 640, 640);

        stage.setScene(scene);
        stage.setTitle("ProSA - " + exception.getClass().getSimpleName());

        ErrorSceneController controller = loader.getController();
        controller.setScene(scene);
        controller.setException(exception);

        stage.show();
    }

}
