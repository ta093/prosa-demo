package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class LogController {
    MainController mainController;
    Logger prosaLogger;

    /*
     * How to log: - use chateaulog exclusively for the 2 chase executions,
     * everything else should be logged within prosaLogger -
     * chateauLog.addEntry("my entry") - prosaLogger.log(Level.INFO, "my entry")
     * 
     * How to use ProSA Log Levels: - INFO - information for the user (everything is
     * fine) - WARNING - the user made a mistake (e.g. wrong db config) - SEVERE -
     * error within the program which shouldn't happen
     */

    public LogController(MainController mainController) {
        this.mainController = mainController;

        prosaLogger = Logger.getLogger("ProSA Log");
        prosaLogger.addHandler(new Handler() {
            public void publish(LogRecord record) {
                StringBuilder sb = new StringBuilder();

                DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date logDate = new Date(record.getMillis());

                sb.append(formatter.format(logDate)).append(" - ").append(record.getLevel()).append(": ")
                        .append(record.getMessage()).append("\n");
                mainController.getMainGuiController().getProsaLogTextArea().appendText(sb.toString());
            }

            public void flush() {
            }

            public void close() throws SecurityException {
            }
        });
    }

    public Logger getProsaLogger() {
        return prosaLogger;
    }
}
