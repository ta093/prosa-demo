package controller.guiController;

import controller.MainController;

public class TabController {
    MainController mainController;

    public TabController(MainController mainController) {
        this.mainController = mainController;
    }

    public void addTabListener() {
        MainGuiController mainGuiController = mainController.getMainGuiController();
        mainGuiController.getTabPane().getSelectionModel().selectedItemProperty()
                .addListener((observableValue, oldTab, newTab) -> {
                    if (oldTab.equals(mainGuiController.getDbconfigTab())) {
                        mainController.getFileController().saveDbConfiguration();
                        mainController.getDBController().startConnection();
                    }

                    if (newTab.equals(mainGuiController.getChaseTab())) {
                        onChaseTabSelected();
                    } else if (newTab.equals(mainGuiController.getBackchaseTab())) {
                        onBackchaseTabSelected();
                    } else if (newTab.equals(mainGuiController.getLogTab())) {
                        onLogTabSelected();
                    } else if (newTab.equals(mainGuiController.getAnonymizerTab())) {
                        onAnonymizerTabSelected();
                    } else if(newTab.equals(mainGuiController.getEvolutionTab())) {
                    	onEvolutionTabSelected();
                    }
                });
    }

    public void onEvolutionTabSelected() {
    	 mainController.getDBController().printDatabase(
                 mainController.getMainGuiController().getEvolutionTabOrigDatabaseTuplesTextField(),
                 mainController.getMainGuiController().getEvolutionTabOrigDatabaseSchemaTextField());
    }
    
    public void onChaseTabSelected() {
    	if(!mainController.getMainGuiController().getWithEvolution()) {
        mainController.getDBController().printDatabase(
                mainController.getMainGuiController().getChaseTabOrigDatabaseTuplesTextField(),
                mainController.getMainGuiController().getChaseTabOrigDatabaseSchemaTextField());
       
    	}
       
    }

    public void onBackchaseTabSelected() {
        mainController.getDBController().printDatabase(
                mainController.getMainGuiController().getBackchaseTabOrigDatabaseTuplesTextField(),
                mainController.getMainGuiController().getBackchaseTabOrigDatabaseSchemaTextField());
    }

    public void onAnonymizerTabSelected() {
        // print tuples to original text box
        mainController.getDBController()
                .printTuples(mainController.getMainGuiController().getAnonTabOrigDBTuplesTextField());

        // entry point for tab change
    }

    public void onLogTabSelected() {

    }
}
