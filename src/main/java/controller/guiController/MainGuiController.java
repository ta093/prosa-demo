package controller.guiController;

import static util.Constants.LINE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import atom.RelationalAtom;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;
import org.jdom2.JDOMException;

import anonymizer.anonymization.Anonymization;
import anonymizer.generalizations.GeneralizationHierarchy;
import anonymizer.io.HierarchyReader;
import anonymizer.structures.Table;
import anonymizer.util.Converter;
import api.ChaseService;
import api.TerminationCheckFailure;
import atom.ProvenanceInformation;
import chase.Chase;
import constraints.Constraint;
import constraints.STTgd;
import constraints.Tgd;
import controller.ErrorController;
import controller.MainController;
import instance.Instance;
import instance.SchemaTag;
import inverse.InverseUtils;
import inverse.InvertingTechniques;
import io.ChateauLogger;
import io.InputReader;
import io.SingleInput;
import jakarta.xml.bind.JAXBException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import provenance.DeDuplifier;
import service.Combiner;
import service.InvalidConstraintsFormat;
import service.ProSAService;
import service.pipelineState.BackChased;
import service.pipelineState.Chased;
import service.pipelineState.Parsed;
import service.pipelineState.ReproducibilityFailure;
import service.pipelineState.WithRewrittenInput;
import sql2sttgd.api.ParserDefaultService;
import sql2sttgd.dbreader.NoColumnsException;
import sql2sttgd.parser.exception.ParsingException;
import sql2sttgd.transformation.exception.TransformationException;
import term.Term;
import util.Constants;
import util.IOUtils;

public class MainGuiController {

	private static final String SLS = System.lineSeparator();
	private final DeDuplifier deDuplifier = new DeDuplifier();
	MainController mainController;
	Stage mainStage;
	private Logger logger;
	private FileChooser fileChooser;
	private File file;

	// needed for evolution
	private SingleInput origEvolutionInput;
	private SingleInput evolutionInput;
	private Instance evolutionOutput;
	private Instance backEvolutionInputInstance;
	private boolean withEvolution = false;

	private HashMap<String, GeneralizationHierarchy> hierarchies;
	private ArrayList<Table> minSubDBTables;
	private LinkedHashMap<String, String> indicesMap = new LinkedHashMap<>();

	@FXML
	private Tab dbconfigTab;

	@FXML
	private TabPane tabPane;

	@FXML
	private Button testConnectionButton;

	@FXML
	private Button saveButton;

	@FXML
	private TextField urlTextfield;

	@FXML
	private TextField portTextfield;

	@FXML
	private TextField dbnameTextfield;

	@FXML
	private TextField userTextfield;

	@FXML
	private Label startHeadingLabel;

	@FXML
	private Label copyrightLabel;

	@FXML
	private Label connectionResultLabel;

	@FXML
	private PasswordField pwdTextfield;

	@FXML
	private TextField evolutionPathTextField;

	@FXML
	private Button openEvolutionFileButton;

	// Evolution Tab:

	@FXML
	private Tab evolutionTab;

	@FXML
	private TextArea evolutionTabShowOperatorTextArea;

	@FXML
	private TextArea evolutionTabOrigDatabaseTuplesTextField;

	@FXML
	private TextArea evolutionTabOrigDatabaseSchemaTextField;

	@FXML
	private TextArea evolutionTabResultSchema;

	@FXML
	private TextArea evolutionTabResultTuples;

	@FXML
	private Button evolutionTabProcessEvolutionButton;

	// Chase Tab:

	@FXML
	private Tab chaseTab;

	@FXML
	private TextArea chaseTabQueryTextfield;

	@FXML
	private TextArea chaseTabOrigDatabaseTuplesTextField;

	@FXML
	private TextArea chaseTabOrigDatabaseSchemaTextField;

	@FXML
	private Button chaseTabRunProsaButton;

	@FXML
	private TextArea chaseTabSttgdTextField;

	@FXML
	private TextArea chaseTabResultWithoutProvTextField;

	@FXML
	private TextArea chaseTabResultWithProvTextField;

	@FXML
	private Button chaseTabDownloadButton;

	@FXML
	private Button chaseTabNextStepButton;

	@FXML
	private Tab chaseProvenanceTab;

	// Backchase tab:

	@FXML
	private Tab backchaseTab;

	@FXML
	private Text backChaseTabInverseTypeText;

	@FXML
	private TextArea backChaseTabInverseQueryTextfield;

	@FXML
	private TextArea backchaseTabOrigDatabaseTuplesTextField;

	@FXML
	private TextArea backchaseTabOrigDatabaseSchemaTextField;

	@FXML
	private Button backChaseTabPreviousStepButton;

	@FXML
	private Button backchaseDownloadButton;

	@FXML
	private TextArea whereProvenanceResultTextfield;

	@FXML
	private TextArea whyProvenanceResultTextfield;

	@FXML
	private TextArea howProvenanceResultTextfield;

	@FXML
	private TextArea prosaProvenanceResultTextfield;

	@FXML
	private Button showLogButton;

	@FXML
	private TextArea backChaseTabSubDBWithoutProvTextfield;

	@FXML
	private TextArea backChaseTabSubDBWithProvTextfield;

	@FXML
	private Tab backchaseProvenanceTab;

	@FXML
	private TabPane provenanceTabPane;

	// Anonymizer Tab:
	@FXML
	private Spinner<Double> anonTabdistinctRatioSpinner;

	@FXML
	private Button anonTabSelectHierarchiesFileButton;

	@FXML
	private Tab anonymizerTab;
	@FXML
	private TextArea anonTabOrigDBTuplesTextField;

	@FXML
	private TextArea anonTabMinimalDatabaseTuplesTextField;

	@FXML
	private Spinner<Integer> anonTab_K_AnonymitySpinner;

	@FXML
	private Spinner<Integer> anonTab_L_DiversitySpinner;

	@FXML
	private ComboBox<String> anonTabTablesComboBox;

	@FXML
	private TextField anonTabAttributesTextField;

	@FXML
	private Button anonTabSaveIdsButton;

	// Inverse Evolution Tab:
	@FXML
	private Tab invEvolutionTab;

	@FXML
	private Button invEvolutionTabProcessInvEvolutionButton;

	@FXML
	private TextArea invEvolutionTabOperatorTextfield;

	@FXML
	private TextArea invEvolutionTabResultSchemaTextField;

	@FXML
	private TextArea invEvolutionTabResultTuplesTextField;

	@FXML
	private TextArea invEvolutionTabInputTuplesTextField;

	@FXML
	private TextArea invEvolutionTabInputSchemaTextField;

	// Log Tab:
	@FXML
	private Tab logTab;

	@FXML
	private Button anonTabSelectConfigFileButton;

	@FXML
	private Button anonTabRunAnonymizerButton;

	@FXML
	private Button anonTabPreviousStepButton;

	@FXML
	private TextArea anonTabAnonymizedSubDBTextField;

	@FXML
	private Button anonTabShowLogButton;

	@FXML
	void handleBackToChase(ActionEvent event) {
		startChaseTab();
	}

	@FXML
	void handleChaseDownload(ActionEvent event) {
		mainController.getFileController().saveChase(chaseTabQueryTextfield, chaseTabOrigDatabaseSchemaTextField,
				chaseTabOrigDatabaseTuplesTextField, chaseTabSttgdTextField, chaseTabResultWithoutProvTextField,
				backChaseTabInverseQueryTextfield, backChaseTabSubDBWithoutProvTextfield);
	}

	@FXML
	void handleLogDownload(ActionEvent event) {
		mainController.getFileController().saveLog();
	}

	@FXML
	void handleShowChaseTab(ActionEvent event) {
		startChaseTab();
	}

	@FXML
	void handleShowBackChase(ActionEvent event) {
		tabPane.getSelectionModel().select(backchaseTab);
		mainController.getTabController().onBackchaseTabSelected();
	}

	@FXML
	void handleShowAnonymizerOrInvEvo(ActionEvent event) {
		if (withEvolution) {
			tabPane.getSelectionModel().select(invEvolutionTab);
		} else {
			tabPane.getSelectionModel().select(anonymizerTab);
			mainController.getTabController().onAnonymizerTabSelected();
		}
	}

	@FXML
	void handleShowAnonymizer(ActionEvent event) {
		tabPane.getSelectionModel().select(anonymizerTab);
		mainController.getTabController().onAnonymizerTabSelected();
	}

	@FXML
	void handleRun(ActionEvent event) throws NoColumnsException, IOException, JDOMException, TransformationException,
			SQLException, JAXBException, ParsingException {
		registerListeners();

		backChaseTabInverseTypeText.setText(detectInverseType(getQueryTextfield().getText()));

		ProSAService ps = new ProSAService(logger, mainController.getDBController().getDbConn());

		if (withEvolution) {
			ps.setWithEvolution(this.evolutionOutput);
		}

		try {
			// Parse step:
			Parsed parsed = ps.parse(getQueryTextfield().getText());
			printConstraints(parsed.getConstraints(), getSttgdTextField());

			// Run without provenance; chase step:
			Chased chasedWithoutProvenance = parsed.chase();
			chaseTabResultWithoutProvTextField.setText(chasedWithoutProvenance.getChaseResult().toString());

			// Backchase step:
			BackChased backChasedWithoutProv = chasedWithoutProvenance.backChase();
			try {
				getinverseQueryTextArea()
						.setText(backChasedWithoutProv.getInverse().iterator().next().toString().trim());
			} catch (NoSuchElementException ex) {
			}

			try {
				backChasedWithoutProv.assertCorrectResultWithSubDB();
				backChaseTabSubDBWithoutProvTextfield.setText(backChasedWithoutProv.getBackChaseResult().toString());
			} catch (ReproducibilityFailure e) {
				logger.log(Level.SEVERE, "", e);
				Platform.runLater(() -> backChaseTabSubDBWithoutProvTextfield
						.setText("Cannot reproduce result with this sub-DB:" + LINE
								+ backChasedWithoutProv.getBackChaseResult().toString()));
			}

			if (withEvolution) {
				// preparation for backEvolution
				backEvolutionInputInstance = backChasedWithoutProv.getBackChaseResult();
				backEvolutionInputInstance.getSchema().putAll(origEvolutionInput.getInstance().getSchema());
				backEvolutionInputInstance.getSchemaTags().putAll(origEvolutionInput.getInstance().getSchemaTags());

				// generate backEvolution-Tab output
				String schemaOutput = "";

				for (Entry<String, LinkedHashMap<String, String>> schema : backEvolutionInputInstance.getSchema()
						.entrySet()) {
					schemaOutput += schema.getKey() + "(";
					for (Entry<String, String> attribute : schema.getValue().entrySet()) {
						schemaOutput += attribute.getKey() + ",";
					}

					// remove "," after last attribute
					schemaOutput = schemaOutput.substring(0, schemaOutput.length() - 1);
					schemaOutput += ")" + SLS;
				}

				invEvolutionTabInputTuplesTextField.setText(backEvolutionInputInstance.toString());
				invEvolutionTabInputSchemaTextField.setText(schemaOutput);
			}

			if (!ps.hasAggregates(getQueryTextfield().getText())) {
				toggleAggregateFeatures(false);

				// Run with provenance; rewrite chase input
				WithRewrittenInput withRewrittenQuery = parsed.rewriteInputForProvenance();

				// chase step
				Chased chasedWithProv = withRewrittenQuery.chase();
				var chaseResult = chasedWithProv.getChaseResult();
				var relationalAtomsBySchema = chaseResult.getRelationalAtomsBySchema("Result");
				var relationAtomsWithoutDuplicates = deDuplicateAtoms(relationalAtomsBySchema);
				showProvenance(relationAtomsWithoutDuplicates);
				chaseTabResultWithProvTextField.setText(chaseResult.toString());

				// backchase step
				BackChased backChasedWithProv = chasedWithProv.backChase();
				Instance minSubDB = backChasedWithProv.getBackChaseResult();

				try {
					backChasedWithProv.assertCorrectResultWithSubDB();
					backChaseTabSubDBWithProvTextfield.setText(minSubDB.toString());
				} catch (ReproducibilityFailure e) {
					logger.log(Level.SEVERE, "", e);
					Platform.runLater(() -> backChaseTabSubDBWithProvTextfield
							.setText("Cannot reproduce result with this sub-DB:" + LINE
									+ minSubDB.toString()));
				}

				backChaseTabSubDBWithProvTextfield.setText(minSubDB.toString());

				// anonymizer
				if (!withEvolution) {
					prepareAnonymizer(minSubDB);
				}
			} else {
				// disable unsupported features
				toggleAggregateFeatures(true);
			}

			StringBuilder chateauLogs = new StringBuilder();
			ChateauLogger.getRecords().forEach(rec -> chateauLogs.append(rec.getMessage() + SLS));
			chateauLogTextfield.setText(chateauLogs.toString());

			// Set 'Next Step' button as default:
			chaseTabNextStepButton.setDefaultButton(true);
			chaseTabRunProsaButton.setDefaultButton(false);
		} catch (IOException | TerminationCheckFailure e) {
			logger.log(Level.SEVERE, "", e);
		} catch (NoColumnsException | TransformationException | SQLException | JAXBException
				| InvalidConstraintsFormat | JDOMException | ParsingException | NotImplementedException e1) {
			logger.log(Level.SEVERE, "", e1);
			getSttgdTextField().setText(e1.getMessage());
		}

	}

	private LinkedHashSet<RelationalAtom> deDuplicateAtoms(LinkedHashSet<RelationalAtom> relationalAtomsBySchema) {
		return deDuplifier.deDuplicateAtoms(relationalAtomsBySchema);
	}

	private int findIndex(ArrayList<Term> terms){
		return deDuplifier.findIndex(terms);
	}

	private void toggleAggregateFeatures(boolean disabled) {
		// clear elements
		try {
			whereProvenanceResultTextfield.clear();
			whyProvenanceResultTextfield.clear();
			howProvenanceResultTextfield.clear();

			chaseTabResultWithProvTextField.clear();
			backChaseTabSubDBWithProvTextfield.clear();
		} catch (NullPointerException ignored) {

		}

		// enable or disable elements
		chaseProvenanceTab.setDisable(disabled);
		backchaseProvenanceTab.setDisable(disabled);
		provenanceTabPane.setDisable(disabled);
	}

	private void prepareAnonymizer(Instance instance) {
		anonTabMinimalDatabaseTuplesTextField.setText(instance.toString());
		minSubDBTables = Converter.fromInstanceToTables(instance);

		// reset indices map and combo box
		indicesMap.clear();
		anonTabTablesComboBox.getItems().clear();

		minSubDBTables.forEach(table -> {
			if (table.size() > 0) {
				anonTabTablesComboBox.getItems().add(table.getName());
			}
		});
	}

	private void registerListeners() {
		anonTabTablesComboBox.getSelectionModel().selectedItemProperty().addListener(
				(options, oldValue, newValue) -> {
					anonTabAttributesTextField.setText(indicesMap.getOrDefault(newValue, ""));
				});
	}

	private void anonymize(ArrayList<Table> tables, int k, double dr) {
		// reset hierarchy step counters
		for (GeneralizationHierarchy hierarchy : hierarchies.values()) {
			hierarchy.reset();
		}

		ArrayList<Table> anonymizedTables = new ArrayList<>();

		// add hierarchies to table attributes and anonymize tables
		for (Table table : tables) {
			addHierarchies(table);

			String[] indicesRaw = indicesMap.getOrDefault(table.getName(), "").split(",");
			int[] ids = new int[indicesRaw.length];

			for (int i = 0; i < indicesRaw.length; i++) {
				int index = Integer.parseInt(indicesRaw[i]);
				ids[i] = index;
			}

			// anonymize table and store anonymized table
			Table anonymizedTable = Anonymization.anonymizeTable(table, k, dr, ids, -1);
			anonymizedTables.add(anonymizedTable);
		}

		// display anonymized tables in text field
		anonTabAnonymizedSubDBTextField.clear();

		Instance anonymizedInstance = Converter.fromTablesToInstance(anonymizedTables);
		anonTabAnonymizedSubDBTextField.setText(anonymizedInstance.toString());

		// TODO send anonymizedInstance to evolution step?
	}

	/**
	 * Prints the constraints from the parser to the according TextArea.
	 * 
	 * @param constraints    the constraints the parser yielded. Should contain
	 *                       exactly one s-t tgd, if the SQL query does not contain
	 *                       aggregate functions..
	 * @param sttgdTextField the TextArea where the constraints should be printed.
	 */
	public static void printConstraints(LinkedHashSet<Constraint> constraints, TextArea sttgdTextField) {
		StringBuilder constraintsString = new StringBuilder();

		for (Constraint c : constraints) {
			constraintsString.append(c.getTypeName()).append(": ").append(c).append(SLS).append(SLS);
		}

		sttgdTextField.setText(constraintsString.toString());
	}

	private String detectInverseType(String sqlQuery) {
		sqlQuery = sqlQuery
				.replace(System.lineSeparator(), " ")
				.replace("(", "")
				.replace(")", "");

		ArrayList<String> tokens = new ArrayList<>(Arrays.asList(sqlQuery.split(" ")));

		String inverseTypeWithhoutProvenance = InverseUtils.findInverseTypeWithoutProvenance(tokens).toString();
		String inverseTypeWithProvenance = InverseUtils.findInverseTypeWithProvenance(tokens).toString();
		String inverseType = inverseTypeWithhoutProvenance + " / " + inverseTypeWithProvenance;

		return inverseType;
	}

	/**
	 * Prints provenance information to the three provenance text fields.
	 *
	 * @param relationalAtomsBySchema
	 */
	private void showProvenance(LinkedHashSet<RelationalAtom> relationalAtomsBySchema) {
		StringBuilder whereProv = new StringBuilder();
		StringBuilder whyProv = new StringBuilder();
		StringBuilder howProv = new StringBuilder();

		relationalAtomsBySchema.forEach(atom -> {

			ProvenanceInformation prov = atom.getProvInfo();

			/**
			 * toString generation taken from {@link ProvenanceInformation.toString} and
			 * slightly modified, because we need each provenance type individually here.
			 */
			whereProv.append("ID: ").append(prov.getId()).append(" - " + printAtom(atom)).append(SLS);
			whereProv.append("Tuples: " + prov.getWhereTupleProvenance()).append(SLS);
			whereProv.append("Relations: (");
			whereProv.append(String.join(", ", prov.getWhereRelationsProvenance()));
			whereProv.append(")").append(LINE);

			// why
			whyProv.append("ID: ").append(prov.getId()).append(" - " + printAtom(atom)).append(SLS);
			List<String> witnessSetStrings = new ArrayList<String>();
			for (ArrayList<String> tupleList : prov.getWhyProvenance()) {
				String tupleListString = "{" + String.join(", ", tupleList) + "}";
				witnessSetStrings.add(tupleListString);
			}
			whyProv.append("{");
			whyProv.append(String.join(", ", witnessSetStrings));
			whyProv.append("}").append(LINE);

			// how
			howProv.append("ID: ").append(prov.getId()).append(" - " + printAtom(atom)).append(SLS);
			howProv.append(prov.getPolynom()).append(LINE);

		});
		whereProvenanceResultTextfield.setText(whereProv.toString());
		whyProvenanceResultTextfield.setText(whyProv.toString());
		howProvenanceResultTextfield.setText(howProv.toString());
	}

	public String printAtom(RelationalAtom atom) {
		var terms = atom.getTerms();
		var index = findIndex(terms);
		StringBuilder builder = new StringBuilder();

		builder.append(atom.getRelationName());
		builder.append("(");

		String sepTerm = "";

		for (int i = 0; i < index; i++) {
			builder.append(sepTerm);
			sepTerm = ", ";
			builder.append(terms.get(i).toString());
		}

		builder.append(")");
		return builder.toString();
	}

	@FXML
	void handleSave(ActionEvent event) {
		mainController.getFileController().saveDbConfiguration();
		mainController.getDBController().startConnection();

		if (evolutionTab.isDisabled() == false) {
			startEvolutionTab();
		} else {
			startChaseTab();
		}
	}

	@FXML
	void handleShowLog(ActionEvent event) {
		tabPane.getSelectionModel().select(logTab);
		mainController.getTabController().onLogTabSelected();
	}

	@FXML
	void handleTestConnection(ActionEvent event) {
		mainController.getFileController().saveDbConfiguration();
		mainController.getDBController().testConnection(connectionResultLabel);
	}

	@FXML
	void handleSelectHierarchiesFile(ActionEvent event) throws FileNotFoundException {
		FileChooser chooser = new FileChooser();
		ExtensionFilter filter = new ExtensionFilter("Hierarchy files", "*.txt");
		chooser.getExtensionFilters().add(filter);
		File file = chooser.showOpenDialog(mainStage);

		if (file == null) {
			return;
		}

		HierarchyReader reader = new HierarchyReader(file);
		hierarchies = reader.getHierarchies();

		anonTabRunAnonymizerButton.setDisable(false);
	}

	@FXML
	void handleRunAnonymizer(ActionEvent event) throws Exception {
		int k = anonTab_K_AnonymitySpinner.getValue();
		double dr = anonTabdistinctRatioSpinner.getValue();

		anonymize(minSubDBTables, k, dr);
	}

	@FXML
	void handleSaveIds(ActionEvent event) {
		String indicesString = anonTabAttributesTextField.getText();
		indicesMap.put(anonTabTablesComboBox.getSelectionModel().getSelectedItem(), indicesString);
	}

	/**
	 * Adds generalization hierarchies to a table.
	 * 
	 * @param table The table to add hierarchies to
	 */
	private void addHierarchies(Table table) {
		for (String attributeName : hierarchies.keySet()) {
			if (table.getAttribute(attributeName) != null) {
				table.getAttribute(attributeName).setHierarchy(hierarchies.get(attributeName));
			}
		}
	}

	public TextField getUrlTextfield() {
		return urlTextfield;
	}

	public TextField getPortTextfield() {
		return portTextfield;
	}

	public TextField getDbnameTextfield() {
		return dbnameTextfield;
	}

	public TextField getUserTextfield() {
		return userTextfield;
	}

	public TextArea getProsaLogTextArea() {
		return prosaLogTextfield;
	}

	public TextArea getChateauLogTextfield() {
		return chateauLogTextfield;
	}

	public TextArea getChaseTabOrigDatabaseTuplesTextField() {
		return chaseTabOrigDatabaseTuplesTextField;
	}

	public TextArea getChaseTabOrigDatabaseSchemaTextField() {
		return chaseTabOrigDatabaseSchemaTextField;
	}

	public TextArea getSttgdTextField() {
		return chaseTabSttgdTextField;
	}

	public TextArea getBackchaseTabOrigDatabaseSchemaTextField() {
		return backchaseTabOrigDatabaseSchemaTextField;
	}

	public TextArea getBackchaseTabOrigDatabaseTuplesTextField() {
		return backchaseTabOrigDatabaseTuplesTextField;
	}

	public PasswordField getPwdTextfield() {
		return pwdTextfield;
	}

	public TextArea getinverseQueryTextArea() {
		return backChaseTabInverseQueryTextfield;
	}

	public TabPane getTabPane() {
		return tabPane;
	}

	public Tab getDbconfigTab() {
		return dbconfigTab;
	}

	public Tab getChaseTab() {
		return chaseTab;
	}

	public Tab getBackchaseTab() {
		return backchaseTab;
	}

	public Tab getLogTab() {
		return logTab;
	}

	public Stage getMainStage() {
		return mainStage;
	}

	public void setMainStage(Stage mainStage) {
		this.mainStage = mainStage;
	}

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}

	public void startChaseTab() {
		tabPane.getSelectionModel().select(chaseTab);
		mainController.getTabController().onChaseTabSelected();
	}

	public TextArea getQueryTextfield() {
		return chaseTabQueryTextfield;
	}

	@FXML
	private Button logDownloadButton;

	@FXML
	private TextArea chateauLogTextfield;

	@FXML
	private TextArea prosaLogTextfield;

	@FXML
	void handleShowEvolutionTab(ActionEvent event) {
		startEvolutionTab();
	}

	// handling the Process Evolution button in the evolution Tab
	@FXML
	void handleProcessEvolution(ActionEvent event) throws Exception {
		// run chase with extended schema Instance and dependencies from evolution.xml
		ChaseService.runTests(evolutionInput.getInstance(), evolutionInput.getConstraints());
		Chase chase = new Chase(evolutionInput.getInstance(), evolutionInput.getConstraints());

		Instance evolutionOutput = chase.chase();

		// build string for schema output
		HashMap<String, SchemaTag> schemaTags = evolutionOutput.getSchemaTags();
		String schemaOutput = "";
		for (Entry<String, LinkedHashMap<String, String>> schema : evolutionOutput.getSchema().entrySet()) {
			if (schemaTags.get(schema.getKey()).getToken().equals("T")) {
				schemaOutput += schema.getKey() + "(";
				for (Entry<String, String> attribute : schema.getValue().entrySet()) {
					schemaOutput += attribute.getKey() + ", ";
				}
				// remove ", " after last attribute
				schemaOutput = schemaOutput.substring(0, schemaOutput.length() - 2);
				schemaOutput += ")" + SLS;
			}
		}

		// set evolution output in the evolution tab
		evolutionTabResultSchema.setText(schemaOutput);
		evolutionTabResultTuples.setText(evolutionOutput.toString());

		// set evolution output in the chase tab
		chaseTabOrigDatabaseTuplesTextField.setText(evolutionOutput.toString());
		chaseTabOrigDatabaseSchemaTextField.setText(schemaOutput);

		// preparation for next steps. (withEvolution switch to tell the parser which
		// schema to use)
		setWithEvolution(true);
		this.evolutionOutput = evolutionOutput;
		invEvolutionTab.setDisable(false);

		String constraintsString = "";
		System.out.println("-Constraints:");
		evolutionInput.getConstraints().forEach(c -> System.out.println(c.getTypeName() + ":" + SLS + c + SLS));
		for (Constraint c : origEvolutionInput.getConstraints()) {
			constraintsString += c.getTypeName() + ": " + SLS + c + SLS + SLS;
		}

		invEvolutionTabOperatorTextfield.setText(constraintsString);

		// save evolution result into src/main/resources/pipelineSteps
		IOUtils.saveInstanceToXMLWithoutProv(evolutionOutput, Constants.ACTUALS.EVOLUTION_RESULT.toString(),
				"Could not save evolution result to XML.", logger);
	}

	@FXML
	void handleInverseEvolution(ActionEvent event) throws Exception {
		IOUtils.saveInstanceToXMLWithoutProv(backEvolutionInputInstance,
				Constants.ACTUALS.BACK_EVOLUTION_INPUT.toString(), "Could not save back evolution result to XML.",
				logger);

		Chase chase = new Chase(backEvolutionInputInstance, origEvolutionInput.getConstraints());
		Instance backEvolutionOutput = chase.chase();

		IOUtils.saveInstanceToXMLWithoutProv(backEvolutionOutput, Constants.ACTUALS.BACK_EVOLUTION_RESULT.toString(),
				"Could not save back evolution result to XML.", logger);

		// build string for schema output
		HashMap<String, SchemaTag> schemaTags = evolutionOutput.getSchemaTags();
		String schemaOutput = "";
		for (Entry<String, LinkedHashMap<String, String>> schema : backEvolutionOutput.getSchema().entrySet()) {
			if (schemaTags.get(schema.getKey()).getToken().equals("S")) {
				schemaOutput += schema.getKey() + "(";
				for (Entry<String, String> attribute : schema.getValue().entrySet()) {
					schemaOutput += attribute.getKey() + ",";
				}

				// remove "," after last attribute
				schemaOutput = schemaOutput.substring(0, schemaOutput.length() - 1);
				schemaOutput += ")" + SLS;
			}
		}

		invEvolutionTabResultTuplesTextField.setText(backEvolutionOutput.toString());
		invEvolutionTabResultSchemaTextField.setText(schemaOutput);

		prepareAnonymizer(backEvolutionOutput);
	}

	private void openEvolutionFile(File file) throws Exception {
		InputReader reader = new InputReader();

		try {
			evolutionInput = reader.readFile(file);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorController.raise(e);
			return;
		}

		origEvolutionInput = evolutionInput;

		// get database schema and instance
		Connection dbConn = mainController.getDBController().getDbConn();
		ParserDefaultService parser = new ParserDefaultService();
		ByteArrayOutputStream parsedByteStream = parser.generateEvolutionXml(dbConn,
				mainController.getDBController().getAllTables(dbConn));
		InputReader inputReader = new InputReader();
		SingleInput input = inputReader.readStream(parsedByteStream);

		// add target schema, schematags and instance tuples to the instance (from
		// evolution.xml)
		input.getInstance().getSchema().putAll(evolutionInput.getInstance().getSchema());
		input.getInstance().getSchemaTags().putAll(evolutionInput.getInstance().getSchemaTags());
		input.getInstance().getRelationalAtoms().addAll(evolutionInput.getInstance().getRelationalAtoms());

		// stream sttgds and tgs for the inverter
		LinkedHashSet<STTgd> sttgds = new LinkedHashSet<>();
		sttgds.addAll(this.evolutionInput.getConstraints().stream().filter(con -> (con instanceof STTgd))
				.map(sttgd -> (STTgd) sttgd).collect(Collectors.toSet()));

		LinkedHashSet<Tgd> tgds = new LinkedHashSet<>();
		tgds.addAll(this.evolutionInput.getConstraints().stream().filter(con -> (con instanceof Tgd))
				.map(tgd -> (Tgd) tgd).collect(Collectors.toSet()));

		Pair<LinkedHashSet<STTgd>, Optional<Tgd>> inverse = InvertingTechniques.invert(sttgds, tgds);
		LinkedHashSet<Constraint> constraints = new LinkedHashSet<>();
		constraints.addAll(inverse.getLeft());

		Optional<Tgd> tgd = inverse.getRight();
		if (tgd.isEmpty()) {
			logger.log(Level.FINE, "Inverse does not contain a tgd, skipping...");
		} else {
			logger.log(Level.FINE, "Adding tgd to inverse...");
			constraints.add(tgd.get());
		}

		// combine schema, tupel and inverted constraints
		evolutionInput = Combiner.combine(input.getInstance(), constraints, logger);
		IOUtils.saveSingleInput(evolutionInput, Constants.ACTUALS.EVOLUTION_INPUT.toString(),
				"Could not save evolution input to XML.", logger);

		// generate inverted constraint output in Evolution-Tab
		String constraintsString = "";
		System.out.println("-Constraints:");
		evolutionInput.getConstraints().forEach(c -> System.out.println(c.getTypeName() + ":" + SLS + c + SLS));

		for (Constraint c : evolutionInput.getConstraints()) {
			constraintsString += c.getTypeName() + ": " + SLS + c + SLS + SLS;
		}

		evolutionTabShowOperatorTextArea.setText(constraintsString);

		this.file = file;
	}

	@FXML
	void handleOpenEvolutionFile(ActionEvent event) throws Exception {

		// define FileChooser; filter *.xml files
		fileChooser = new FileChooser();
		ExtensionFilter filter = new ExtensionFilter("Evolution file (*.xml)", "*.xml");
		fileChooser.getExtensionFilters().add(filter);
		fileChooser.setInitialDirectory(new File("."));
		fileChooser.setTitle("Open Evolution (*.xml) file");

		// show FileChooser
		file = fileChooser.showOpenDialog(mainStage);

		if (file == null) {
			return;
		}

		mainController.getDBController().startConnection();

		// add path to evolution text field
		evolutionPathTextField.setText(file.getName());

		// enable evolution Tab
		evolutionTab.setDisable(false);

		openEvolutionFile(file);

	}

	public TextField getEvolutionPathTextField() {
		return evolutionPathTextField;
	}

	public TextArea getEvolutionTabOrigDatabaseTuplesTextField() {
		return evolutionTabOrigDatabaseTuplesTextField;
	}

	public TextArea getEvolutionTabOrigDatabaseSchemaTextField() {
		return evolutionTabOrigDatabaseSchemaTextField;
	}

	public Tab getEvolutionTab() {
		return evolutionTab;
	}

	public void startEvolutionTab() {
		tabPane.getSelectionModel().select(evolutionTab);
		mainController.getTabController().onEvolutionTabSelected();
	}

	public void setLogger(Logger prosaLogger) {
		this.logger = prosaLogger;
	}

	public Tab getAnonymizerTab() {
		return anonymizerTab;
	}

	public TextArea getAnonTabOrigDBTuplesTextField() {
		return anonTabOrigDBTuplesTextField;
	}

	public Instance getEvolutionOutput() {
		return evolutionOutput;
	}

	public void setEvolutionOutput(Instance evolutionOutput) {
		this.evolutionOutput = evolutionOutput;
	}

	public Boolean getWithEvolution() {
		return withEvolution;
	}

	public void setWithEvolution(Boolean withEvolution) {
		this.withEvolution = withEvolution;
	}
}
