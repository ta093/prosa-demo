package controller.guiController;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;

public class ErrorSceneController {

    private Scene scene;

    @FXML
    private Button closeButton;

    @FXML
    private Button copyToClipboardButton;

    @FXML
    private TextArea exceptionTextArea;

    @FXML
    void handleClose(ActionEvent event) {
        // close window
        scene.getWindow().hide();
    }

    @FXML
    void handleCopyToClipboard(ActionEvent event) {
        // paste exception text into clipboard
        Clipboard.getSystemClipboard().setContent(Map.of(DataFormat.PLAIN_TEXT, exceptionTextArea.getText()));
    }

    /**
     * Sets the scene for the controller.
     * 
     * @param scene The JavaFX scene the content is displayed in
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    /**
     * Provides the exception to the GUI.
     * 
     * @param exception The exception that occurs
     */
    public void setException(Throwable exception) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);

        exceptionTextArea.setText(stringWriter.toString());
    }

}
