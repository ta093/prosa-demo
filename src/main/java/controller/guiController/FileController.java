package controller.guiController;

import controller.MainController;
import util.Constants;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static util.Constants.LINE;
import static util.Constants.SLS;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileController {
    MainController mainController;
    Logger logger;

    public FileController(MainController mainController) {
        this.mainController = mainController;
        logger = mainController.getLogController().getProsaLogger();
    }

    @SuppressWarnings("unchecked")
    public void saveDbConfiguration() {
        JSONObject configurationJson = new JSONObject();
        configurationJson.put("host", mainController.getMainGuiController().getUrlTextfield().getText());
        configurationJson.put("port", mainController.getMainGuiController().getPortTextfield().getText());
        configurationJson.put("dbName", mainController.getMainGuiController().getDbnameTextfield().getText());
        configurationJson.put("user", mainController.getMainGuiController().getUserTextfield().getText());
        configurationJson.put("password", mainController.getMainGuiController().getPwdTextfield().getText());

        try {
            FileWriter file = new FileWriter("config.json");
            file.write(configurationJson.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while saving database configuration.");
        }
    }

    // parses the DBConfig and prints it into the GUI
    public boolean loadDbConfiguration() {

        MainGuiController mainGuiController = mainController.getMainGuiController();
        TextField dbnameTextfield = mainGuiController.getDbnameTextfield();
        TextField userTextfield = mainGuiController.getUserTextfield();
        PasswordField pwdTextfield = mainGuiController.getPwdTextfield();
        TextField urlTextfield = mainGuiController.getUrlTextfield();
        TextField portTextfield = mainGuiController.getPortTextfield();

        File f = new File(Constants.jsonDBConfigPath);
        if (f.exists() && !f.isDirectory()) {
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(new FileReader(Constants.jsonDBConfigPath));
                JSONObject configurationJson = (JSONObject) obj;
                String url = configurationJson.get("host").toString();
                String port = configurationJson.get("port").toString();
                String dbName = configurationJson.get("dbName").toString();
                String user = configurationJson.get("user").toString();
                String pwd = configurationJson.get("password").toString();

                dbnameTextfield.setText(dbName);
                urlTextfield.setText(url);
                userTextfield.setText(user);
                pwdTextfield.setText(pwd);
                portTextfield.setText(port);

                return true;
            } catch (ParseException | IOException e) {
                logger.log(Level.SEVERE, "Error while loading DB-Configuration.");
            }
        }
        return false;
    }

    public void saveLog() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(mainController.getMainGuiController().getMainStage());
        if (file != null) {
            if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("txt")) {
                String filename = file.toString();
                file.delete();
                file = new File(filename + ".txt");
            }
            try {
                FileWriter writer = new FileWriter(file.getAbsolutePath());
                writer.write("PROSA LOG \n \n");
                writer.write(mainController.getMainGuiController().getProsaLogTextArea().getText());
                writer.write("\n \n \n \nCHATEAU LOG \n \n");
                writer.write(mainController.getMainGuiController().getChateauLogTextfield().getText());
                writer.close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Error while saving Log.");
            }
        }
    }

    public void saveChase(TextArea queryTextfield, TextArea origDatabaseSchemaTextField,
            TextArea chaseTabOrigDatabaseTuplesTextField, TextArea chaseTabSttgdTextField,
            TextArea chaseTabResultWithoutProvTextField, TextArea backChaseTabInverseQueryTextfield,
            TextArea backChaseTabSubDBWithoutProvTextfield) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(mainController.getMainGuiController().getMainStage());
        if (file != null) {
            try {
                FileWriter writer = new FileWriter(file.getAbsolutePath());
                writer.write(
                        "Original Database Schema:" + SLS + SLS + origDatabaseSchemaTextField.getText() + SLS + LINE);
                writer.write("Original Database Tuples:" + SLS + SLS + chaseTabOrigDatabaseTuplesTextField.getText()
                        + SLS + LINE);
                writer.write("SQL Query:" + SLS + SLS + queryTextfield.getText() + SLS + LINE);
                writer.write("STTGD:" + SLS + SLS + chaseTabSttgdTextField.getText() + SLS + LINE);
                writer.write("Chase Result:" + SLS + SLS + chaseTabResultWithoutProvTextField.getText() + SLS + LINE);
                writer.write("Inverse:" + SLS + SLS + backChaseTabInverseQueryTextfield.getText() + SLS + LINE);
                writer.write("Minimal Subdatabase:" + SLS + SLS + backChaseTabSubDBWithoutProvTextfield.getText());
                writer.close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Error while saving Chase results.");
            }
        }
    }
}
