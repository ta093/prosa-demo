package controller.guiController;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class GuiLauncher extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        URL fxmlLocation = getClass().getResource("/gui/prosaGUI.fxml");
        FXMLLoader loader = new FXMLLoader(fxmlLocation);
        Parent root = loader.load();

        setup(loader, stage);

        stage.setTitle("ProSA");

        stage.setScene(new Scene(root, 1089, 845));
        stage.setResizable(false);
        stage.show();
    }

    private void setup(FXMLLoader loader, Stage stage) {
        MainGuiController mainGuiController = loader.getController();
        MainController mainController = new MainController(mainGuiController);
        boolean config = mainController.getFileController().loadDbConfiguration();
        if (config) {
            mainController.getDBController().startConnection();
            mainGuiController.startChaseTab();
        }
        mainGuiController.setMainStage(stage);
        mainController.getTabController().addTabListener();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
