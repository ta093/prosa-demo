package controller.guiController;

import controller.MainController;
import util.Constants;
import xml.Attribute;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import sql2sttgd.data.dbschema.DBSchema;
import sql2sttgd.dbreader.DatabaseReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import atom.RelationalAtom;

import static util.Constants.SLS;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBController {
    Connection dbConn;
    MainController mainController;
    Logger logger;
    String dbUrl;
    String user;

    public DBController(MainController mainController) {
        this.logger = mainController.getLogController().getProsaLogger();
        this.mainController = mainController;
    }

    // starts the Connection dbConn, which can be used then
    public void startConnection() {
        if (dbConn != null) {
            try {
                dbConn.close();
            } catch (SQLException ignored) {
            }

            dbConn = null;
        }
        try {
            FileReader configReader = new FileReader(Constants.jsonDBConfigPath);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonConfig = (JSONObject) jsonParser.parse(configReader);

            String host = (String) jsonConfig.get("host");
            String port_string = (String) jsonConfig.get("port");
            String dbName = (String) jsonConfig.get("dbName");
            user = (String) jsonConfig.get("user");
            String pwd = (String) jsonConfig.get("password");

            int port = Integer.parseInt(port_string);

            dbUrl = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
            dbConn = DriverManager.getConnection(dbUrl, user, pwd);
            logger.log(Level.INFO, "Connected to database " + dbUrl + " with user '" + user + "'.");
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING, "config.json file not found.");
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "Database port can't be converted to int.");
        } catch (IOException | ParseException e) {
            logger.log(Level.WARNING, "Failed to parse config.json.");
        } catch (SQLException throwables) {
            logger.log(Level.INFO, "Failed to connect to database " + dbUrl + " with user '" + user + "'.");
        }
    }

    public Connection getDbConn() {
        return dbConn;
    }

    private void setConnectionFailed(Label connectionResultLabel) {
        connectionResultLabel.setTextFill(Color.web("rgb(255,0,0)"));
        connectionResultLabel.setText("Failed to connect.");
        logger.log(Level.INFO, "Failed to connect to database " + dbUrl + " with user '" + user + "'.");
    }

    public void testConnection(Label connectionResultLabel) {
        try {
            startConnection();
            if (dbConn == null) {
                setConnectionFailed(connectionResultLabel);
            } else {
                connectionResultLabel.setTextFill(Color.web("rgb(0,128,0)"));
                connectionResultLabel.setText("Connection successful.");

                dbConn.close();
            }
        } catch (SQLException throwables) {
            setConnectionFailed(connectionResultLabel);
        }
    }

    public void printDatabase(TextArea tuplesTextField, TextArea schemaTextField) {
        printTuples(tuplesTextField);
        printSchema(schemaTextField);
    }

    private void printSchema(TextArea schemaTextField) {
        if (dbConn == null) {
            String msg = "Failed to connect to database. Please check your DB Configuration.";
            schemaTextField.setText(msg);
            return;
        }
        StringBuilder schemaSB = new StringBuilder();
        try {
            DatabaseReader reader = new DatabaseReader(dbConn);
            List<String> tables = getAllTables(dbConn);
            DBSchema schema = reader.readSchema("public", tables);
            schema.getTables().forEach((name, relation) -> {
                schemaSB.append(name).append("(");
                Iterator<Attribute> it = relation.getAttribute().iterator();
                while (it.hasNext()) {
                    schemaSB.append(it.next().getName());
                    if (it.hasNext()) {
                        schemaSB.append(", ");
                    }
                }
                schemaSB.append(")").append(SLS);

            });

            schemaTextField.setText(schemaSB.toString());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
            schemaTextField.setText(e.getMessage());
        }
    }

    public void printTuples(TextArea tuplesTextField) {
        if (dbConn == null) {
            String msg = "Failed to connect to database. Please check your DB Configuration.";
            tuplesTextField.setText(msg);
            return;
        }
        try {
            DatabaseReader reader = new DatabaseReader(dbConn);
            List<String> tables = getAllTables(dbConn);

            LinkedHashMap<String, RelationalAtom> tuples = reader.readInstanceWithMappedAttributes("public", tables);
            StringBuilder tuplesSB = new StringBuilder();
            tuples.forEach((atom, id) -> tuplesSB.append(id + ": " + atom + SLS));

            tuplesTextField.setText(tuplesSB.toString());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
            tuplesTextField.setText(e.getMessage());
        }
    }

    public LinkedHashMap<String, RelationalAtom> readOriginalDataBase() throws Exception {

        List<String> tables = getAllTables(dbConn);
        DatabaseReader reader = new DatabaseReader(dbConn);
        return reader.readInstanceWithMappedAttributes("public", tables);
    }

    public List<String> getAllTables(Connection dbConn) throws Exception {
        Statement tablesStmt = dbConn.createStatement();
        ResultSet tablesRS = tablesStmt.executeQuery(
                "SELECT table_name FROM information_schema.tables "
                        + "WHERE table_schema='public' AND table_type='BASE TABLE';");
        if (!tablesRS.isBeforeFirst()) {
            throw new Exception("No tables found in database " + dbUrl + ".");
        }

        List<String> tables = new ArrayList<>();
        while (tablesRS.next()) {
            tables.add(tablesRS.getString(1));
        }
        return tables;
    }

    public LinkedHashSet<String> getAllAttributes() throws Exception {
        LinkedHashSet<String> attributes = new LinkedHashSet<>();

        DatabaseReader reader = new DatabaseReader(dbConn);
        List<String> tables = getAllTables(dbConn);

        DBSchema schema = reader.readSchema("public", tables);
        schema.getTables().forEach((name, relation) -> {
            relation.getAttribute().forEach(attribute -> attributes.add(attribute.getName()));
        });
        return attributes;
    }
}
