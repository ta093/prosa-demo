package service;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.logging.Logger;

import instance.Instance;
import instance.SchemaTag;
import constraints.Constraint;
import io.SingleInput;
import util.IOUtils;

public class Combiner {

    public static SingleInput combine(Instance chaseResult, LinkedHashSet<Constraint> inverse, Logger logger) {
        // switch source and target tags:
        SingleInput copyInput = IOUtils.reReadInput(new SingleInput(chaseResult, inverse), logger);
        System.out.println(copyInput.getInstance());
        Instance copyChaseResult = copyInput.getInstance();
        HashMap<String, SchemaTag> switchedSchemaTags = new HashMap<>();

        copyChaseResult.getSchemaTags().forEach((relation, schemaTag) -> {
            switch (schemaTag) {
                case SOURCE:
                    switchedSchemaTags.put(relation, SchemaTag.TARGET);
                    break;
                case TARGET:
                    switchedSchemaTags.put(relation, SchemaTag.SOURCE);
                    break;
            }

        });

        Instance copy = new Instance(copyChaseResult.getRelationalAtoms(), copyChaseResult.getSchema(),
                copyChaseResult.getOriginTag(), switchedSchemaTags);

        return new SingleInput(copy, inverse);
    }

}
