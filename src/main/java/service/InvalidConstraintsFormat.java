package service;

public class InvalidConstraintsFormat extends Exception {

    public InvalidConstraintsFormat(String msg) {
        super(msg);
    }

    /**
     * Thrown if constraints do not contain exactly one STTGD.
     */
    private static final long serialVersionUID = 916617134099503121L;

}
