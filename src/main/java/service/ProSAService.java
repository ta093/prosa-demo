package service;

import static util.Constants.SLS;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom2.JDOMException;

import api.TerminationCheckFailure;
import constraints.Constraint;
import constraints.STTgd;
import instance.Instance;
import io.InputReader;
import io.SingleInput;
import jakarta.xml.bind.JAXBException;
import service.pipelineState.BackChased;
import service.pipelineState.Chased;
import service.pipelineState.Parsed;
import sql2sttgd.api.ParserDefaultService;
import sql2sttgd.dbreader.NoColumnsException;
import sql2sttgd.parser.exception.ParsingException;
import sql2sttgd.transformation.exception.TransformationException;
import util.Constants;

public class ProSAService {

	protected Logger logger;
	protected Connection connection;
	private Optional<Parsed> plainPipeLine = Optional.empty();
	protected Optional<Parsed> provenancePipeLine = Optional.empty();
	private Instance evolutionOutput;
	private boolean withEvolution = false;

	/**
	 * Constructor
	 * 
	 * @param con PostgreSQL DB connection to use.
	 */
	public ProSAService(Logger logger, Connection con) {
		this.logger = logger;
		this.connection = con;
	}

	/**
	 * Checks whether given SQL query contains aggregate functions, such as
	 * {@code MIN}, {@code MAX}, {@code AVG}, {@code COUNT} and {@code SUM}.
	 * 
	 * @param sqlQuery SQL query to check if it has aggregate functions
	 * @return {@code true} if SQL query contains aggregate functions, otherwise
	 *         {@code false}
	 */
	public boolean hasAggregates(String sqlQuery) {
		// remove whitespace:
		String query = sqlQuery.toLowerCase().replaceAll("\\s+", "");
		return query.contains("min(")
				|| query.contains("max(")
				|| query.contains("count(")
				|| query.contains("avg(")
				|| query.contains("sum(");
	}

	/**
	 * Chase without provenance
	 * 
	 * @throws TerminationTestFailure
	 */
	public Chased chase(String sqlQuery)
			throws NoColumnsException, TransformationException, SQLException, JAXBException, ParsingException,
			InvalidConstraintsFormat, IOException, JDOMException, TerminationCheckFailure {
		Parsed parsed = parse(sqlQuery);
		Chased chased = parsed.chase();
		this.plainPipeLine = Optional.of(chased);
		return (Chased) chased;
	}

	public Parsed parse(String sqlQuery) throws NoColumnsException, TransformationException, SQLException,
			JAXBException, ParsingException, InvalidConstraintsFormat, IOException, JDOMException {
		// create byte stream that contains ChaTEAU-esque xml, then create a SingleInput
		// out of it
		logger.log(Level.INFO, "Running parse step...");
		ParserDefaultService parser = new ParserDefaultService();
		ByteArrayOutputStream parsedByteStream = new ByteArrayOutputStream();

		if (withEvolution) {
			parsedByteStream = parser.generateXmlAfterEvolution(sqlQuery, evolutionOutput);
		} else {
			parsedByteStream = parser.generateXml(connection, sqlQuery);
		}

		saveParserXML(parsedByteStream);

		InputReader reader = new InputReader();
		SingleInput input = reader.readStream(parsedByteStream);

		logger.log(Level.INFO, "Successfully converted query to s-t tgd.");
		if (input == null) {
			String msg = "Input is null.";
			logger.log(Level.SEVERE, msg);
			throw new InvalidConstraintsFormat(msg);
		}
		Parsed parsed = new Parsed(this.connection, sqlQuery, input, parsedByteStream, logger);
		this.plainPipeLine = Optional.of(parsed);
		logger.log(Level.INFO, "Parsing successfull.");
		return parsed;
	}

	private void saveParserXML(ByteArrayOutputStream parserOutput) {
		try {
			// save parser result XML for debugging:
			logger.log(Level.INFO, "Saving actual parser output to '" + Constants.ACTUALS.PARSER_OUTPUT + "'.");

			File file = new File(Constants.ACTUALS.PARSER_OUTPUT.toString());
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			OutputStream o = new FileOutputStream(Constants.ACTUALS.PARSER_OUTPUT.toString());
			parserOutput.writeTo(o);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not save parser output to XML.", e);
		}
	}

	public Optional<Parsed> getPlainPipeLine() {
		return plainPipeLine;
	}

	public Optional<Parsed> getProvenancePipeLine() {
		return provenancePipeLine;
	}

	public boolean hasCompletedNormally() {
		if (this.provenancePipeLine.isEmpty() || this.plainPipeLine.isEmpty()) {
			return false;
		} else if (this.provenancePipeLine.get() instanceof BackChased
				&& this.plainPipeLine.get() instanceof BackChased) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if constraints are exactly one s-t tgd.
	 * 
	 * @param constraints that should be exactly one s-t tgd
	 * @throws InvalidConstraintsFormat
	 */
	public void checkSTTGD(LinkedHashSet<Constraint> constraints) throws InvalidConstraintsFormat {

		logger.log(Level.INFO, "Checking constraints...");
		if (constraints.size() != 1) {
			StringBuilder constraintsString = new StringBuilder(
					"Constraints from parser is not exactly one s-t tgd: " + SLS);
			for (Constraint c : constraints) {
				constraintsString.append(c.getTypeName()).append(": ").append(c).append(SLS);
			}
			throw new InvalidConstraintsFormat(constraintsString.toString());
		} else {
			Constraint con = constraints.iterator().next();
			if (con instanceof STTgd) {
				logger.log(Level.INFO, "Constraint is a valid sttgd.");
			} else {
				String msg = "Constraint is not an s-t tgd: " + con.toString();
				throw new InvalidConstraintsFormat(msg);
			}
		}
	}

	public void setWithEvolution(instance.Instance evolutionOutput) {
		this.withEvolution = true;
		this.evolutionOutput = evolutionOutput;
	}

}
