package service.pipelineState;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.logging.Logger;

import api.TerminationCheckFailure;
import io.SingleInput;

public class WithRewrittenInput extends Parsed {

    public WithRewrittenInput(Connection connection, String sqlQuery, SingleInput rewrittenInput,
            ByteArrayOutputStream parsedStream, Logger logger) {
        super(connection, sqlQuery, rewrittenInput, parsedStream, logger);
    }
    
    @Override
    public Chased chase() throws TerminationCheckFailure {
        return chase(true);
    }

}
