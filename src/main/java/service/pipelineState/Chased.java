package service.pipelineState;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import api.ChaseService;
import api.TerminationCheckFailure;
import constraints.Constraint;
import constraints.STTgd;
import constraints.Tgd;
import instance.Instance;
import inverse.InvertingTechniques;
import io.SingleInput;
import provenance.Provenancer;
import service.Combiner;
import util.Constants;
import util.IOUtils;

public class Chased extends Parsed {

    protected Instance chaseResult;
    private boolean withProv; // whether this is part of the original pipeline or pipeline with provenance

    public Chased(Instance chaseResult, boolean withProv, Connection con, String sqlQuery, SingleInput chaseInput,
            ByteArrayOutputStream parsedStream, Logger logger) {
        super(con, sqlQuery, chaseInput, parsedStream, logger);
        this.chaseResult = chaseResult;
        this.withProv = withProv;
    }

    private BackChased backChase(SingleInput backChaseInput) throws TerminationCheckFailure {
        logger.log(Level.INFO, "Running backchase step...");
        ChaseService.runTests(backChaseInput.getInstance(), backChaseInput.getConstraints());
        Instance backChaseResult = ChaseService.runChase(backChaseInput.getInstance(), backChaseInput.getConstraints());

        IOUtils.saveBackChaseResultToXML(backChaseResult,
                withProv ? Constants.ACTUALS.BACKCHASE_RESULT_WITH_PROV.toString()
                        : Constants.ACTUALS.BACKCHASE_RESULT_WITHOUT_PROV.toString(),
                "Could not save backchase result.", logger);
        logger.log(Level.INFO, "Backchase step successful.");
        return new BackChased(backChaseResult, this.chaseResult, backChaseInput.getConstraints(), this.withProv,
                connection,
                sqlQuery, this.chaseInput, parsedStream, logger);
    }

    public BackChased backChase() throws TerminationCheckFailure {
        LinkedHashSet<STTgd> sttgds = new LinkedHashSet<>();
        sttgds.addAll(this.chaseInput.getConstraints().stream().filter(con -> (con instanceof STTgd))
                .map(sttgd -> (STTgd) sttgd)
                .collect(Collectors.toSet()));

        LinkedHashSet<Tgd> tgds = new LinkedHashSet<>();
        tgds.addAll(this.chaseInput.getConstraints().stream().filter(con -> (con instanceof Tgd)).map(tgd -> (Tgd) tgd)
                .collect(Collectors.toSet()));

        Pair<LinkedHashSet<STTgd>, Optional<Tgd>> inverse;

        if (!hasAggregates(this.sqlQuery)) {
            inverse = InvertingTechniques.invert(sttgds, tgds);
        } else {
            inverse = InvertingTechniques.invertAgg(sttgds, tgds);
        }

        LinkedHashSet<Constraint> constraints = new LinkedHashSet<>();
        constraints.addAll(inverse.getLeft());
        Optional<Tgd> tgd = inverse.getRight();
        if (tgd.isEmpty()) {
            logger.log(Level.FINE, "Inverse does not contain a tgd, skipping...");
        } else {
            logger.log(Level.FINE, "Adding tgd to inverse...");
            constraints.add(tgd.get());
        }
        SingleInput backChaseInput = Combiner.combine(this.chaseResult, constraints, logger);

        if (withProv) {
            Provenancer.addEgdsForDuplicateRemoval(backChaseInput, logger);
        }
        IOUtils.saveSingleInput(backChaseInput,
                withProv ? Constants.ACTUALS.BACKCHASE_INPUT_WITH_PROV.toString()
                        : Constants.ACTUALS.BACKCHASE_INPUT_WITHOUT_PROV.toString(),
                "Could not save backchase input to XML.", logger);

        SingleInput rereadBackChaseInput = IOUtils.reReadInput(backChaseInput, logger);
        return backChase(rereadBackChaseInput);
    }

    public Instance getChaseResult() {
        return chaseResult;
    }

}