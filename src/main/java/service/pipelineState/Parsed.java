package service.pipelineState;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom2.JDOMException;

import api.ChaseService;
import api.TerminationCheckFailure;
import chase.Chase;
import constraints.Constraint;
import instance.Instance;
import io.InputReader;
import io.SingleInput;
import provenance.Provenancer;
import service.ProSAService;
import util.Constants;
import util.IOUtils;

public class Parsed extends ProSAService {

	protected String sqlQuery;
	protected SingleInput chaseInput;
	protected ByteArrayOutputStream parsedStream;

	public Parsed(Connection con, String sqlQuery, SingleInput input, ByteArrayOutputStream parsedStream,
			Logger logger) {
		super(logger, con);
		this.sqlQuery = sqlQuery;
		this.chaseInput = input;
		this.parsedStream = parsedStream;
	}

    public LinkedHashSet<Constraint> getConstraints() {
        return chaseInput.getConstraints();
    }

    public Chased chase() throws TerminationCheckFailure {
        return chase(false);
    }

    protected Chased chase(boolean withProv) throws TerminationCheckFailure {
        logger.log(Level.INFO, "Running chase step...");
        Instance chaseResult = getChaseResult();
        IOUtils.saveInstanceToXML(chaseResult,
                withProv ? Constants.ACTUALS.CHASE_RESULT_WITH_PROV.toString()
                        : Constants.ACTUALS.CHASE_RESULT_WITHOUT_PROV.toString(),
                "Could not save chase phase result to XML.", logger);
        logger.log(Level.INFO, "Chase step successfull");
        return new Chased(chaseResult, withProv, connection, sqlQuery, chaseInput, parsedStream, logger);
    }


    private Instance getChaseResult() throws TerminationCheckFailure {
        ChaseService.runTests(this.chaseInput.getInstance(), this.chaseInput.getConstraints());
        Chase chase = new Chase(this.chaseInput.getInstance(), this.chaseInput.getConstraints());
        chase.calculateProvenance = true;
        Instance chaseResult = chase.chase();
        return chaseResult;
    }

	public SingleInput getChaseInput() {
		return chaseInput;
	}

	public WithRewrittenInput rewriteInputForProvenance() {
		/*
		 * get a SingleInput copy by reading the byte stream again, since the
		 * constructor {@link #new(Instance instance)} does not create an actual copy,
		 * but keeps references between copies.
		 */
		InputReader reader = new InputReader();

		try {
			SingleInput inputCopy = reader.readStream(this.parsedStream);
			SingleInput rewrittenInput = Provenancer.provenanceChaseInput(inputCopy);
			WithRewrittenInput rewritten = new WithRewrittenInput(connection, sqlQuery, rewrittenInput, parsedStream,
					logger);
			IOUtils.saveSingleInput(rewrittenInput, Constants.ACTUALS.CHASE_INPUT_WITH_PROV.toString(),
					"Could not save rewritten chase input", logger);
			super.provenancePipeLine = Optional.of(rewritten);
			return rewritten;
		} catch (IOException | JDOMException e) {
			// InputReader.readStream cannot throw an exception, that would have already
			// happened before in #WithConnection.parse.
			logger.log(Level.SEVERE, "Cannot read parsed byte stream (again!).", e);
			return null;
		}

	}

}
