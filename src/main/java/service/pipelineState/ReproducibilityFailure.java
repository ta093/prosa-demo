package service.pipelineState;

/**
 * thrown if executing SQL query on computed subdatabase is different than the
 * original result (i.e. the original query result cannot be reproduced with the
 * sub-DB).
 *
 */
public class ReproducibilityFailure extends Exception {

    public ReproducibilityFailure(String message) {
        super(message);
    }

    /**
     * 
     */
    private static final long serialVersionUID = -5761116531546277022L;

}
