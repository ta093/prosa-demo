package service.pipelineState;

import static util.Constants.LINE;
import static util.Constants.SLS;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.logging.Logger;

import api.ChaseService;
import constraints.Constraint;
import instance.Instance;
import io.SingleInput;
import util.IOUtils;

public class BackChased extends Chased {
    private Instance backChaseResult;
    private LinkedHashSet<Constraint> inverse;

    BackChased(Instance backChaseResult, Instance chaseResult, LinkedHashSet<Constraint> inverse, boolean withProv,
            Connection con,
            String sqlQuery, SingleInput chaseInput, ByteArrayOutputStream parsedStream, Logger logger) {
        super(chaseResult, withProv, con, sqlQuery, chaseInput, parsedStream, logger);
        this.backChaseResult = backChaseResult;
        this.inverse = inverse;
    }

    public Instance getBackChaseResult() {
        return backChaseResult;
    }

    public LinkedHashSet<Constraint> getInverse() {
        return inverse;
    }

    private Anonymized anonymize(Instance backChaseResult) {
        return new Anonymized(backChaseResult);
    }

    public Anonymized anonymize() {
        return anonymize(this.backChaseResult);
    }

    public void assertCorrectResultWithSubDB() throws ReproducibilityFailure {
        // use schema from chase input for re-building the input with
        // backChaseResult(which does not have correct schema for running chase):
        HashMap<String, LinkedHashMap<String, String>> schema = chaseInput.getInstance().getSchema();
        Instance instanceWithCorrectSchema = new Instance(backChaseResult.getRelationalAtoms(), schema,
                backChaseResult.getOriginTag(), chaseResult.getSchemaTags());

        SingleInput reReadInput = IOUtils.reReadInput(new SingleInput(instanceWithCorrectSchema, this.getConstraints()),
                logger);
        Instance subDBResult = ChaseService.runChase(reReadInput.getInstance(), this.getConstraints());
        if (!subDBResult.equals(this.getChaseResult())) {

            StringBuilder message = new StringBuilder(
                    "Result with minimal sub-DB is different than result with original DB.");
            message.append(LINE);
            message.append("Result with orignal DB:");
            message.append(LINE).append(this.getChaseResult()).append(SLS + LINE + SLS);
            message.append("Result with minimal sub-DB:");
            message.append(LINE).append(subDBResult).append(LINE);

            throw new ReproducibilityFailure(message.toString());
        }

    }

}
