package inverse;

import java.util.ArrayList;

public class InverseUtils {

	static boolean selection = false;
	static boolean projection = true;
	static boolean join = false;

	static boolean aggregation = false;
	static boolean extremum = false;
	static boolean count = false;
	static boolean sum = false;

	static boolean group = false;

	static boolean union = false;
	static boolean intersect = false;
	static boolean diff = false;
	static boolean scalar = false;

	private static void inverseType(ArrayList<String> tokens) {
		for (int p = 0; p < tokens.size(); p++) {
			switch (tokens.get(p).toLowerCase()) {
				case "where":
					selection = true;
					break;

				case "join":
					join = true;
					break;

				case "max":
				case "min":
					extremum = true;
					projection = false;
					break;

				case "count":
					count = true;
					projection = false;
					break;

				case "sum":
				case "avg":
					aggregation = true;
					projection = false;
					break;

				case "group":
					group = true;
					break;

				case "union":
					union = true;
					break;

				case "intersect":
					intersect = true;
					break;

				default:
					if (tokens.get(p).equals("*")) {
						projection = false;
					}
					break;
			}
		}
	}

	public static InverseType findInverseTypeWithoutProvenance(ArrayList<String> tokens) {
		inverseType(tokens);

		if (union || aggregation) {
			return InverseType.RESULT_EQUIVALENT;
		} else if (extremum || selection || diff || intersect || join || projection) {
			return InverseType.RELAXED;
		} else if (count) {
			return InverseType.TP_RELAXED;
		} else if (scalar || !projection) {
			return InverseType.EXACT;
		} else {
			return InverseType.NO_INVERSE;
		}
	}

	public static InverseType findInverseTypeWithProvenance(ArrayList<String> tokens) {
		inverseType(tokens);

		if (selection || intersect) {
			return InverseType.RELAXED;
		} else if (count || extremum || projection) {
			return InverseType.TP_RELAXED;
		} else if (scalar || join || union || diff || aggregation || !projection) {
			return InverseType.EXACT;
		} else {
			return InverseType.NO_INVERSE;
		}
	}
}