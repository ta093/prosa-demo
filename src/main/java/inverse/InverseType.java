package inverse;

public enum InverseType {
    NO_INVERSE("relaxed"), 
    RESULT_EQUIVALENT("result equivalent"), 
    RELAXED("relaxed"), 
    TP_RELAXED("tp-relaxed"), 
    CLASSICAL("classical"),
    EXACT("exact");

    private final String name;

    private InverseType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}