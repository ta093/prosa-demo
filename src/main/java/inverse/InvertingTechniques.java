package inverse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import atom.RelationalAtom;
import constraints.STTgd;
import constraints.Tgd;
import term.Term;
import term.Variable;
import term.VariableType;

public class InvertingTechniques {

    public static Pair<LinkedHashSet<STTgd>, Optional<Tgd>> invert(LinkedHashSet<STTgd> sttgds,
            LinkedHashSet<Tgd> tgds) {
        // init new sets of s-t tgds and tgds
        HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds = new HashMap<>();
        HashMap<Integer, LinkedHashSet<Tgd>> invertedTgds = new HashMap<>();

        // Step 1, 2 ,3: Normalised and Inverting
        normalizeAndInvert(sttgds, tgds, invertedSttgds, invertedTgds);

        // Step 4: Replacing
        replace(invertedSttgds, invertedTgds);

        // Step 6: Delete negated Head-Atoms
        removeNegatedAtoms(invertedSttgds);

        // Step 7: Resume Sttgds
        resume(invertedSttgds);

        // Step 9: Adjusting the quantifiers
        adjustQuantifiers(invertedSttgds);

        // result
        LinkedHashSet<STTgd> result = new LinkedHashSet<>();
        invertedSttgds.forEach((k, v) -> {
            result.addAll(v);
        });

        return Pair.of(result, Optional.empty());
    }

    private static void normalizeAndInvert(LinkedHashSet<STTgd> sttgds, LinkedHashSet<Tgd> tgds,
            HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds, HashMap<Integer, LinkedHashSet<Tgd>> invertedTgds) {
        int key = 0;

        for (STTgd sttgdEl : sttgds) {
            LinkedHashSet<STTgd> sttgdSet = new LinkedHashSet<>();

            for (RelationalAtom atom : sttgdEl.getHead()) {
                LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>();
                newHead.add(atom);
                STTgd newSttgd = new STTgd(newHead, sttgdEl.getBody());
                sttgdSet.add(newSttgd);
            }

            invertedSttgds.put(key++, sttgdSet);
        }

        key = 0;

        for (Tgd tgdEl : tgds) {
            LinkedHashSet<Tgd> tgdList = new LinkedHashSet<>();

            for (RelationalAtom atom : tgdEl.getHead()) {
                LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>();
                newHead.add(atom);
                Tgd newTgd = new Tgd(newHead, tgdEl.getBody());
                tgdList.add(newTgd);

            }

            invertedTgds.put(key++, tgdList);
        }
    }

    private static void replace(HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds,
            HashMap<Integer, LinkedHashSet<Tgd>> invertedTgds) {
        boolean adjust = true;

        while (adjust) {
            adjust = false;

            for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgd : invertedSttgds.entrySet()) {
                LinkedHashSet<STTgd> newSttgdSet = new LinkedHashSet<>(entrySttgd.getValue());

                // STTgd for Head-Atom
                for (STTgd sttgdHead : entrySttgd.getValue()) {
                    STTgd sttgdTemp = sttgdHead;

                    // Replacing in Sttgds
                    for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgdComp : invertedSttgds.entrySet()) {
                        // STTgd for Body-Atom
                        for (STTgd sttgdBody : entrySttgdComp.getValue()) {
                            if (!sttgdHead.equals(sttgdBody)) {
                                for (RelationalAtom headAtom : sttgdHead.getHead()) {
                                    if (sttgdBody.getBody().contains(headAtom)) {
                                        LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>(
                                                sttgdTemp.getHead());
                                        newHead.remove(headAtom);
                                        newHead.addAll(sttgdBody.getHead());

                                        STTgd newSttgd = new STTgd(sttgdTemp.getBody(), newHead);

                                        // Remove both Sttgds
                                        newSttgdSet.remove(sttgdTemp);
                                        sttgdTemp = newSttgd;

                                        if (!entrySttgd.equals(entrySttgdComp)) {
                                            LinkedHashSet<STTgd> removeSet = new LinkedHashSet<>(
                                                    entrySttgdComp.getValue());
                                            removeSet.remove(sttgdBody);
                                            invertedSttgds.put(entrySttgdComp.getKey(), removeSet);
                                        } else {
                                            newSttgdSet.remove(sttgdBody);
                                        }

                                        newSttgdSet.add(newSttgd);
                                        adjust = true;
                                    }
                                }
                            }
                        }
                    }

                    // Replacing in Tgds
                    for (Map.Entry<Integer, LinkedHashSet<Tgd>> entryTgd : invertedTgds.entrySet()) {
                        // Body-Atom
                        for (Tgd tgdBody : entryTgd.getValue()) {
                            for (RelationalAtom headAtom : sttgdHead.getHead()) {
                                if (tgdBody.getBody().contains(headAtom)) {
                                    LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>(sttgdTemp.getHead());
                                    newHead.remove(headAtom);
                                    newHead.addAll(tgdBody.getHead());
                                    STTgd newSttgd = new STTgd(sttgdTemp.getBody(), newHead);
                                    newSttgdSet.remove(sttgdTemp);
                                    newSttgdSet.add(newSttgd);
                                    sttgdTemp = newSttgd;
                                    adjust = true;
                                }
                            }
                        }
                    }

                    invertedSttgds.put(entrySttgd.getKey(), newSttgdSet);
                }
            }
        }
    }

    private static void removeNegatedAtoms(HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds) {
        for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgd : invertedSttgds.entrySet()) {
            // STTgd for Head-Atom
            LinkedHashSet<STTgd> newSttgdSet = new LinkedHashSet<>(entrySttgd.getValue());

            for (STTgd sttgd : entrySttgd.getValue()) {
                // refcopy to avoid modifying iterating element
                STTgd newSttgd = sttgd;

                // remove all negated atoms from head of s-t tgd
                for (RelationalAtom headAtom : sttgd.getHead()) {
                    LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>(newSttgd.getHead());

                    if (headAtom.isNegated()) {
                        newHead.remove(headAtom);
                        newSttgd.setHead(newHead);
                    }
                }

                // replace old s-t tgds (with negations) with new ones (without negations)
                newSttgdSet.remove(sttgd);
                newSttgdSet.add(newSttgd);
            }

            invertedSttgds.put(entrySttgd.getKey(), newSttgdSet);
        }
    }

    private static void resume(HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds) {
        for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgd : invertedSttgds.entrySet()) {
            // for each s-t tgd in invertedSttgds
            for (STTgd sttgdEl : entrySttgd.getValue()) {
                // refcopy
                STTgd sttgdTemp = sttgdEl;

                // for each s-t tgd in invertedSttgds again
                for (STTgd sttgdComp : entrySttgd.getValue()) {
                    // two s-t tgds that are not equal
                    if (!sttgdEl.equals(sttgdComp)) {
                        // head of sttgdComp is subset of or equal to head of sttgdEl
                        if (sttgdEl.getHead().containsAll(sttgdComp.getHead())) {
                            // bodies get merged
                            LinkedHashSet<RelationalAtom> newBody = new LinkedHashSet<>(sttgdEl.getBody());
                            newBody.addAll(sttgdComp.getBody());

                            LinkedHashSet<STTgd> newSttgdSet = new LinkedHashSet<>(entrySttgd.getValue());

                            // s-t tgd with merged body but sttgdEl's old head
                            STTgd newSttgd = new STTgd(newBody, sttgdEl.getHead());

                            // remove old s-t tgds
                            newSttgdSet.remove(sttgdTemp); // = sttgdEl
                            newSttgdSet.remove(sttgdComp);

                            // add new merged s-t tgd
                            newSttgdSet.add(newSttgd);

                            sttgdTemp = newSttgd;

                            // replace sttgdEl with newSttgd(Set)
                            invertedSttgds.put(entrySttgd.getKey(), newSttgdSet);
                        }
                    }
                }
            }
        }
    }

    private static void adjustQuantifiers(HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds) {
        for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgd : invertedSttgds.entrySet()) {
            LinkedHashSet<STTgd> newSttgdList = new LinkedHashSet<>(entrySttgd.getValue());

            for (STTgd sttgdEl : entrySttgd.getValue()) {
                LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<>(sttgdEl.getHead());

                for (RelationalAtom headAtom : sttgdEl.getHead()) {
                    ArrayList<Term> newTermListHead = new ArrayList<>(headAtom.getTerms());

                    for (Term termEl : headAtom.getTerms()) {
                        Term termTemp = termEl;
                        boolean exists = true;

                        if (termEl.isVariable()) {
                            for (RelationalAtom bodyAtom : sttgdEl.getBody()) {
                                for (Term termComp : bodyAtom.getTerms()) {
                                    if (exists) {
                                        if (termComp.getName().equals(termEl.getName())) {
                                            Term newTerm = new Variable(VariableType.FOR_ALL, termTemp.getName(),
                                                    ((Variable) termTemp).getIndex());
                                            newTermListHead.remove(termTemp);
                                            newTermListHead.add(newTerm);
                                            termTemp = newTerm;
                                            exists = false;
                                        } else {
                                            Term newTerm = new Variable(VariableType.EXISTS, termTemp.getName(),
                                                    ((Variable) termTemp).getIndex());
                                            newTermListHead.remove(termTemp);
                                            newTermListHead.add(newTerm);
                                            termTemp = newTerm;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    RelationalAtom newHeadAtom = new RelationalAtom(headAtom.getName(), newTermListHead);
                    newHead.remove(headAtom);
                    newHead.add(newHeadAtom);
                }

                STTgd newSttgd = new STTgd(sttgdEl.getBody(), newHead);
                newSttgdList.remove(sttgdEl);
                newSttgdList.add(newSttgd);

                invertedSttgds.put(entrySttgd.getKey(), newSttgdList);
            }
        }

        for (Map.Entry<Integer, LinkedHashSet<STTgd>> entrySttgd : invertedSttgds.entrySet()) {
            LinkedHashSet<STTgd> newSttgdList = new LinkedHashSet<>(entrySttgd.getValue());

            for (STTgd sttgdEl : entrySttgd.getValue()) {
                LinkedHashSet<RelationalAtom> newBody = new LinkedHashSet<>(sttgdEl.getBody());

                for (RelationalAtom bodyAtom : sttgdEl.getBody()) {
                    ArrayList<Term> newTermListBody = new ArrayList<>(bodyAtom.getTerms());

                    for (Term termEl : bodyAtom.getTerms()) {
                        if (termEl.isVariable()) {
                            Term newTerm = new Variable(VariableType.FOR_ALL, termEl.getName(),
                                    ((Variable) termEl).getIndex());
                            newTermListBody.remove(termEl);
                            newTermListBody.add(newTerm);
                        }
                    }

                    RelationalAtom newBodyAtom = new RelationalAtom(bodyAtom.getName(), newTermListBody);
                    newBody.remove(bodyAtom);
                    newBody.add(newBodyAtom);
                }

                STTgd newSttgd = new STTgd(newBody, sttgdEl.getHead());
                newSttgdList.remove(sttgdEl);
                newSttgdList.add(newSttgd);

                invertedSttgds.put(entrySttgd.getKey(), newSttgdList);
            }
        }
    }

    /**
     * ...
     * 
     * @param sttgds
     * @param tgds
     * @return
     */
    public static Pair<LinkedHashSet<STTgd>, Optional<Tgd>> invertAgg(LinkedHashSet<STTgd> sttgds,
            LinkedHashSet<Tgd> tgds) {
        // systrace
        System.out.println("InvertingTechniques.invertAgg()");

        RelationalAtom resultAtom = getResultAtom(tgds);
        RelationalAtom originalAtom = getOriginalRelationAtom(tgds);

        LinkedHashSet<RelationalAtom> body = new LinkedHashSet<>();
        body.add(resultAtom);

        LinkedHashSet<RelationalAtom> head = new LinkedHashSet<>();
        head.add(originalAtom);

        STTgd inverse = new STTgd(body, head);

        LinkedHashSet<STTgd> sttgdSet = new LinkedHashSet<>();
        sttgdSet.add(inverse);

        HashMap<Integer, LinkedHashSet<STTgd>> invertedSttgds = new HashMap<>();
        invertedSttgds.put(1, sttgdSet);

        adjustQuantifiers(invertedSttgds);
        sttgdSet = invertedSttgds.get(1);

        return Pair.of(sttgdSet, Optional.empty());
    }

    /**
     * ...
     * 
     * @param sttgds
     * @return
     */
    private static RelationalAtom getResultAtom(LinkedHashSet<Tgd> tgds) {
        for (Tgd tgd : tgds) {
            for (RelationalAtom atom : tgd.getHead()) {
                if (atom.getName().equals("Result")) {
                    return atom;
                }
            }
        }

        return null;
    }

    /**
     * ...
     * 
     * @param tgds
     * @return
     */
    private static RelationalAtom getOriginalRelationAtom(LinkedHashSet<Tgd> tgds) {
        String relationName = null;

        TgdLoop: for (Tgd tgd : tgds) {
            for (RelationalAtom atom : tgd.getBody()) {
                if (atom.getName().endsWith("_sorted")) {
                    relationName = atom.getName().replaceAll("_sorted", "");
                    break TgdLoop;
                }
            }
        }

        if (relationName != null) {
            // COUNT, SUM or AVG
            for (Tgd tgd : tgds) {
                for (RelationalAtom atom : tgd.getBody()) {
                    if (atom.getName().equals(relationName)) {
                        return atom;
                    }
                }
            }
        } else {
            // MIN or MAX
            for (Tgd tgd : tgds) {
                if (tgd.getBodyComparisons().size() == 0) {
                    for (RelationalAtom atom : tgd.getBody()) {
                        if (!atom.isNegated()) {
                            return atom;
                        }
                    }
                }
            }
        }

        return null;
    }

}
