# Using the anonymizer

The final step of the _ProSA pipeline_ is an optional, experimental anonymization of the resulting sub-database by using generalization hierarchies. Anonymization is done until a given _k-anonymity_ is reached, with respect to subsets of attributes - so-called _quasi-identifiers_ - that are unique for a given threshold, called the _distinct ratio_. Calculation of quasi-identifiers is done by ProSA automatically.

## Input

To use the anonymization, you have to provide some information:

* the desired (minimum) k-anonymity
* the desired distinct ratio to identify quasi-identifiers
* directly identifying attributes that are ignored (also, these attributes have to be at the leftmost positions of a relation)
* generalization hierarchies for attributes to be generalized

While the first three requirements can be input via the GUI, the generalization hierarchies need to be provided in a separate text (`*.txt`) file.

## Generalization files

_ProSA_'s anonymizer uses an own syntax for describing generalization hierarchies. The general syntax for an attribute looks as follows:

```
[<attribute>: [<gen-type> {<gen-options>}]+;]*
```

`<attribute>` represents the name of the attribute, `<gen-type>` the type of generalization, and `<gen-options>` the options for the specific generalization type. There are six types of generalizations that can be used.

Generalization | Syntax | Explanation | Example
---|---|---|---
any | `any` | Replaces values with an asterisk | `course: any;`
interval | `interval: {<start>-<end> and ...}` | Generalizes numerical values into intervals from `<start>` to `<end>` | `year: interval {1900-1949 and 1950-1999};`
map | `map: {<old> to <new> and ...}` | Maps `<old>` values to `<new>` values | `grade: map {1.0 to A and 1.3 to A and 1.7 to B};` 
masking | `masking {<num>}` | Replaces the last `<num>` characters with asterisks | `zipcode: masking {3};`
round | `round` | Rounds floating-point numbers to integers | `grade: round;`
substring | `substring {<start> to <end>}` | Replaces the value with a substring from `<start>` (inclusive) to `<end>` (exclusive) | `lastname: substring {0 to 2};`

## Example

Consider a database relation that looks like this:

studentid | studies | semester | grade
---|---|---|---
1 | Teaching | WS 15/16 | 3.7
2 | Mathematics | WS 14/15 | 1.3
3 | Engineering | WS 14/15 | 2.3
4 | Computer Science | WS 15/16 | 1.0
5 | Computer Science | WS 15/16 | 1.3
6 | Computer Science | WS 15/16 | 2.0
7 | Engineering | WS 15/16 | 3.3

The `studentid` attribute is directly identifying, therefore the set of identifying indices is {0}. We note that there are 7 rows/tuples in total and assume that we want attribute sets to have a distinct ratio of at least 0.5 to be considered a quasi-identifier. Also, our anonymized relation should be at least 2-anonymous.

`studies` has four distinct values, therefore the distinct ratio is 4/7 (or approx. 0.57), `semester` has two distinct values which gives it a distinct ratio of 2/7 (or approx. 0.29), and `grade` has six distinct values and therefore a distinct ratio of 6/7 (or approx. 0.86). Given the minimum distinct ratio of 0.5, `studies` and `grade` are considered quasi-identifying while `semester` and all supersets containing the attribute are not. Hence, both `studies` and `grade` need to be genralized.

For `studies`, we want to use a domain generalization hierarchy and therefore a map generalization. To achieve that, we need to add the following line to our input file:

```
studies: map {Mathematics to Science and Computer Science to Science};
```

For `grade`, we can use a round generalization to make it more coarse-grained:

```
grade: round;
```

This will eventually lead to the following relation:

studentid | studies | semester | grade
---|---|---|---
1 | Teaching | WS 15/16 | 4
2 | Science | WS 14/15 | 1
3 | Engineering | WS 14/15 | 2
4 | Science | WS 15/16 | 1
5 | Science | WS 15/16 | 1
6 | Science | WS 15/16 | 2
7 | Engineering | WS 15/16 | 3
