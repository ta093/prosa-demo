# Working with evolutions

The _ProSA pipeline_ can be run with or without evolution. If you want to perform an evolution and an inverse evolution within _ProSA_, you need to specify an evolution file.

## ChaTEAU file format

Before doing so, you need to understand the _ChaTEAU_ XML file format. We highly recommend reading the specification of the format, which can be found [here](https://git.informatik.uni-rostock.de/ta093/chateau-demo/-/blob/main/doc/Input.md).

## Terminology and formalization

When working with evolutions, we want to perform a query on an older version of the database schema, then transform the result set back to the present schema. The first transformation (into a past schema) is referred to as evolution while the second transformation (back to the present schema) is referred to as inverse evolution.

We want to take a look at the university example database, more specificly, on its `lecturer` and `course` relations:

```
lecturer(courseid, lecturer)
course(courseid, title)
```

Let's assume that, in the past, the `title` column wasn't part of `course` but `lecturer`:

```
lecturer(courseid, title, lecturer)
course(courseid)
```

To get there, we need two s-t tgds which we will create using _ChaTEAU_'s XML format.

## Building an evolution file

Firstly, we need a scaffold for our file:

```xml
<input>
	<schema>
		<relations>
			<!-- see below -->
		</relations>

		<dependencies>
			<!-- see below -->
		</dependencies>
	</schema>

	<instance />
</input>
```

Since we don't need to define any instance, we can create an empty element for it. In the `relations` part, we need to describe both the past and the present relation schemes. Note that, since the present relations are the newer ones, present relations are the target relations, even though evolution s-t tgds work the opposite way. _ProSA_ takes care of that by inverting the s-t tgd(s) for us.

```xml
<!-- within "relations" element -->

<relation name="lecturer" tag="T">
    <attribute name="courseid" type="int" />
    <attribute name="lecturer" type="string" />
</relation>

<relation name="course" tag="T">
    <attribute name="courseid" type="int" />
    <attribute name="title" type="string" />
</relation>

<relation name="lecturerWithTitle" tag="S">
    <attribute name="courseid" type="int" />
    <attribute name="title" type="string" />
    <attribute name="lecturer" type="string" />
</relation>

<relation name="courseWithoutTitle" tag="S">
    <attribute name="courseid" type="int" />
</relation>
```

In the `dependencies` part, we must define our evolution operations as s-t tgds. Note that we also need to define them "inverted", which means that the present relations have to occur in the head while the past relations have to occur in the body.

```xml
<!-- within "dependencies" element -->

<sttgd>
    <body>
        <atom name="lecturerWithTitle">
            <variable name="courseid" type="V" index="1" />
            <variable name="title" type="V" index="1" />
            <variable name="lecturer" type="V" index="1" />
        </atom>
    </body>
    <head>
        <atom name="course">
            <variable name="courseid" type="V" index="1" />
            <variable name="title" type="V" index="1" />
        </atom>
        <atom name="lecturer">
            <variable name="courseid" type="V" index="1" />
            <variable name="lecturer" type="V" index="1" />
        </atom>
    </head>
</sttgd>

<sttgd>
    <body>
        <atom name="courseWithoutTitle">
            <variable name="courseid" type="V" index="1" />
        </atom>
    </body>
    <head>
        <atom name="course">
            <variable name="courseid" type="V" index="1" />
            <variable name="title" type="E" index="1" />
        </atom>
    </head>
</sttgd>
```

After putting all parts together, we can use our evolution file to perform the evolution within _ProSA_.

## Example

Two examples - the one we just discussed as well as a second "company" example - can be found in the `examples` directory.