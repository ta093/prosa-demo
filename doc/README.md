# Using ProSA

Welcome to the documentation for _ProSA_. This file describes the _ProSA_ GUI, _ProSA Pipeline_, and general usage of the tool. For an installation/build guide and preliminaries, please refer to the [main README file](../README.md). This manual covers a complete run through the _ProSA Pipeline_ and showcases every tab of the interface.

## Configuration

If you start ProSA for the first time, you will be greeted with the configuration tab. Here, you can enter the database's connection details such as the host address and port, the database name and the database user and its corresponding password. Optionally, you can select an evolution file. After that, you can either proceed directly by pressing the "Save & Next" button or test if the connection is working by pressing the "Test connection" button.

![Configuration tab](img/config.png)

## Evolution

The next tab is the first optional one. If you have selected an evolution file in the previous step, it gets activated and enables the evolution step. On the left side, you can see the evolution operations as s-t tgds followed by the original database's schema and tuples. If you process the evolution by pressing the corresponding button on the bottom left, you will find the resulting database on the right side. Again, both the schema and tuples are displayed.

The screenshot shows the given university example. The `title` attribute from `course` is moved "back" into `lecturer` to represent an earlier version of the database schema. You can read more about the example [here](Evolution.md).

![Evolution tab](img/evolution.png)

## Chase

If you skipped the evolution process, or if you did not and proceeded to the next step afterwards, you will see the Chase tab. From here, you can start the actual _ProSA Pipeline_ by entering a SQL query in the top-left text area. Below that area, the original database is shown for reference. If you "Run ProSA", as the bottom-left button suggests, the SQL query will be transformed into an s-t tgd that is then presented in the upper right text area. The query result, in tuple notation and without and with _ProSA_ provenance, can be found below.

![CHASE tab](img/chase.png)

## Backchase

The next tab is the Backchase tab. This one does not require any user input since the Backchase step is executed immediately after the Chase step. In this tab, you can see the inverse query used to compute the sub-database and the CHASE-inverse type without and with provenance (top left), again followed by the original database for reference. On the right side, details about where-, why-, and how-provenance is shown, followed by the minimal sub-database, without and with provenance annotations.

![BACKCHASE tab](img/backchase.png)

## Inverse Evolution

If you have used an evolution, the inverse evolution step is active and can be found at the "Inverse-Evolution" tab. On the top left, the inverted evolution operations to restore the present schema version are shown as s-t tgds. The right half therefore shows the sub-database's schema and tuples after processing the inverse evolution, which can be achieved by pressing the according button in the bottom left. Again, for reference, the original database is also presented in this tab. 

![Inverse Evolution tab](img/inverse_evolution.png)

## Anonymizer

The last step in the _ProSA Pipeline_ is the anonymization step. This tab presents the original database and the minimal sub-database on the left side, followed by the anonymization options, including identifying attributes per relation, k-anonymity, a distinct ratio, and the selection of a hierarchy file. Details can be found [here](Anonymization.md). On the right side, the anonymized sub-database can be seen, after the "Run Anonymizer" button has been pressed.

![Anonymizer tab](img/anonymizer.png)

## Log

Lastly, the log tab shows both the _ProSA_ log and the log of the underlying _ChaTEAU_ library that is used to execute the CHASE algorithm.

![Log tab](img/log.png)