# ProSA - Provenance Management using Schema mappings with Annotations

In our project _Provenance Management using Schema mappings with Annotations_, we use additional provenance information to determine which part of a huge amount of data is necessary for evaluating a query result in terms of reproducibility, reconstructability and traceability. The determined source data we call _(minimal) sub-database_.

In ProSA, we answer questions such as: (i) Where does the data come from, (ii) why, and (iii) how is a query result calculated, by combining the _CHASE_ - a technique for transforming databases - with data provenance. The same technique allows us to process temporal databases as well. Query evaluation, evolution and their corresponding inverses are processed by the _CHASE_, implemented in a separate library called _ChaTEAU_.

## Prerequisites

* Java Development Kit 11
* Apache Maven 3.6.5 or newer
* a running PostgreSQL database (8.2 or newer). For testing purposes, we highly recommend a local version.

## Installation

Before installing ProSA, the underlying CHASE library ChaTEAU v1.3 must be installed first. Both ProSA and ChaTEAU require Apache Maven 3.6.5 or newer.

1. Clone the repository at [Rostock University GitLab](https://git.informatik.uni-rostock.de/ta093/chateau-demo).
2. Inside the root directory of ChaTEAU, run `mvn clean install` to add ChaTEAU 1.3 to your local Maven repository.
3. Inside the ProSA root directory (this repository), run `mvn clean package` to compile the software and bundle it into an executable JAR file.
4. After steps 1-3, the ProSA JAR can be found in a new `target` directory within the ProSA root directory. This file can either be executed directly or by using the Java CLI: `java -jar prosa-1.0-all-jar-with-dependencies.jar` (the file name may vary). This is especially useful to see additional log information and to trace exceptions, if any occur.

Alternatively, just use the packaged version provided in the repository.

## Documentation/Manual

A manual/walkthrough can be found [here](doc/README.md). A short promo video can be found [here](https://mediathek2.uni-regensburg.de/playthis/63dff3d69cc445.84848168) (password: prosa).

## Literature referencing ProSA

1. Auge, Heuer: ProSA -- Using the CHASE for Provenance Management (2019). [(paper)](https://link.springer.com/chapter/10.1007/978-3-030-28730-6_22)
2. Auge: Extended Provenance Management for Data Science Applications (2020). [(paper)](http://ceur-ws.org/Vol-2652/paper04.pdf)
3. Auge, Scharlau, Heuer: Provenance and Privacy in ProSA -- A Guided Interview on Privacy-Aware Provenance (2021). [(paper)](https://link.springer.com/chapter/10.1007/978-3-030-87101-7_6)
4. Auge, Scharlau, Zimmer, Görres, Heuer: ChaTEAU -- A Universal Toolkit for Applying the Chase (2022). [(paper)](https://arxiv.org/pdf/2206.01643.pdf)
5. Auge, Hanzig, Heuer: ProSA Pipeline -- Provenance Conquers the Chase (2023). [(paper)](https://doi.org/10.1007/978-3-031-15743-1_9)
